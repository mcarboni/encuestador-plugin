package encuestador

enum EstadoCarga {
    P,  // carga parcial
    C   // completa
}

enum EstadoEncuesta {
    INVITADA, ABIERTA, POR_VALIDAR, REABIERTA, VALIDADA, OBSERVADA, DECLINADA, PRECARGADA
}

enum Base {
    BASE_UNO, BASE_DOS
}

enum TipoPuntoDeContacto {
    EMAIL, TELEFONO, CELULAR
}