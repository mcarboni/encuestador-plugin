# Encuestador Plugin #

Plugin para grails 2.4.4 para construir una encuesta con múltiples formularios y múltiples ediciones

## ¿Cómo lo uso? ##

Crear un proyecto con grails 2.4.4 en adelante.

Agregar a BuildConfig.groovy:

```groovy
   plugins {

        // otras dependencias

   	runtime ":encuestador-plugin:1.0.0"

        // otras dependencias

   }
```

Y agregar a Config.groovy:

```groovy

grails.config.locations = [EncuestadorPluginConfig]

```

El plugin incluye layouts, js y css. Eliminar los que vienen por defecto.

En resources.groovy se debe declarar el userDetailsService a mano:

```groovy

import encuestador.CaseInsensitiveDetailsService

beans = {

    userDetailsService(CaseInsensitiveDetailsService)

}

```