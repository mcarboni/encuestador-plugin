import encuestador.AsignacionParticipante
import encuestador.Edicion
import encuestador.Empresa
import encuestador.Edicion
import encuestador.Encuestador
import encuestador.ListaControlada
import encuestador.ParticipaEn
import encuestador.PuntoDeContacto
import encuestador.TipoPuntoDeContacto
import encuestador.security.Authority
import encuestador.security.Person
import encuestador.security.PersonAuthority
import org.apache.commons.lang3.RandomStringUtils

/**
 * Importa el padrón.
 * ID	usuario	Empresa	CUIT	Nombre y Apellido	E-mail de contacto	2013	2014	Encuestador	FOP
 * 1	13MA	3 M ARGENTINA SACIFIA	30529994393	VIRGINIA BASUALDO	vgbasualdo@mmm.com			Contacto 01 - Agustina	1
 */

def enq = Edicion.findByCodigo('eract2016')
def cuando = new Date().format("yyyyMMdd-HHmmss")
def salida = new File("padron_dnic_${cuando}.log")
def error = new File("padron_dnic_${cuando}.error.log")
def i = 0, correctos = 0, fallaron = 0
def provincias = ListaControlada.findByCodigo('provincia').valores

System.in.splitEachLine("\t") { fields ->
    def mensaje
    if (fields.size() >= 10) {
        try {
            def elID = fields[0].trim()
            //def elUsername = fields[1].trim()
            def laEmpresa = fields[2].trim()
            //def elUsername = laEmpresa.split(" ")[0].toLowerCase() + elID
            def elCuit = fields[3]?.trim() ?: '0'
            def domicilio = fields[4]?.trim() ?: ''
            def codigoPostal = fields[5]?.trim() ?: ''
            def localidad = fields[6]?.trim() ?: ''
            def provincia = fields[7]?.trim() ?: ''
            def telefono = fields[8]?.trim() ?: ''
            def email = fields[9]?.trim() ?: ''
            def web = fields[10]?.trim() ?: ''
            def nombreDeContacto = fields[11]?.trim() ?: ''
            def emailDeContacto = fields[12]?.trim() ?: ''
            def telefonoDeContacto = fields[13]?.trim() ?: ''
            def mostrar2013 = fields[14]?.trim() ?: 0
            def mostrar2014 = fields[15]?.trim() ?: 0
            def mostrar2015 = fields[16]?.trim() ?: 0
            def ventas2014 = fields[17]?.trim()?.replace(',','.') ?: null
            def cantidadEmpleados2014 = fields[18]?.trim()?.replace(',','.') ?: null
            def cuantosEmpleados2014 =  fields[19]?.trim()?.replace(',','.') ?: null
            def gastoInterno2014 = fields[20]?.trim()?.replace(',','.') ?: null
            def gastoExterno2014 = fields[21]?.trim()?.replace(',','.') ?: null
            def gastoInterno2014Total = fields[22]?.trim()?.replace(',','.') ?: null
            def password = fields[23]?.trim() ?: null
            def elUsername = fields[24]?.trim() ?: null

            elUsername = elUsername ?: laEmpresa.split(" ")[0].toLowerCase() + elID

            def elPassword = password ?: RandomStringUtils.random(9, true, true)

            def laProvincia = provincias.find { it.descripcion == provincia }?.codigo

            def re = Authority.findByAuthority("ROLE_EMPRESA")

            def elContexto = '{'
            elContexto += mostrar2013 == '0' ? ' "mostrar2013":false ' : '"mostrar2013":true '
            elContexto += ','
            elContexto += mostrar2014 == '0' ? ' "mostrar2014":false ' : '"mostrar2014":true '
            elContexto += ','
            elContexto += '"DATOS_EMPRESA": {'
            elContexto += "\"razonSocial\":\"$laEmpresa\","
            elContexto += "\"cuit\":\"$elCuit\","
            elContexto += "\"domicilio\":\"$domicilio\","
            elContexto += "\"codigoPostal\":\"$codigoPostal\","
            elContexto += "\"provincia\":\"$laProvincia\","
            elContexto += "\"localidad\":\"$localidad\","
            elContexto += "\"telefono\":\"$telefono\","
            elContexto += "\"email\":\"$email\","
            elContexto += "\"paginaWeb\":\"$web\""
            elContexto += '}'
            def hayPrecarga = false
            if (ventas2014) {
                hayPrecarga = true
                elContexto += ','
                elContexto += '"INFO_EMPRESA": {'
                elContexto += "\"ventas2014\":$ventas2014,"
                elContexto += "\"cantidadEmpleados2014\":$cantidadEmpleados2014"
                elContexto += '}'
            }
            if (cuantosEmpleados2014) {
                hayPrecarga = true
                elContexto += ','
                elContexto += '"RECURSOS_HUMANOS": {'
                elContexto += "\"cuantosEmpleados2014\":$cuantosEmpleados2014"
                elContexto += '}'
            }
            if (gastoInterno2014) {
                hayPrecarga = true
                elContexto += ','
                elContexto += '"RECURSOS_FINANCIEROS": {'
                elContexto += "\"gastoInterno2014\":$gastoInterno2014,"
                elContexto += "\"gastoExterno2014\":$gastoExterno2014,"
                elContexto += "\"gastoInterno2014Total\":$gastoInterno2014Total"
                elContexto += '}'
            }
            if (hayPrecarga) {
                elContexto += ','
                elContexto += '"precargada": true'
            }
            elContexto += '}'

//            println(elContexto)
//            println(elUsername)

            // println(encuestadorUsername)
            def parti, emp

            Person.withTransaction { status ->
                def usuario = new Person(username: elUsername, password: elPassword)
                usuario.save(flush: true)

                new PersonAuthority(person: usuario, authority: re).save(flush: true)

                def pto = null
                if ( nombreDeContacto != 'null' ) {
                    pto = new PuntoDeContacto(persona: nombreDeContacto, contacto: emailDeContacto, posicion: 'n/d', tipo: TipoPuntoDeContacto.EMAIL)
                    pto.save(flush: true)

                    if (!pto.id) {
                        throw new RuntimeException("no se pudo persisitir el punto de contacto")
                    }
                }

                emp = new Empresa(empresaId: elID,cuit: elCuit, nombre: laEmpresa, email: emailDeContacto, user: usuario)
                if (pto) {
                    emp.addToPuntosDeContacto(pto)
                }
                emp.save(flush: true)

                if (!emp.id) {
                    throw new RuntimeException("no se pudo persisitir la empresa")
                }

                parti = new ParticipaEn(empresa: emp, edicion: enq, contexto: elContexto, relevadaExternamente: false)
                parti.save(flush: true)

                if (!parti.id) {
                    throw new RuntimeException("no se pudo persisitir la participacion")
                }

                parti = ParticipaEn.findByEmpresaAndEdicion(emp, enq)

            }
            ++correctos
            mensaje = "OK: $elID;'$elUsername';'$elPassword';'$emailDeContacto';'$nombreDeContacto';'$laEmpresa'"
        } catch (Exception ex) {
            mensaje = "ERROR: $fields: problemas con los datos."
            error << "$fields:" + ex.getMessage() + "\r\n"
            ++fallaron
        }
    } else {
        ++fallaron
        mensaje = "ERROR: $fields: cantidad incorrecta de parametros"
    }
    ++i
    salida << "$mensaje\r\n"
}
salida << "$i elementos procesados. $correctos correctos, $fallaron fallaron.\r\n"


