import encuestador.Empresa
import encuestador.Edicion
import encuestador.Formulario
import encuestador.ListaControlada
import encuestador.ParticipaEn
import encuestador.ValorListaControlada
import encuestador.security.Authority
import encuestador.security.Person
import encuestador.security.PersonAuthority
import encuestador.security.Requestmap
import org.grails.datastore.mapping.transactions.SessionHolder
import org.springframework.orm.hibernate3.SessionFactoryUtils
import org.springframework.transaction.support.TransactionSynchronizationManager

def configureHibernateSession() {
    // without this you'll get a lazy initialization exception when using a many-to-many relationship
    def sessionFactory = appCtx.getBean("sessionFactory")
    def session = SessionFactoryUtils.getSession(sessionFactory, true)
    TransactionSynchronizationManager.bindResource(sessionFactory, new SessionHolder(session))
}

target(iniciarSeguridad: "Crea los roles, los usuarios y los requestmap básicos!") {

    depends(configureProxy, packageApp, classpath, loadApp, configureApp)
    configureHibernateSession()

}

setDefaultTarget(iniciarSeguridad)
