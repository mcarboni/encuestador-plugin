import encuestador.Edicion
import encuestador.Empresa
import encuestador.Encuesta
import encuestador.ParticipaEn

/**
 * Asocia empresas con encuesta.
 *
 * Para eso necesitamos pasarle el cuit y el codigo de encuesta separado por tabs
 */
System.in.splitEachLine("\t") { fields ->
    def mensaje
    if (fields.size() == 2) {
        def cuit = fields[0]
        def codigoEncuesta = fields[1]

        def emp = Empresa.findByCuit(cuit)
        def enq = Edicion.findByCodigo(codigoEncuesta)

        if (enq && emp) {
            def p = new ParticipaEn(empresa: emp, edicion: enq).save(flush: true)
            if (p.id != null) {
                mensaje  = "'$emp.nombre' participa en '$enq.nombre'"
            } else {
                mensaje = "$fields: problemas guardando la encuesta"
            }
        } else {
            mensaje = "$fields: no encuentro la empresa o la encuesta"
        }
    } else {
        mensaje = "$fields: cantidad incorrecta de parametros"
    }
    println mensaje
}
