
import encuestador.Empresa
import encuestador.PuntoDeContacto
import encuestador.TipoPuntoDeContacto
import encuestador.security.Authority
import encuestador.security.Person
import encuestador.security.PersonAuthority
import grails.util.Environment
import static grails.util.Metadata.current as metaInfo

/*
header 'Application Status'
row 'App version', metaInfo['app.version']
row 'Grails version', metaInfo['app.grails.version']
row 'Groovy version', GroovySystem.version
row 'JVM version', System.getProperty('java.version')
row 'Reloading active', Environment.reloadingAgentEnabled
row 'Controllers', grailsApplication.controllerClasses.size()
row 'Domains', grailsApplication.domainClasses.size()
row 'Services', grailsApplication.serviceClasses.size()
row 'Tag Libraries', grailsApplication.tagLibClasses.size()
println()

header 'Installed Plugins'
ctx.getBean('pluginManager').allPlugins.each { plugin ->
    row plugin.name, plugin.version
}

void row(final String label, final value) {
    println label.padRight(18) + ' : ' + value.toString().padLeft(8)
}

void header(final String title) {
    final int length = 29
    println '-' * length
    println title.center(length)
    println '-' * length
}
*/

def re = Authority.findByAuthority('ROLE_EMPRESA')
def i = 1

new File( 'eract.padron.csv' ).splitEachLine("\t") { fields ->
    def fs = fields.collect{ it.trim()}
    def pto = new PuntoDeContacto(persona: fs[4], contacto: fs[3], posicion: fs[5] ?: 'n/d', tipo: TipoPuntoDeContacto.EMAIL)
    pto.save()
    def usuario = new Person(username:"empresa$i", password: "empresa${i}2016")
    usuario.save()

    new PersonAuthority(person: usuario, authority: re).save()

    def empresa = new Empresa(cuit: fs[2], nombre: fs[1], email: "info@empresa${i}.com.ar", puntosDeContacto: pto, user: usuario)
    empresa.save()
    println fs
    ++i
}
