import encuestador.AsignacionParticipante
import encuestador.Edicion
import encuestador.Empresa
import encuestador.Encuesta
import encuestador.Encuestador
import encuestador.ParticipaEn
import encuestador.PuntoDeContacto
import encuestador.TipoPuntoDeContacto
import encuestador.security.Authority
import encuestador.security.Person
import encuestador.security.PersonAuthority
import org.apache.commons.lang3.RandomStringUtils

/**
 * Importa el padrón.
 * ID	usuario	Empresa	CUIT	Nombre y Apellido	E-mail de contacto	2013	2014	Encuestador	FOP
 * 1	13MA	3 M ARGENTINA SACIFIA	30529994393	VIRGINIA BASUALDO	vgbasualdo@mmm.com			Contacto 01 - Agustina	1
 */

def enq = Edicion.findByCodigo('eract2016')
def cuando = new Date().format("yyyyMMdd-hhmmss")
def salida = new File("asignar_encuestadores_${cuando}.log")
def error = new File("asignar_encuestadores_${cuando}.error.log")
def i = 0, correctos = 0, fallaron = 0

System.in.splitEachLine("\t") { fields ->
    def mensaje
    if (fields.size() == 10) {
        def elID = fields[0].trim()
        def elUsername = fields[1].trim()
        def laEmpresa = fields[2].trim()
        def elCuit = fields[3]?.trim() ?: '0'
        def nombreYApellido = fields[4]?.trim() ?: ''
        def emailDeContacto = fields[5]?.trim() ?: ''
        def mostrar2013 = fields[6]?.trim() ?: 0
        def mostrar2014 = fields[7]?.trim() ?: 0
        def encuestador = fields[8]?.trim()
        def esFop = fields[9]?.trim() ?: 0
        def elPassword = RandomStringUtils.random(9, true, true)

        def re = Authority.findByAuthority("ROLE_EMPRESA")

        try {

            def encuestadorTokens = encuestador.split()
            encuestadorUsername = encuestadorTokens[0].toLowerCase() + encuestadorTokens[1]
            println(encuestadorUsername)
            def elEncuestador = Encuestador.findByUser(Person.findByUsername(encuestadorUsername))
            if (!elEncuestador) {
                throw new RuntimeException("no encuentro el encuestador $encuestadorUsername")
            }

            def emp = Empresa.findByEmpresaId(elID)

            if (!emp) {
                throw new RuntimeException("no encontre la empresa con el id $elID")
            }
            parti = ParticipaEn.findByEmpresaAndEdicion(emp, enq)

            if (!parti) {
                throw new RuntimeException("no encontre la participacion para la empresa '$laEmpresa'")
            }

            def asig = new AsignacionParticipante(encuestador: elEncuestador, participante: parti)
            asig.save(flush: true)

            if (!asig.id) {
                throw new RuntimeException("no se pudo persisitir la asignacion")
            }

            ++correctos
            mensaje = "OK: $elID;'$elUsername';'$elPassword';'$emailDeContacto';'$nombreYApellido';'$laEmpresa';'$elEncuestador.nombre'"
        } catch (Exception ex) {
            mensaje = "ERROR: $fields: problemas con los datos."
            error << "$fields:" + ex.getMessage() + "\r\n"
            ++fallaron
        }
        ++i
    } else {
        ++i
        ++fallaron
        mensaje = "ERROR: $fields: cantidad incorrecta de parametros"
    }
    salida << "$mensaje\r\n"
}
salida << "$i elementos procesados. $correctos correctos, $fallaron fallaron.\r\n"


