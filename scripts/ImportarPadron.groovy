import encuestador.AsignacionParticipante
import encuestador.Empresa
import encuestador.Encuesta
import encuestador.Encuestador
import encuestador.ParticipaEn
import encuestador.PuntoDeContacto
import encuestador.TipoPuntoDeContacto
import encuestador.security.Authority
import encuestador.security.Person
import encuestador.security.PersonAuthority
import org.apache.commons.lang3.RandomStringUtils

/**
 * Importa el padrón.
 * ID	usuario	Empresa	CUIT	Nombre y Apellido	E-mail de contacto	2013	2014	Encuestador	FOP
 * 1	13MA	3 M ARGENTINA SACIFIA	30529994393	VIRGINIA BASUALDO	vgbasualdo@mmm.com			Contacto 01 - Agustina	1
 */

def enq = Edicion.findByCodigo('eract2016')
def cuando = new Date().format("yyyyMMdd-hhmmss")
def salida = new File("importar_empresas_${cuando}.log")
def error = new File("importar_empresas_${cuando}.error.log")
def i = 0, correctos = 0, fallaron = 0

System.in.splitEachLine("\t") { fields ->
    def mensaje
    if (fields.size() >= 10) {
        try {
            def elID = fields[0].trim()
            def elUsername = fields[1].trim()
            def laEmpresa = fields[2].trim()
            def elCuit = fields[3]?.trim() ?: '0'
            def nombreYApellido = fields[4]?.trim() ?: ''
            def emailDeContacto = fields[5]?.trim() ?: ''
            def mostrar2013 = fields[6]?.trim() ?: 0
            def mostrar2014 = fields[7]?.trim() ?: 0
            def encuestador = fields[8]?.trim()
            def esFop = fields[9]?.trim() ?: 0
            def elPassword = RandomStringUtils.random(9, true, true)

            def re = Authority.findByAuthority("ROLE_EMPRESA")

            def elContexto = '{'
            elContexto += mostrar2013 == '1' ? ' "mostrar2013":false ' : '"mostrar2013":true '
            elContexto += ','
            elContexto += mostrar2014 == '1' ? ' "mostrar2014":false ' : '"mostrar2014":true '
            elContexto += '}'

            def encuestadorTokens = encuestador.split()

    //        println(encuestador)
    //        println(encuestadorTokens)
            encuestadorUsername = encuestadorTokens[0].toLowerCase() + encuestadorTokens[1]
            println(elContexto)
            println(encuestadorUsername)
            def elEncuestador = Encuestador.findByUser(Person.findByUsername(encuestadorUsername))
            if (!elEncuestador) {
                throw new RuntimeException("no encuentro el encuestador $encuestadorUsername")
            }

            def parti, emp

            Person.withTransaction { status ->
                def usuario = new Person(username: elUsername, password: elPassword)
                usuario.save(flush: true)

                new PersonAuthority(person: usuario, authority: re).save(flush: true)

                def pto = null
                if ( nombreYApellido != 'null' ) {
                    pto = new PuntoDeContacto(persona: nombreYApellido, contacto: emailDeContacto, posicion: 'n/d', tipo: TipoPuntoDeContacto.EMAIL)
                    pto.save(flush: true)

                    if (!pto.id) {
                        throw new RuntimeException("no se pudo persisitir el punto de contacto")
                    }
                }

                emp = new Empresa(empresaId: elID,cuit: elCuit, nombre: laEmpresa, email: emailDeContacto, user: usuario)
                if (pto) {
                    emp.addToPuntosDeContacto(pto)
                }
                emp.save(flush: true)

                if (!emp.id) {
                    throw new RuntimeException("no se pudo persisitir la empresa")
                }

                parti = new ParticipaEn(empresa: emp, edicion: enq, contexto: elContexto, relevadaExternamente: true)
                parti.save(flush: true)

                if (!parti.id) {
                    throw new RuntimeException("no se pudo persisitir la participacion")
                }

                parti = ParticipaEn.findByEmpresaAndEdicion(emp, enq)

                def asig = new AsignacionParticipante(encuestador: elEncuestador, participante: parti)
                asig.save(flush: true)

                if (!asig.id) {
                    throw new RuntimeException("no se pudo persisitir la asignacion")
                }

            }

            ++correctos
            mensaje = "OK: $elID;'$elUsername';'$elPassword';'$emailDeContacto';'$nombreYApellido';'$laEmpresa';'$elEncuestador.nombre'"
        } catch (Exception ex) {
            mensaje = "ERROR: $fields: problemas con los datos."
            error << "$fields:" + ex.getMessage() + "\r\n"
            ++fallaron
        }
    } else {
        ++fallaron
        mensaje = "ERROR: $fields: cantidad incorrecta de parametros"
    }
    ++i
    salida << "$mensaje\r\n"
}
salida << "$i elementos procesados. $correctos correctos, $fallaron fallaron.\r\n"


