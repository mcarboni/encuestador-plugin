import encuestador.Encuestador
import encuestador.security.Authority
import encuestador.security.Person
import encuestador.security.PersonAuthority

/**
 * Importa encuestadores de la forma:
 * Nombre   Username/email  Password - separados por tab y sin cabeceras!
 * Miguel	contacto13 	5C176
 */
System.in.splitEachLine("\t") { fields ->
    def mensaje
    if (fields.size() == 3) {
        def elNombre = fields[0].trim()
        def elUsername = fields[1]?.trim()
        def elPassword = fields[2].trim()
        def elEmail = "$elUsername@mincyt.gob.ar"
        def rf = Authority.findByAuthority("ROLE_FOP")

        new Person(elUsername, elPassword).save(flush: true)

        def p = Person.findByUsername(elUsername)

        if (p) {
            new PersonAuthority(p, rf).save(flush: true)
            def enc = new Encuestador(nombre: elNombre,email: elEmail,user: p)
            enc.save(flush: true)
            if (enc.id) {
                mensaje = "Encuestador '$enc.nombre' creado con usuario $enc.user.username."
            } else {
                mensaje = "$fields: no se puede crear el encuestador"
            }
        } else {
            mensaje = "$fields: no se pudo crear el usuario"
        }
    } else {
        mensaje = "$fields: cantidad incorrecta de parametros"
    }
    println mensaje
}
