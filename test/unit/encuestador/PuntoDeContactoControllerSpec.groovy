package encuestador



import grails.test.mixin.*
import spock.lang.*

@TestFor(PuntoDeContactoController)
@Mock(PuntoDeContacto)
class PuntoDeContactoControllerSpec extends Specification {

    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        //params["name"] = 'someValidName'
    }

    void "Test the index action returns the correct model"() {

        when:"The index action is executed"
            controller.index()

        then:"The model is correct"
            !model.puntoDeContactoInstanceList
            model.puntoDeContactoInstanceCount == 0
    }

    void "Test the create action returns the correct model"() {
        when:"The create action is executed"
            controller.create()

        then:"The model is correctly created"
            model.puntoDeContactoInstance!= null
    }

    void "Test the save action correctly persists an instance"() {

        when:"The save action is executed with an invalid instance"
            request.contentType = FORM_CONTENT_TYPE
            request.method = 'POST'
            def puntoDeContacto = new PuntoDeContacto()
            puntoDeContacto.validate()
            controller.save(puntoDeContacto)

        then:"The create view is rendered again with the correct model"
            model.puntoDeContactoInstance!= null
            view == 'create'

        when:"The save action is executed with a valid instance"
            response.reset()
            populateValidParams(params)
            puntoDeContacto = new PuntoDeContacto(params)

            controller.save(puntoDeContacto)

        then:"A redirect is issued to the show action"
            response.redirectedUrl == '/puntoDeContacto/show/1'
            controller.flash.message != null
            PuntoDeContacto.count() == 1
    }

    void "Test that the show action returns the correct model"() {
        when:"The show action is executed with a null domain"
            controller.show(null)

        then:"A 404 error is returned"
            response.status == 404

        when:"A domain instance is passed to the show action"
            populateValidParams(params)
            def puntoDeContacto = new PuntoDeContacto(params)
            controller.show(puntoDeContacto)

        then:"A model is populated containing the domain instance"
            model.puntoDeContactoInstance == puntoDeContacto
    }

    void "Test that the edit action returns the correct model"() {
        when:"The edit action is executed with a null domain"
            controller.edit(null)

        then:"A 404 error is returned"
            response.status == 404

        when:"A domain instance is passed to the edit action"
            populateValidParams(params)
            def puntoDeContacto = new PuntoDeContacto(params)
            controller.edit(puntoDeContacto)

        then:"A model is populated containing the domain instance"
            model.puntoDeContactoInstance == puntoDeContacto
    }

    void "Test the update action performs an update on a valid domain instance"() {
        when:"Update is called for a domain instance that doesn't exist"
            request.contentType = FORM_CONTENT_TYPE
            request.method = 'PUT'
            controller.update(null)

        then:"A 404 error is returned"
            response.redirectedUrl == '/puntoDeContacto/index'
            flash.message != null


        when:"An invalid domain instance is passed to the update action"
            response.reset()
            def puntoDeContacto = new PuntoDeContacto()
            puntoDeContacto.validate()
            controller.update(puntoDeContacto)

        then:"The edit view is rendered again with the invalid instance"
            view == 'edit'
            model.puntoDeContactoInstance == puntoDeContacto

        when:"A valid domain instance is passed to the update action"
            response.reset()
            populateValidParams(params)
            puntoDeContacto = new PuntoDeContacto(params).save(flush: true)
            controller.update(puntoDeContacto)

        then:"A redirect is issues to the show action"
            response.redirectedUrl == "/puntoDeContacto/show/$puntoDeContacto.id"
            flash.message != null
    }

    void "Test that the delete action deletes an instance if it exists"() {
        when:"The delete action is called for a null instance"
            request.contentType = FORM_CONTENT_TYPE
            request.method = 'DELETE'
            controller.delete(null)

        then:"A 404 is returned"
            response.redirectedUrl == '/puntoDeContacto/index'
            flash.message != null

        when:"A domain instance is created"
            response.reset()
            populateValidParams(params)
            def puntoDeContacto = new PuntoDeContacto(params).save(flush: true)

        then:"It exists"
            PuntoDeContacto.count() == 1

        when:"The domain instance is passed to the delete action"
            controller.delete(puntoDeContacto)

        then:"The instance is deleted"
            PuntoDeContacto.count() == 0
            response.redirectedUrl == '/puntoDeContacto/index'
            flash.message != null
    }
}
