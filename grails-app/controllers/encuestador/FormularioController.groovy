package encuestador



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class FormularioController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Formulario.list(params), model:[formularioInstanceCount: Formulario.count()]
    }

    def show(Formulario formularioInstance) {
        respond formularioInstance
    }

    def create() {
        respond new Formulario(params)
    }

    @Transactional
    def save(Formulario formularioInstance) {
        if (formularioInstance == null) {
            notFound()
            return
        }

        if (formularioInstance.hasErrors()) {
            respond formularioInstance.errors, view:'create'
            return
        }

        formularioInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'formulario.label', default: 'Formulario'), formularioInstance.id])
                redirect formularioInstance
            }
            '*' { respond formularioInstance, [status: CREATED] }
        }
    }

    def edit(Formulario formularioInstance) {
        respond formularioInstance
    }

	def obtenerArchivo(ruta) {
		grailsApplication.parentContext.getResource('/..'+ruta)
	}
	
	@Transactional
	def refrescarTemplates(Formulario formularioInstance) {
		def archivoBase = 'f'+formularioInstance.orden
		def dirBase = "/imports/templates/${formularioInstance.edicion.encuesta.codigo}/${formularioInstance.edicion.anio}/"
        def texto = obtenerArchivo(dirBase+archivoBase+'.html').file.getText('UTF-8')
		formularioInstance.template = texto
		texto = obtenerArchivo(dirBase+archivoBase+'_view.html').file.getText('UTF-8')
		formularioInstance.templateVista = texto
		texto = obtenerArchivo(dirBase+archivoBase+'_validacion.js').file.getText('UTF-8')
		formularioInstance.scriptValidacion = texto

		formularioInstance.save flush:true
		request.withFormat {
			'*'{ respond formularioInstance, view: 'show' }
			
		}
	}

    @Transactional
    def obtenerCampos(Formulario formularioInstance) {
		formularioInstance.campos = formularioInstance.extraerCampos()
		
		formularioInstance.save flush:true
		request.withFormat {
			'*'{ respond formularioInstance, view: 'show' }
			
		}
    }
	
    @Transactional
    def update(Formulario formularioInstance) {
        if (formularioInstance == null) {
            notFound()
            return
        }

        if (formularioInstance.hasErrors()) {
            respond formularioInstance.errors, view:'edit'
            return
        }

		formularioInstance.campos = formularioInstance.extraerCampos()

		formularioInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Formulario.label', default: 'Formulario'), formularioInstance.id])
                redirect formularioInstance
            }
            '*'{ respond formularioInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Formulario formularioInstance) {

        if (formularioInstance == null) {
            notFound()
            return
        }

        formularioInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Formulario.label', default: 'Formulario'), formularioInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'formulario.label', default: 'Formulario'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
