package encuestador



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional
import groovy.json.JsonOutput

@Transactional(readOnly = true)
class ParticipaEnController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond ParticipaEn.list(params), model:[participaEnInstanceCount: ParticipaEn.count()]
    }

    def show(ParticipaEn participaEnInstance) {
		respond participaEnInstance
    }

    def create() {
        respond new ParticipaEn(params)
    }

    @Transactional
    def save(ParticipaEn participaEnInstance) {
        if (participaEnInstance == null) {
            notFound()
            return
        }

        if (participaEnInstance.hasErrors()) {
            respond participaEnInstance.errors, view:'create'
            return
        }

        participaEnInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'participaEn.label', default: 'ParticipaEn'), participaEnInstance.id])
                redirect participaEnInstance
            }
            '*' { respond participaEnInstance, [status: CREATED] }
        }
    }

    def edit(ParticipaEn participaEnInstance) {
        participaEnInstance.contexto = JsonOutput.prettyPrint(participaEnInstance.contexto)
		respond participaEnInstance
    }

    @Transactional
    def update(ParticipaEn participaEnInstance) {
        if (participaEnInstance == null) {
            notFound()
            return
        }

        if (participaEnInstance.hasErrors()) {
            respond participaEnInstance.errors, view:'edit'
            return
        }

        participaEnInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'ParticipaEn.label', default: 'ParticipaEn'), participaEnInstance.id])
                redirect participaEnInstance
            }
            '*'{ respond participaEnInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(ParticipaEn participaEnInstance) {

        if (participaEnInstance == null) {
            notFound()
            return
        }

        participaEnInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'ParticipaEn.label', default: 'ParticipaEn'), participaEnInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'participaEn.label', default: 'ParticipaEn'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }

	def search(Integer max) {
		params.max = Math.min(max ?: 10, 100)
		String eid = new String(params.empresaId)
		def laEmpresa = Empresa.findByEmpresaId(eid)
		def participantes = ParticipaEn.where { empresa == laEmpresa}.list(params) 
		respond participantes, model:[participaEnInstanceCount: ParticipaEn.count()], view: 'index'
	}


}
