package encuestador



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class EventoContactoController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond EventoContacto.list(params), model:[eventoContactoInstanceCount: EventoContacto.count()]
    }

    def show(EventoContacto eventoContactoInstance) {
        respond eventoContactoInstance
    }

    def create() {
        respond new EventoContacto(params)
    }

    @Transactional
    def save(EventoContacto eventoContactoInstance) {
        if (eventoContactoInstance == null) {
            notFound()
            return
        }

        if (eventoContactoInstance.hasErrors()) {
            respond eventoContactoInstance.errors, view:'create'
            return
        }

        eventoContactoInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'eventoContacto.label', default: 'EventoContacto'), eventoContactoInstance.id])
                redirect eventoContactoInstance
            }
            '*' { respond eventoContactoInstance, [status: CREATED] }
        }
    }

    def edit(EventoContacto eventoContactoInstance) {
        respond eventoContactoInstance
    }

    @Transactional
    def update(EventoContacto eventoContactoInstance) {
        if (eventoContactoInstance == null) {
            notFound()
            return
        }

        if (eventoContactoInstance.hasErrors()) {
            respond eventoContactoInstance.errors, view:'edit'
            return
        }

        eventoContactoInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'EventoContacto.label', default: 'EventoContacto'), eventoContactoInstance.id])
                redirect eventoContactoInstance
            }
            '*'{ respond eventoContactoInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(EventoContacto eventoContactoInstance) {

        if (eventoContactoInstance == null) {
            notFound()
            return
        }

        eventoContactoInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'EventoContacto.label', default: 'EventoContacto'), eventoContactoInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'eventoContacto.label', default: 'EventoContacto'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
