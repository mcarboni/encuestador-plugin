package encuestador


import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class EdicionController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Edicion.list(params), model:[edicionInstanceCount: Edicion.count()]
    }

    def show(Edicion edicionInstance) {
        respond edicionInstance
    }

    def create() {
        respond new Edicion(params)
    }

    @Transactional
    def save(Edicion edicionInstance) {
        if (edicionInstance == null) {
            notFound()
            return
        }

        if (edicionInstance.hasErrors()) {
            respond edicionInstance.errors, view:'create'
            return
        }

        edicionInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'edicion.label', default: 'Edicion'), edicionInstance.id])
                redirect edicionInstance
            }
            '*' { respond edicionInstance, [status: CREATED] }
        }
    }

    def edit(Edicion edicionInstance) {
        respond edicionInstance
    }

    @Transactional
    def update(Edicion edicionInstance) {
        if (edicionInstance == null) {
            notFound()
            return
        }

        if (edicionInstance.hasErrors()) {
            respond edicionInstance.errors, view:'edit'
            return
        }

        edicionInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'edicion.label', default: 'Edicion'), edicionInstance.id])
                redirect edicionInstance
            }
            '*'{ respond edicionInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Edicion edicionInstance) {

        if (edicionInstance == null) {
            notFound()
            return
        }

        edicionInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'edicion.label', default: 'Edicion'), edicionInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'edicion.label', default: 'Edicion'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
