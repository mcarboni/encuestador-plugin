package encuestador



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class PuntoDeContactoController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond PuntoDeContacto.list(params), model:[puntoDeContactoInstanceCount: PuntoDeContacto.count()]
    }

    def show(PuntoDeContacto puntoDeContactoInstance) {
        respond puntoDeContactoInstance
    }

    def create() {
        respond new PuntoDeContacto(params)
    }

    @Transactional
    def save(PuntoDeContacto puntoDeContactoInstance) {
        if (puntoDeContactoInstance == null) {
            notFound()
            return
        }

        if (puntoDeContactoInstance.hasErrors()) {
            respond puntoDeContactoInstance.errors, view:'create'
            return
        }

        puntoDeContactoInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'puntoDeContacto.label', default: 'PuntoDeContacto'), puntoDeContactoInstance.id])
                redirect puntoDeContactoInstance
            }
            '*' { respond puntoDeContactoInstance, [status: CREATED] }
        }
    }

    def edit(PuntoDeContacto puntoDeContactoInstance) {
        respond puntoDeContactoInstance
    }

    @Transactional
    def update(PuntoDeContacto puntoDeContactoInstance) {
        if (puntoDeContactoInstance == null) {
            notFound()
            return
        }

        if (puntoDeContactoInstance.hasErrors()) {
            respond puntoDeContactoInstance.errors, view:'edit'
            return
        }

        puntoDeContactoInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'PuntoDeContacto.label', default: 'PuntoDeContacto'), puntoDeContactoInstance.id])
                redirect puntoDeContactoInstance
            }
            '*'{ respond puntoDeContactoInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(PuntoDeContacto puntoDeContactoInstance) {

        if (puntoDeContactoInstance == null) {
            notFound()
            return
        }

        puntoDeContactoInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'PuntoDeContacto.label', default: 'PuntoDeContacto'), puntoDeContactoInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'puntoDeContacto.label', default: 'PuntoDeContacto'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
