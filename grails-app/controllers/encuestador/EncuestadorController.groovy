package encuestador



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class EncuestadorController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Encuestador.list(params), model:[encuestadorInstanceCount: Encuestador.count()]
    }

    def show(Encuestador encuestadorInstance) {
        respond encuestadorInstance
    }

    def create() {
        respond new Encuestador(params)
    }

    @Transactional
    def save(Encuestador encuestadorInstance) {
        if (encuestadorInstance == null) {
            notFound()
            return
        }

        if (encuestadorInstance.hasErrors()) {
            respond encuestadorInstance.errors, view:'create'
            return
        }

        encuestadorInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'encuestador.label', default: 'Encuestador'), encuestadorInstance.id])
                redirect encuestadorInstance
            }
            '*' { respond encuestadorInstance, [status: CREATED] }
        }
    }

    def edit(Encuestador encuestadorInstance) {
        respond encuestadorInstance
    }

    @Transactional
    def update(Encuestador encuestadorInstance) {
        if (encuestadorInstance == null) {
            notFound()
            return
        }

        if (encuestadorInstance.hasErrors()) {
            respond encuestadorInstance.errors, view:'edit'
            return
        }

        encuestadorInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Encuestador.label', default: 'Encuestador'), encuestadorInstance.id])
                redirect encuestadorInstance
            }
            '*'{ respond encuestadorInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Encuestador encuestadorInstance) {

        if (encuestadorInstance == null) {
            notFound()
            return
        }

        encuestadorInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Encuestador.label', default: 'Encuestador'), encuestadorInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'encuestador.label', default: 'Encuestador'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
