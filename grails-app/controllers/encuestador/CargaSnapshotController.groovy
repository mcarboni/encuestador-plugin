package encuestador



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class CargaSnapshotController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond CargaSnapshot.list(params), model:[cargaSnapshotInstanceCount: CargaSnapshot.count()]
    }

    def show(CargaSnapshot cargaSnapshotInstance) {
        respond cargaSnapshotInstance
    }

    def create() {
        respond new CargaSnapshot(params)
    }

    @Transactional
    def save(CargaSnapshot cargaSnapshotInstance) {
        if (cargaSnapshotInstance == null) {
            notFound()
            return
        }

        if (cargaSnapshotInstance.hasErrors()) {
            respond cargaSnapshotInstance.errors, view:'create'
            return
        }

        cargaSnapshotInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'cargaSnapshot.label', default: 'CargaSnapshot'), cargaSnapshotInstance.id])
                redirect cargaSnapshotInstance
            }
            '*' { respond cargaSnapshotInstance, [status: CREATED] }
        }
    }

    def edit(CargaSnapshot cargaSnapshotInstance) {
        respond cargaSnapshotInstance
    }

    @Transactional
    def update(CargaSnapshot cargaSnapshotInstance) {
        if (cargaSnapshotInstance == null) {
            notFound()
            return
        }

        if (cargaSnapshotInstance.hasErrors()) {
            respond cargaSnapshotInstance.errors, view:'edit'
            return
        }

        cargaSnapshotInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'CargaSnapshot.label', default: 'CargaSnapshot'), cargaSnapshotInstance.id])
                redirect cargaSnapshotInstance
            }
            '*'{ respond cargaSnapshotInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(CargaSnapshot cargaSnapshotInstance) {

        if (cargaSnapshotInstance == null) {
            notFound()
            return
        }

        cargaSnapshotInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'CargaSnapshot.label', default: 'CargaSnapshot'), cargaSnapshotInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'cargaSnapshot.label', default: 'CargaSnapshot'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
