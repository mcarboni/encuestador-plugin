package encuestador



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class CargaFormularioController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond CargaFormulario.list(params), model:[cargaFormularioInstanceCount: CargaFormulario.count()]
    }

    def show(CargaFormulario cargaFormularioInstance) {
        respond cargaFormularioInstance
    }

    def create() {
        respond new CargaFormulario(params)
    }

    @Transactional
    def save(CargaFormulario cargaFormularioInstance) {
        if (cargaFormularioInstance == null) {
            notFound()
            return
        }

        if (cargaFormularioInstance.hasErrors()) {
            respond cargaFormularioInstance.errors, view:'create'
            return
        }

        cargaFormularioInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'cargaFormulario.label', default: 'CargaFormulario'), cargaFormularioInstance.id])
                redirect cargaFormularioInstance
            }
            '*' { respond cargaFormularioInstance, [status: CREATED] }
        }
    }

    def edit(CargaFormulario cargaFormularioInstance) {
        respond cargaFormularioInstance
    }

    @Transactional
    def update(CargaFormulario cargaFormularioInstance) {
        if (cargaFormularioInstance == null) {
            notFound()
            return
        }

        if (cargaFormularioInstance.hasErrors()) {
            respond cargaFormularioInstance.errors, view:'edit'
            return
        }

        cargaFormularioInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'CargaFormulario.label', default: 'CargaFormulario'), cargaFormularioInstance.id])
                redirect cargaFormularioInstance
            }
            '*'{ respond cargaFormularioInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(CargaFormulario cargaFormularioInstance) {

        if (cargaFormularioInstance == null) {
            notFound()
            return
        }

        cargaFormularioInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'CargaFormulario.label', default: 'CargaFormulario'), cargaFormularioInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'cargaFormulario.label', default: 'CargaFormulario'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }

    def preview(CargaFormulario cargaFormularioInstance) {
        def Formulario f = cargaFormularioInstance.formulario

        def preview = '<div>'+f.templateVista+'</div>'


        respond cargaFormularioInstance, model: [preview: preview]
    }
}
