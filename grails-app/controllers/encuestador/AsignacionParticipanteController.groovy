package encuestador



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class AsignacionParticipanteController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond AsignacionParticipante.list(params), model:[asignacionParticipanteInstanceCount: AsignacionParticipante.count()]
    }

    def show(AsignacionParticipante asignacionParticipanteInstance) {
        respond asignacionParticipanteInstance
    }

    def create() {
        respond new AsignacionParticipante(params)
    }

    @Transactional
    def save(AsignacionParticipante asignacionParticipanteInstance) {
        if (asignacionParticipanteInstance == null) {
            notFound()
            return
        }

        if (asignacionParticipanteInstance.hasErrors()) {
            respond asignacionParticipanteInstance.errors, view:'create'
            return
        }

        asignacionParticipanteInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'asignacionParticipante.label', default: 'AsignacionParticipante'), asignacionParticipanteInstance.id])
                redirect asignacionParticipanteInstance
            }
            '*' { respond asignacionParticipanteInstance, [status: CREATED] }
        }
    }

    def edit(AsignacionParticipante asignacionParticipanteInstance) {
        respond asignacionParticipanteInstance
    }

    @Transactional
    def update(AsignacionParticipante asignacionParticipanteInstance) {
        if (asignacionParticipanteInstance == null) {
            notFound()
            return
        }

        if (asignacionParticipanteInstance.hasErrors()) {
            respond asignacionParticipanteInstance.errors, view:'edit'
            return
        }

        asignacionParticipanteInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'AsignacionParticipante.label', default: 'AsignacionParticipante'), asignacionParticipanteInstance.id])
                redirect asignacionParticipanteInstance
            }
            '*'{ respond asignacionParticipanteInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(AsignacionParticipante asignacionParticipanteInstance) {

        if (asignacionParticipanteInstance == null) {
            notFound()
            return
        }

        asignacionParticipanteInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'AsignacionParticipante.label', default: 'AsignacionParticipante'), asignacionParticipanteInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'asignacionParticipante.label', default: 'AsignacionParticipante'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
