package encuestador



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class ListaControladaController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond ListaControlada.list(params), model:[listaControladaInstanceCount: ListaControlada.count()]
    }

    def show(ListaControlada listaControladaInstance) {
        respond listaControladaInstance
    }

    def create() {
        respond new ListaControlada(params)
    }

    @Transactional
    def save(ListaControlada listaControladaInstance) {
        if (listaControladaInstance == null) {
            notFound()
            return
        }

        if (listaControladaInstance.hasErrors()) {
            respond listaControladaInstance.errors, view:'create'
            return
        }

        listaControladaInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'listaControlada.label', default: 'ListaControlada'), listaControladaInstance.id])
                redirect listaControladaInstance
            }
            '*' { respond listaControladaInstance, [status: CREATED] }
        }
    }

    def edit(ListaControlada listaControladaInstance) {
        respond listaControladaInstance
    }

    @Transactional
    def update(ListaControlada listaControladaInstance) {
        if (listaControladaInstance == null) {
            notFound()
            return
        }

        if (listaControladaInstance.hasErrors()) {
            respond listaControladaInstance.errors, view:'edit'
            return
        }

        listaControladaInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'ListaControlada.label', default: 'ListaControlada'), listaControladaInstance.id])
                redirect listaControladaInstance
            }
            '*'{ respond listaControladaInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(ListaControlada listaControladaInstance) {

        if (listaControladaInstance == null) {
            notFound()
            return
        }

        listaControladaInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'ListaControlada.label', default: 'ListaControlada'), listaControladaInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'listaControlada.label', default: 'ListaControlada'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
