package encuestador



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class EventoParticipacionController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond EventoParticipacion.list(params), model:[eventoParticipacionInstanceCount: EventoParticipacion.count()]
    }

    def show(EventoParticipacion eventoParticipacionInstance) {
        respond eventoParticipacionInstance
    }

    def create() {
        respond new EventoParticipacion(params)
    }

    @Transactional
    def save(EventoParticipacion eventoParticipacionInstance) {
        if (eventoParticipacionInstance == null) {
            notFound()
            return
        }

        if (eventoParticipacionInstance.hasErrors()) {
            respond eventoParticipacionInstance.errors, view:'create'
            return
        }

        eventoParticipacionInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'eventoParticipacion.label', default: 'EventoParticipacion'), eventoParticipacionInstance.id])
                redirect eventoParticipacionInstance
            }
            '*' { respond eventoParticipacionInstance, [status: CREATED] }
        }
    }

    def edit(EventoParticipacion eventoParticipacionInstance) {
        respond eventoParticipacionInstance
    }

    @Transactional
    def update(EventoParticipacion eventoParticipacionInstance) {
        if (eventoParticipacionInstance == null) {
            notFound()
            return
        }

        if (eventoParticipacionInstance.hasErrors()) {
            respond eventoParticipacionInstance.errors, view:'edit'
            return
        }

        eventoParticipacionInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'EventoParticipacion.label', default: 'EventoParticipacion'), eventoParticipacionInstance.id])
                redirect eventoParticipacionInstance
            }
            '*'{ respond eventoParticipacionInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(EventoParticipacion eventoParticipacionInstance) {

        if (eventoParticipacionInstance == null) {
            notFound()
            return
        }

        eventoParticipacionInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'EventoParticipacion.label', default: 'EventoParticipacion'), eventoParticipacionInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'eventoParticipacion.label', default: 'EventoParticipacion'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
