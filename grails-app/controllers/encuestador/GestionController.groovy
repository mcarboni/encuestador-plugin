package encuestador

import grails.transaction.Transactional
import org.codehaus.groovy.transform.tailrec.TailRecursiveASTTransformation

class GestionController {

    def index() { }

    def encuesta(Encuesta encuestaInstance) {
        respond encuestaInstance
    }

    @Transactional
    def nuevaEdicion() {
        def encuestaInstance = Encuesta.get(params.long('encuesta_id'))
        def anio = params.int('anio')
        def copiarFormularios = params.boolean('copiarFormularios')
        def copiarParticipantes = params.boolean('copiarParticipantes')
        def ultimaEdicion = encuestaInstance.ediciones.last()

        if (Edicion.findByEncuestaAndAnio(encuestaInstance, anio)) {
            flash.message = "Ya existe una edicion para $anio"
        } else {
            def transaction = Edicion.withTransaction {

                Edicion edicion = new Edicion(ultimaEdicion.properties.findAll {it.key != 'formularios'})
                edicion.nombre = "${encuestaInstance.nombre} ${anio}"
                edicion.codigo = "${encuestaInstance.codigo}${anio}"
                edicion.anio = anio
                edicion.save flush: true
                if (copiarFormularios) {
                    Formulario.findAllByEdicion(ultimaEdicion).each { f->
                        def fNuevo = new Formulario(f.properties.findAll { it.key != 'edicion'})
                        fNuevo.edicion = edicion
                        fNuevo.save flush: true
                    }
                }
                if (copiarParticipantes) {
                    ParticipaEn.findAllByEdicion(ultimaEdicion).each { p ->
                        def pNuevo = new ParticipaEn(empresa: p.empresa, edicion: edicion)
                        pNuevo.save flush: true
                    }
                }
            }

            flash.message = "Se ha creado la edición $anio"
        }
        redirect (action: 'encuesta', id: params.encuesta_id )
    }

}
