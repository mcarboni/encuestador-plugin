package encuestador.security



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class PersonAuthorityController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond PersonAuthority.list(params), model:[personAuthorityInstanceCount: PersonAuthority.count()]
    }

    def show(PersonAuthority personAuthorityInstance) {
        respond personAuthorityInstance
    }

    def create() {
        respond new PersonAuthority(params)
    }

    @Transactional
    def save(PersonAuthority personAuthorityInstance) {
        if (personAuthorityInstance == null) {
            notFound()
            return
        }

        if (personAuthorityInstance.hasErrors()) {
            respond personAuthorityInstance.errors, view:'create'
            return
        }

        personAuthorityInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'personAuthority.label', default: 'PersonAuthority'), personAuthorityInstance.person.username])
                redirect personAuthorityInstance.person
            }
            '*' { respond personAuthorityInstance, [status: CREATED] }
        }
    }

    def edit(PersonAuthority personAuthorityInstance) {
        respond personAuthorityInstance
    }

    @Transactional
    def update(PersonAuthority personAuthorityInstance) {
        if (personAuthorityInstance == null) {
            notFound()
            return
        }

        if (personAuthorityInstance.hasErrors()) {
            respond personAuthorityInstance.errors, view:'edit'
            return
        }

        personAuthorityInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'PersonAuthority.label', default: 'PersonAuthority'), personAuthorityInstance.id])
                redirect personAuthorityInstance
            }
            '*'{ respond personAuthorityInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(PersonAuthority personAuthorityInstance) {

        if (personAuthorityInstance == null) {
            notFound()
            return
        }

        personAuthorityInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'PersonAuthority.label', default: 'PersonAuthority'), personAuthorityInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'personAuthority.label', default: 'PersonAuthority'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
