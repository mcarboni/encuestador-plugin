package encuestador



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class ContactoTemplateController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond ContactoTemplate.list(params), model:[contactoTemplateInstanceCount: ContactoTemplate.count()]
    }

    def show(ContactoTemplate contactoTemplateInstance) {
        respond contactoTemplateInstance
    }

    def create() {
        respond new ContactoTemplate(params)
    }

    @Transactional
    def save(ContactoTemplate contactoTemplateInstance) {
        if (contactoTemplateInstance == null) {
            notFound()
            return
        }

        if (contactoTemplateInstance.hasErrors()) {
            respond contactoTemplateInstance.errors, view:'create'
            return
        }

        contactoTemplateInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'contactoTemplate.label', default: 'ContactoTemplate'), contactoTemplateInstance.id])
                redirect contactoTemplateInstance
            }
            '*' { respond contactoTemplateInstance, [status: CREATED] }
        }
    }

    def edit(ContactoTemplate contactoTemplateInstance) {
        respond contactoTemplateInstance
    }

    @Transactional
    def update(ContactoTemplate contactoTemplateInstance) {
        if (contactoTemplateInstance == null) {
            notFound()
            return
        }

        if (contactoTemplateInstance.hasErrors()) {
            respond contactoTemplateInstance.errors, view:'edit'
            return
        }

        contactoTemplateInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'ContactoTemplate.label', default: 'ContactoTemplate'), contactoTemplateInstance.id])
                redirect contactoTemplateInstance
            }
            '*'{ respond contactoTemplateInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(ContactoTemplate contactoTemplateInstance) {

        if (contactoTemplateInstance == null) {
            notFound()
            return
        }

        contactoTemplateInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'ContactoTemplate.label', default: 'ContactoTemplate'), contactoTemplateInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'contactoTemplate.label', default: 'ContactoTemplate'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
