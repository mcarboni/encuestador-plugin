package encuestador.api

import encuestador.EstadoEncuesta;
import encuestador.ListaControlada
import encuestador.ListaService
import encuestador.ValorListaControlada

class ListasController {

    static namespace = "api"

    static responseFormats = ['json', 'xml']
    def listaService
    def empresaService

    def index() {
        if (params.id) {
            def valoresBuscados
            if (params.discriminador) {
                valoresBuscados = ValorListaControlada.createCriteria().list {
                    lista {
                        eq('codigo', params.id)
                    }
                    eq('discriminador', params.discriminador)
                    order("orden")
                }
            } else {
                valoresBuscados = ValorListaControlada.createCriteria().list {
                    lista {
                        eq('codigo', params.id)
                    }
                    order("orden")
                }
            }

            respond valoresBuscados.collect { mappearValor(it) }
        } else {
            render(contentType: "text/json") {
                [mensaje: 'quien es?', parametros: params]
            }
        }
    }
	
	def estados() {		
		def estados = EstadoEncuesta.values()
		respond estados*.name()
	}

    def buscarElementos() {
        def busqueda = listaService.buscarEnLista(params.lista, params.q)

        respond busqueda.collect { mappearValor(it) }
    }

    def buscarElemento() {
        def elemento = listaService.traerElementoDeLista(params.lista, params.codigo)

        respond mappearValor(elemento)
    }

    private def mappearValor(ValorListaControlada v) {
        [
          codigo: v.codigo,
          descripcion: v.descripcion,
          discriminador: v.discriminador
        ]
    }
}
