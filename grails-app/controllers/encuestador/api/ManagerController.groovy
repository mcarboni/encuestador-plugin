package encuestador.api

import encuestador.Base
import encuestador.CargaSnapshot
import encuestador.Empresa
import encuestador.Encuesta

import java.text.NumberFormat

import encuestador.AsignacionParticipante
import encuestador.CargaFormulario
import encuestador.Edicion
import encuestador.Encuestador
import encuestador.EstadoEncuesta
import encuestador.EventoParticipacion
import encuestador.Formulario
import encuestador.ParticipaEn
import encuestador.TipoPuntoDeContacto
import encuestador.security.Person
import grails.converters.JSON
import grails.transaction.Transactional
import groovy.json.JsonSlurper

class ManagerController {

	static namespace = "api"

	static responseFormats = ['json', 'xml']

	def springSecurityService

	def validationService

	def managerService

	static allowedMethods = [nuevaEmpresa:'POST', reabrir: 'POST', validar: 'POST', observar: 'POST', declinar: 'POST', asignarEncuestadorA:'POST', contactar: 'POST', nuevoEvento:'POST']

	def ediciones() {
		respond  Edicion.list(params).collect {  mappearEdicionResumida(it)  }
	}

	def empresas() {
		def user = springSecurityService.currentUser
		params.max = Math.min(params.max ?: 10, 100)
		params.offset = params.offset?: 0
		def todos = managerService.empresas(user, params)
		respond ([rows: todos, totalCount: todos.totalCount])
	}

	def edicionesPorEmpresa() {
		def user = springSecurityService.currentUser
		def todos = managerService.edicionesPorEmpresa(user, params)
		respond ([rows: todos.collect{ mappearEdicion(it) }, totalCount: todos.totalCount])
	}

	private def mappearEdicion(ParticipaEn pe) {
		[
			'anio': pe.edicion.anio,
			'nombre': pe.edicion.nombre,
			'encuesta': pe.edicion.encuesta.nombre,
			'codigoEncuesta': pe.edicion.encuesta.codigo,
			'participaEnId': pe.id,
			'codigo': pe.edicion.codigo,
			'estado': pe.estadoBase1?.name(),
			'contexto': pe.contexto,
			'fechaEnvio': pe.fechaEnvio,
			'resultadoValidacion': pe.resultadoValidacion
		]
	}

	def empresasPorEncuesta() {
		def user = springSecurityService.currentUser
		params.max = Math.min(params.max ?: 10, 100)
		params.offset = params.offset?: 0
		def codigo = params.id
		//		if ( !codigo )
		//			codigo = Edicion.

		def todos = managerService.empresasParaUsuarioYEncuesta(user, codigo, params)

		if (codigo) {
			respond ([rows: todos.collect { mappearEmpresa(it) }, totalCount: todos.totalCount])
		}
	}

	def exportarDatos() {
		def user = springSecurityService.currentUser
		def codigo = params.id
		def emps = managerService.empresasParaUsuarioYEncuesta(user, codigo, params)

		respond emps.collect { generarFilaExportacion(it, true) }
	}

	def exportar() {
		def user = springSecurityService.currentUser
		def codigo = params.id
		def emps = managerService.empresasParaUsuarioYEncuesta(user, codigo, params)

		respond emps.collect { generarFilaExportacion(it, false) }
	}

	def exportarEventos() {
		def user = springSecurityService.currentUser
		def codigo = params.id
		def evs = managerService.eventosPorEncuesta(codigo, params)

		respond evs.collect { generarFilaEvento(it) }
	}

	def camposCache = [:]

	/**
	 * dado un formulario obtiene el array de campos. este se guarda como array json. este método lo parsea. esta cacheado
	 * @param f
	 * @return
	 */
	def buscarCamposEnCache(Formulario f) {
		def campos = camposCache[f.id]
		if (!campos) {
			campos = Eval.me(f.campos)
			camposCache[f.id] = campos
		}
		campos
	}

	def generarFilaExportacion(ParticipaEn p, boolean tambienDatos) {

		def r = ['id': p.empresa.empresaId, 'cuit': p.empresa.cuit?.replaceAll("-",""), 'empresa': p.empresa.nombre, 'abierto': p.fechaApertura, 'encuestador Asignado': encuestadorAsignado(p)?.user?.username ]
		def ctx = new JsonSlurper().parseText(p.contexto)
		r['Reporta no hacer I+D'] = ctx.noHaceID ?: ''
		p.edicion.formularios.each { f ->
			def CargaFormulario cf = CargaFormulario.findByFormularioAndParticipante(f,p)
			if (tambienDatos){
				def campos = buscarCamposEnCache(f)
				def d = JSON.parse(cf?.datos?:'{}')
				campos.each{ nombreCampo ->
					r["${f.tabla}_$nombreCampo"] = generarCampo(d ? d[nombreCampo] : '')
				}
				//campos.each { nombreCampo -> r["${f.tabla}_$nombreCampo"] = d ? d[nombreCampo]: '' }
			}
			r[f.tabla + ' estado'] = cf?.estado?.name()
			r[f.tabla + ' comentarios'] = cf?.resultadoValidacion?.toString()
			r[f.tabla + ' ult. modif.'] = cf?.actualizado
		}
		r.cierre = p.fechaEnvio
		r.estado = p.estadoBase1?.name()
		r.comentarios = p.resultadoValidacion?.toString()
		r
	}

	def generarFilaEvento(EventoParticipacion e) {
		def r = ['id': e.participacion.empresa.empresaId, 'empresa': e.participacion.empresa.nombre, 'usuario': e.usuario.username, 'fecha': e.fecha.format("dd/MM/yyyy HH:mm"), 'estado': e.estadoDestino.toString(), 'descripcion': e.observacion ]
		r
	}

	def generarCampo(valor) {
		if (Double.class.isInstance(valor)) {
			def nf = NumberFormat.getNumberInstance(Locale.getInstance("es", "AR", ""))
			valor = nf.format(valor)
		}
		valor
	}

	@Transactional(readOnly = true)
	def participante() {
		def user = springSecurityService.currentUser
		def id = params.id
		if (id) {
			def p = ParticipaEn.get(id)

			respond mappearParticipante(p)
		}
	}

	@Transactional(readOnly=false)
	def nuevaEmpresa() {
		def currentUser = springSecurityService.currentUser
		//TODO: validar rol DNIC
		def datos = request.JSON
		def resp = managerService.nuevaEmpresa(datos)
		respond resp
	}

	@Transactional(readOnly=false)
	def asignarEncuestadorA() {
		def currentUser = springSecurityService.currentUser
		//TODO: validar rol DNIC y SUPERVISOR_FOP
		def datos = request.JSON
		def resp = managerService.asignarEncuestadorA(currentUser, datos)
		respond resp
	}

	@Transactional
	def reabrir() {
		def user = springSecurityService.currentUser
		def id = params.id
		def participante = ParticipaEn.get(id)

		def r = managerService.cambiarEstado(user, participante, EstadoEncuesta.REABIERTA, params)
		respond r
	}

	@Transactional
	def validar() {
		def user = springSecurityService.currentUser
		def id = params.id
		def participante = ParticipaEn.get(id)

		def r = managerService.cambiarEstado(user, participante, EstadoEncuesta.VALIDADA, params)
		respond r
	}

	@Transactional(readOnly=false)
	def nuevoEvento() {
		def resp = [ 'estado': 'OK' ]
		def user = springSecurityService.currentUser
		def id = params.id
		def datos = request.JSON
		def elParticipante = ParticipaEn.get(id)
		def ep = new EventoParticipacion(
				participacion: elParticipante,
				base: Base.BASE_UNO,
				estadoOrigen: elParticipante.estadoBase1,
				estadoDestino: elParticipante.estadoBase1,
				fecha: new Date(),
				usuario: user,
				observacion: datos.observacion
				)
		ep.save( flush:  true)

		if (elParticipante.hasErrors()) {
			resp['estado'] = 'error'
			resp['mensaje'] = elParticipante.errors
		} else {
			resp['evento'] = mappearEvento(ep)
			resp['mensaje'] = 'Se registró el evento.'
		}
		respond resp
	}

	@Transactional(readOnly=false)
	def declinar() {
		def user = springSecurityService.currentUser
		def id = params.id
		def participante = ParticipaEn.get(id)
		//TODO: validar rol DNIC
		def resp = managerService.cambiarEstado(user, participante, EstadoEncuesta.DECLINADA, params)
		respond resp
	}

	def carga() {
		def currentUser = springSecurityService.currentUser
		def id = params.id
		//TODO: validar rol DNIC
		def laCarga = CargaFormulario.get(id)
		respond mappearCarga ( laCarga )
	}

	private def mappearCarga(CargaFormulario it) {
		if (it) {
			[
				titulo: "${it.formulario?.orden}. ${it.formulario?.titulo}",
				template: it.formulario?.templateVista,
				contexto: JSON.parse(it.participante.contexto),
				encuesta: [ anio: it.formulario.edicion.anio ],
				datos: JSON.parse(it.datos),
				resultadoValidacion: JSON.parse(it.resultadoValidacion),
				snapshots: CargaSnapshot.findAllByCarga(it, [sort: 'creado', order: 'desc']).collect { mappearSnapshot(it) }
			]
		}
	}

	private def mappearSnapshot(CargaSnapshot it) {
		if (it) {
			[
				fecha: it.creado,
				datos: it.datos
			]
		}
	}

	@Transactional(readOnly=false)
	def observar() {
		def user = springSecurityService.currentUser
		def id = params.id
		def participante = ParticipaEn.get(id)
		//TODO: validar rol DNIC
		def resp = managerService.cambiarEstado(user, participante, EstadoEncuesta.OBSERVADA, params)
		respond resp
	}

	def supervisores() {
		def list = Person.where { authorities =~ 'ROLE_FOP' }

		respond list
	}

	def encuestadores() {
		def enqs = Encuestador.findAll().collect { e -> mappearEncuestador(e) }

		respond enqs
	}

	def puntosDeContactoDeEmpresa() {
		def puntos = ParticipaEn.get(params.id).empresa.puntosDeContacto

		respond puntos
				.findAll { it.tipo == TipoPuntoDeContacto.EMAIL }
				.collect { p ->
					[
						puntoId: p.id,
						nombre: p.persona,
						email: p.contacto,
						posicion: p.posicion
					]
				}
	}

	def contactar() {
		//TODO: agregar validaciones de tipo de usuario + asignacion del contacto
		def currentUser = springSecurityService.currentUser

		respond managerService.contactar(currentUser, request.JSON)
	}

	def encuestadorAsignado = { p -> AsignacionParticipante.findByParticipante(p)*.encuestador?.first() }

	private def mappearParticipante(ParticipaEn p) {
		[
			id: p.id,
			empresa: p.empresa.nombre,
			empresaId: p.empresa.empresaId,
			contexto: JSON.parse(p.contexto?:'{}'),
			encuesta: p.edicion.nombre,
			estado: p.estadoBase1?.name(),
			anio: p.edicion.anio,
			relevadaExternamente: p.relevadaExternamente,
			encuestadorAsignado: encuestadorAsignado(p)?.id,
			formularios: p.edicion.formularios.collect { mappearFormulario(it, p) },
			eventos: EventoParticipacion.findAllByParticipacion(p).collect { mappearEvento(it) }
		]
	}

	private def mappearEvento(EventoParticipacion ep) {
		[
			usuario: ep.usuario.username,
			fecha: ep.fecha,
			estado: ep.estadoDestino.name(),
			observacion: ep.observacion
		]
	}

	private def mappearEncuestador(Encuestador encuestador) {
		[
			'id': encuestador.id,
			'nombre': encuestador.nombre,
			'email': encuestador.email,
			'usuario':encuestador.user.username
		]
	}

	private def mappearFormulario(Formulario f, ParticipaEn p) {
		def carga = CargaFormulario.findByFormularioAndParticipante(f, p)
		return [
			titulo: f.titulo,
			estado: carga?.estado?.name(),
			resultadoValidacion: carga?.resultadoValidacion ? JSON.parse(carga?.resultadoValidacion) : null,
			actualizado: carga?.actualizado,
			cargaId: carga?.id
		]
	}

	private def mappearEmpresa(ParticipaEn e) {
		[
			id: e.id,
			nombre: e.empresa.nombre,
			empresaId: e.empresa.empresaId,
			cuit: e.empresa.cuit,
			estado: e.estadoBase1.name(),
			relevadaExternamente: e.relevadaExternamente
		]
	}

	private def mappearEdicionResumida(Edicion enq) {
		[
			id: enq.codigo,
			nombre: enq.nombre,
			encuesta: enq.encuesta.nombre
		]
	}

	def encuestas() {

		def encuestas = Edicion.createCriteria().list {
			eq('habilitado', true)
			encuesta { eq('habilitado', true) }
			order('anio', 'asc')
		}

		respond encuestas.collect { mappearEdicionResumida(it) }
	}

}
