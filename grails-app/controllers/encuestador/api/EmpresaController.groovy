package encuestador.api

import encuestador.CargaFormulario
import encuestador.Edicion
import encuestador.EstadoCarga
import encuestador.EstadoEncuesta
import encuestador.ParticipaEn

import grails.converters.JSON
import grails.transaction.Transactional

class EmpresaController {

	static namespace = "api"

	static responseFormats = ['json', 'xml']

	def empresaService

	def validationService

	def springSecurityService

	static allowedMethods = [guardarFormulario: 'POST', enviarEncuesta: 'POST', abrirEncuesta: 'POST']

    def encuesta() {
        def user = springSecurityService.currentUser
        def id = params.id

		// agrupo por encuesta y luego mapeo para obtener los campos que necesito presentarle al visor.
		def encuestas = empresaService.buscarEncuestas(user, id).groupBy { p -> p.edicion.encuesta  }.collect { p ->
			[
				'nombre' : p.key.nombre,
				'codigo' : p.key.codigo,
				'edicionesActivas' : p.value.sort {it.edicion.anio}.reverse().collect { mappear(it) }
			]
		}

		respond encuestas
    }

    def edicion() {
		def user = springSecurityService.currentUser
		def id = params.id
		if (!id) {
			respond empresaService.buscar(user).collect { mappear(it) }
		}
		respond empresaService.buscar(user, id).collect { mappear(it) }
	}

	def formulario() {
		def carga = empresaService.buscarOCrearFormulario(springSecurityService.currentUser, params.id?.toLong())
		def elJson = mappearFormulario ( carga )
		respond elJson
	}

	def imprimirFormulario() {
		def carga = empresaService.buscarOCrearFormulario(springSecurityService.currentUser, params.id?.toLong())
		respond mappearFormulario ( carga, false )
	}

	private def maximoEntre(Date fecha1, Date fecha2) {
		if (!fecha1)
			return fecha2
		else {
			if (fecha1.before(fecha2)) {
				return fecha2
			}
		}
		return fecha1
	}

	def imprimirEncuesta() {
		def ultimaFecha
		def formularios = Edicion.findByCodigo(params.id).formularios.collect { f ->
			def CargaFormulario carga = empresaService.buscarOCrearFormulario(springSecurityService.currentUser, f.id)
			if (carga.id) {
				ultimaFecha = maximoEntre(ultimaFecha, carga.actualizado)
			}
			mappearFormulario(carga, false)
		}
		def ParticipaEn p = empresaService.buscar(springSecurityService.currentUser, params.id)

		def impresion = [formularios : formularios, estado: p.estadoBase1.name(), ultimaModificacion : ultimaFecha, anio : p.edicion.anio ]
		respond impresion
	}

	@Transactional
	def guardarFormulario() {
		def id = request.JSON.formulario?.toLong()
		def datos = request.JSON.datos
		def user = springSecurityService.currentUser
		CargaFormulario carga

		if (id) {
			carga = empresaService.buscarOCrearFormulario(user, id)

			carga.actualizar(user.username, datos)

			def validacion = validationService.validarCargaFormulario(carga)
			if (!validacion.esValido) {
				carga.estado = EstadoCarga.P
				carga.resultadoValidacion = new JSON(validacion.mensajes)
				carga.participante.contexto = new JSON(validacion.contexto)
				carga.save(flush: true)
				respond resultado('warning', validacion.mensajes)
				return
			} else {
				carga.resultadoValidacion = '[]'
				carga.participante.contexto = new JSON(validacion.contexto)
				carga.estado = EstadoCarga.C
				carga.save(flush: true)
			}
			def p = carga.participante
			def validacionEncuesta = validationService.validarEncuesta(p)
			p.resultadoValidacion = new JSON(validacionEncuesta.mensajes)
		} else {
			response.status = 400
		}
		// respond mappearFormulario(carga)
		respond resultado('success', 'Los datos han sido guardados.')
	}

	def validarEncuesta() {
		def user = springSecurityService.currentUser
		def id = params.id
		def participante = empresaService.buscar(user, id)
		def r = validationService.validarEncuesta(participante)
		respond r
	}

	@Transactional
	def abrirEncuesta() {
		def user = springSecurityService.currentUser
		def id = params.id
		def participante = empresaService.buscar(user, id)

		def r = empresaService.cambiarEstado(user, participante, EstadoEncuesta.ABIERTA)
		respond r
	}

	@Transactional
	def iniciarSesion() {
		def user = springSecurityService.currentUser
		def id = params.id
		def ParticipaEn participante = empresaService.buscar(user, id)
/*
		def Encuesta encuesta = participante.edicion.encuesta
		def anotaciones = JSON.parse(participante.empresa.anotaciones?: '{}')
		if (anotaciones?.anioInicioActividades) {
			def anio = anotaciones?.anioInicioActividades

			encuesta.ediciones.each {
				if (it.anio >= anio) {
					def p = ParticipaEn.findByEdicionAndEmpresa(it, participante.empresa)
					def mostrarMenos2 = anio <= it.anio - 2
					def mostrarMenos1 = anio <= it.anio - 1
					if (!p) {
						new ParticipaEn(
								empresa: participante.empresa,
								edicion: it,
								contexto: '{"mostrarActualMenos2": '+ mostrarMenos2 +', "mostrarActualMenos1": '+ mostrarMenos1 +'}'
						).save(flush:true)
					} else {
						def pCtx = JSON.parse(p.contexto?:'{}')
						pCtx['mostrarActualMenos2'] = mostrarMenos2
						pCtx['mostrarActualMenos1'] = mostrarMenos1
						p.contexto = pCtx
						p.save(flush:true)
					}
				}
			}

		}
*/
		respond participante
	}

	def encuestaService

	@Transactional
	def enviarEncuesta() {
		def user = springSecurityService.currentUser
		def id = params.id
		def participante = empresaService.buscar(user, id)
		def r = validationService.validarEncuesta(participante)

		if (r.esValido) {
			empresaService.cambiarEstado(user, participante, EstadoEncuesta.POR_VALIDAR)
			encuestaService.sincronizar(participante)
		}
		respond r
	}

	def resultado(status, message) {
		[estado: status, mensaje: message]
	}

	def mappear(it) {
		if (it) {
			[
				'id'         : it.edicion.codigo,
				'encuesta'   : it.edicion.nombre,
				'vence'      : it.edicion.fin,
				'abre'       : it.edicion.inicio,
				'anio'       : it.edicion.anio,
                'contexto'	 : JSON.parse(it.contexto?:'{}'),
				'estado'     : it.estadoBase1.name(),
				'presentacion': it.edicion.templatePresentacion,
				'formularios': it.edicion.formularios.collect {
					[
						'id'    : it.id,
						'titulo': it.titulo,
                        'orden': it.orden
                    ]
				}
			]
		}
	}

	private def mappearFormulario(CargaFormulario it, boolean esEditable) {
		if (it) {			
			[
				titulo: "${it.formulario?.orden}. ${it.formulario?.titulo}",
				template: esEditable ? it.formulario?.template : it.formulario?.templateVista,
				datos: JSON.parse(it.datos),
				contexto: JSON.parse(it.participante.contexto),
				resultadoValidacion: JSON.parse(it.resultadoValidacion),
				esEditable: esEditable
			]
		}
	}

	private def mappearFormulario(CargaFormulario it){
		mappearFormulario (it, it.participante.esEditable())
	}
}
