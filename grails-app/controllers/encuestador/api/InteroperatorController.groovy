package encuestador.api

import encuestador.Dataset
import encuestador.Edicion
import encuestador.Empresa
import encuestador.EstadoEncuesta
import encuestador.ParticipaEn
import grails.converters.JSON
import grails.plugin.springsecurity.rest.token.AccessToken
import grails.transaction.Transactional

class InteroperatorController {

	static namespace = "api"

	static responseFormats = ['json', 'xml']

	def springSecurityService

	def validationService

	def managerService

	static allowedMethods = [nuevaEmpresa:'POST', reabrir: 'POST', contactar: 'POST', anotar: 'POST', actualizarContexto: 'POST', actualizarDataset: 'POST']

	/**
	 * listado paginado de encuestas
	 *
	 * @return
	 */
	def ediciones() {
		params.max = Math.min(params.max ?: 10, 100)
		def ediciones = Edicion.list(params).collect { mappearEdicion(it) }
		respond ediciones, model:[edicionInstanceCount: Edicion.count()]
	}

	def mappearEdicion(Edicion e) {
		return [
			"nombre" : e.nombre,
			"anio" : e.anio,
			"fechaInicio" : e.inicio,
			"fechaFin" : e.fin,
			"codigo" : e.codigo
		]
	}

	/**
	 * listado paginado de empresas
	 *
	 * @return
	 */
	def empresas() {
		params.max = Math.min(params.max ?: 10, 100)
		respond Empresa.list(params).collect { mappearEmpresa(it) }, model:[empresaInstanceCount: Empresa.count()]
	}

	def mappearEmpresa(Empresa e) {
		[
			"nombre": e.nombre,
			"empresaId": e.empresaId,
			"email": e.email,
			"cuit": e.cuit
		]
	}

	/**
	 * Genera el token de autenticación para una empresa en particular
	 *
	 * @return
	 */
	def autenticar() {
		def empresa = Empresa.findByCuit(params.id)

		if (!empresa)
			respond (["status": "error", "message": "No se encontro ninguna empresa con ID ${params.id}"])

		def username = empresa.user.username

		AccessToken accessToken = managerService.autenticar(username)

		respond ([ "token": accessToken.accessToken, "token_type": "Bearer"])
	}

	/**
	 * devuelve el listado de encuestas para una empresa
	 *
	 * @return
	 */
	def estadoEncuestas() {
		def empresa = Empresa.findByCuit(params.id)

		respond ParticipaEn.findAllByEmpresa(empresa).collect { mappearParticipante(it) }
	}

	def mappearParticipante(ParticipaEn p) {
		[
			"id": p.id,
			"encuesta": p.edicion.nombre,
			"edicion": p.edicion.anio,
			"estado": p.estadoBase1.name(),
			"fechaApertura": p.fechaApertura,
			"fechaEnvio": p.fechaEnvio
		]
	}


	@Transactional(readOnly=false)
	def nuevaEmpresa() {
		def currentUser = springSecurityService.currentUser
		//TODO: validar rol DNIC
		def datos = request.JSON
		def resp = managerService.nuevaEmpresa(datos)
		respond resp
	}

	@Transactional
	def reabrir() {
		def user = springSecurityService.currentUser
		def id = params.id as Long
		def participante = ParticipaEn.get(id)

		def r = managerService.cambiarEstado(user, participante, EstadoEncuesta.REABIERTA, params)
		respond r
	}

	@Transactional
	def anotar() {
		def empresa = Empresa.findByCuit(params.id)
		def datos = request.JSON

		def anotaciones = JSON.parse(empresa.anotaciones ?: '{}')
		anotaciones[datos.anotacion] = datos.valor
		empresa.anotaciones = anotaciones
		empresa.save()

		respond JSON.parse(empresa.anotaciones)
	}

	def anotaciones() {
		def empresa = Empresa.findByCuit(params.id)
		respond JSON.parse(empresa.anotaciones?: '{}')
	}

	def resultado(status, message) {
		[estado: status, mensaje: message]
	}


	def contexto() {
		def res = resultado('error', 'Faltan los parametros cuit y codigo de edición.')

		def participaEn = null

		if (params.cuit && params.codigo) {
			participaEn = ParticipaEn.createCriteria().get() {
				edicion {
					eq('codigo', params.codigo)
				}
				empresa {
					eq('cuit', params.cuit)
				}
			}
			if (participaEn) {
				res = ["estado": "OK", "contexto": JSON.parse(participaEn.contexto)]
			} else {
				res = resultado("error", "No se hallaron datos de contexto para la edicion $params.codigo y el cuit $params.cuit")
			}
		}

		respond res
	}

	@Transactional
	def actualizarContexto() {
		def res = resultado('error', 'Faltan los parametros cuit y codigo de edición.')

		ParticipaEn participaEn = null

		def requestBody = request.JSON

		if (requestBody.cuit && requestBody.codigo && requestBody.contexto) {
			participaEn = ParticipaEn.createCriteria().get() {
				edicion {
					eq('codigo', requestBody.codigo)
				}
				empresa {
					eq('cuit', requestBody.cuit)
				}
			}

			if (participaEn) {
				participaEn.contexto = requestBody.contexto
				participaEn.save()
				res = resultado("OK", "Se ha actualizado el contexto.")
			} else {
				res = resultado("error", "No se hallaron datos de contexto para la edicion $requestBody.codigo y el cuit $requestBody.cuit")
			}
		}

		respond res
	}

	@Transactional
	def dataset() {
		def res = resultado('error', 'Faltan los parametros cuit y codigo de encuesta.')

		Dataset dataset = null
		Empresa empresa = null

		if (params.cuit && params.codigo) {
			empresa = Empresa.findByCuit(params.cuit)
			def elCodigo = "${empresa.empresaId}_${params.codigo}"
			dataset = Dataset.findByCodigo(elCodigo)
			if (!dataset) {
				dataset = new Dataset(codigo: elCodigo)
				dataset.save()
			}
			if (empresa) {
				res = ["estado": "OK", "datos": JSON.parse(dataset.datos), "codigo": dataset.codigo]
			} else {
				res = resultado("error", "No se hallaron datos de contexto para la edicion $params.codigo y el cuit $params.cuit")
			}
		}

		respond res
	}


	@Transactional
	def actualizarDataset() {
		def res = resultado('error', 'Falta el parametro codigo de dataset.')

		def requestBody = request.JSON

		if (requestBody.codigo && requestBody.datos) {
			Dataset d = Dataset.findByCodigo(requestBody.codigo)

			if (d) {
				d.datos = requestBody.datos
				d.actualizado = new Date()
				d.save()
				res = resultado("OK", "Se ha actualizado el dataset.")
			} else {
				res = resultado("error", "No se hallo el dataset con codigo $requestBody.codigo")
			}
		}

		respond res
	}

	@Transactional
	def invitar() {
		def datos = request.JSON

		def empresa = Empresa.findByCuit(datos.cuit)
		if (!empresa)
			respond (["estado": "error", "mensaje": "No se encontro ninguna empresa con ID ${datos.cuit}"])

		def edicion = Edicion.findByCodigo(datos.codigoEdicion)
		if (!edicion)
			respond (["estado": "error", "mensaje": "No se encontro ninguna edicion con codigo ${datos.codigoEdicion}"])

		def pe = ParticipaEn.findByEmpresaAndEdicion(empresa, edicion);

		if (!pe) {
			pe = new ParticipaEn(empresa: empresa, edicion: edicion)
			if (!pe.save()) {
				pe.errors.each { println it }
				respond (["estado": "error", "mensaje": "Problemas invitando al encuestado."])
			}
		} else {
			respond (["estado": "error", "mensaje": "El encuestado ya fue invitado para esta edición"])
		}

		respond (["estado": "success", "mensaje": "Se ha registrado la invitación"])
	}
}
