package encuestador.api

import encuestador.security.Person

import java.security.Principal

class UserController {

    static namespace = "api"

    static responseFormats = ['json', 'xml']

    def springSecurityService

    static allowedMethods = [cambiarContrasenia:'POST']

    def index() {
        def principal = springSecurityService.principal
        render(contentType: "text/json") {
            [user: principal.username, roles: principal.authorities*.authority]
        }
    }

    def cambiarContrasenia() {
        def principal = springSecurityService.principal
        def solicitud = request.JSON
        if (solicitud.passwordAnterior && solicitud.newPassword && solicitud.newPasswordRepeated) {
            if (solicitud.newPassword.equals(solicitud.newPasswordRepeated)) {
                def oldPassword = springSecurityService.currentUser.getPassword()
                def hayQueCambiar = springSecurityService.passwordEncoder.isPasswordValid(oldPassword,solicitud.passwordAnterior, null)
                if  (hayQueCambiar) {
                    def p = Person.findByUsername(principal.username)
                    p.password = solicitud.newPassword
                    p.save(flush: true)
                    return [status: 'success', message:'password changed']
                }
            }
        }
        return [status: 'error', message:'oops']
    }

}
