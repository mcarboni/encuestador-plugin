package encuestador



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class ValorListaControladaController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond ValorListaControlada.list(params), model:[valorListaControladaInstanceCount: ValorListaControlada.count()]
    }

    def show(ValorListaControlada valorListaControladaInstance) {
        respond valorListaControladaInstance
    }

    def create() {
        respond new ValorListaControlada(params)
    }

    @Transactional
    def save(ValorListaControlada valorListaControladaInstance) {
        if (valorListaControladaInstance == null) {
            notFound()
            return
        }

        if (valorListaControladaInstance.hasErrors()) {
            respond valorListaControladaInstance.errors, view:'create'
            return
        }

        valorListaControladaInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'valorListaControlada.label', default: 'ValorListaControlada'), valorListaControladaInstance.id])
                redirect valorListaControladaInstance
            }
            '*' { respond valorListaControladaInstance, [status: CREATED] }
        }
    }

    def edit(ValorListaControlada valorListaControladaInstance) {
        respond valorListaControladaInstance
    }

    @Transactional
    def update(ValorListaControlada valorListaControladaInstance) {
        if (valorListaControladaInstance == null) {
            notFound()
            return
        }

        if (valorListaControladaInstance.hasErrors()) {
            respond valorListaControladaInstance.errors, view:'edit'
            return
        }

        valorListaControladaInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'ValorListaControlada.label', default: 'ValorListaControlada'), valorListaControladaInstance.id])
                redirect valorListaControladaInstance
            }
            '*'{ respond valorListaControladaInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(ValorListaControlada valorListaControladaInstance) {

        if (valorListaControladaInstance == null) {
            notFound()
            return
        }

        valorListaControladaInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'ValorListaControlada.label', default: 'ValorListaControlada'), valorListaControladaInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'valorListaControlada.label', default: 'ValorListaControlada'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
