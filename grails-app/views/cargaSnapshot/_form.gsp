<%@ page import="encuestador.CargaSnapshot" %>



<div class="fieldcontain ${hasErrors(bean: cargaSnapshotInstance, field: 'creadoPor', 'error')} required">
	<label for="creadoPor">
		<g:message code="cargaSnapshot.creadoPor.label" default="Creado Por" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="creadoPor" required="" value="${cargaSnapshotInstance?.creadoPor}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: cargaSnapshotInstance, field: 'creado', 'error')} required">
	<label for="creado">
		<g:message code="cargaSnapshot.creado.label" default="Creado" />
		<span class="required-indicator">*</span>
	</label>
	<g:datePicker name="creado" precision="day"  value="${cargaSnapshotInstance?.creado}"  />

</div>

<div class="fieldcontain ${hasErrors(bean: cargaSnapshotInstance, field: 'base', 'error')} required">
	<label for="base">
		<g:message code="cargaSnapshot.base.label" default="Base" />
		<span class="required-indicator">*</span>
	</label>
	<g:select name="base" from="${encuestador.Base?.values()}" keys="${encuestador.Base.values()*.name()}" required="" value="${cargaSnapshotInstance?.base?.name()}" />

</div>

<div class="fieldcontain ${hasErrors(bean: cargaSnapshotInstance, field: 'resultadoValidacion', 'error')} required">
	<label for="resultadoValidacion">
		<g:message code="cargaSnapshot.resultadoValidacion.label" default="Resultado Validacion" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="resultadoValidacion" required="" value="${cargaSnapshotInstance?.resultadoValidacion}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: cargaSnapshotInstance, field: 'datos', 'error')} required">
	<label for="datos">
		<g:message code="cargaSnapshot.datos.label" default="Datos" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="datos" required="" value="${cargaSnapshotInstance?.datos}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: cargaSnapshotInstance, field: 'carga', 'error')} required">
	<label for="carga">
		<g:message code="cargaSnapshot.carga.label" default="Carga" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="carga" name="carga.id" from="${encuestador.CargaFormulario.list()}" optionKey="id" required="" value="${cargaSnapshotInstance?.carga?.id}" class="many-to-one"/>

</div>

