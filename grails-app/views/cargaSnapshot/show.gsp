
<%@ page import="encuestador.CargaSnapshot" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'cargaSnapshot.label', default: 'CargaSnapshot')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-cargaSnapshot" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-cargaSnapshot" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list cargaSnapshot">
			
				<g:if test="${cargaSnapshotInstance?.creadoPor}">
				<li class="fieldcontain">
					<span id="creadoPor-label" class="property-label"><g:message code="cargaSnapshot.creadoPor.label" default="Creado Por" /></span>
					
						<span class="property-value" aria-labelledby="creadoPor-label"><g:fieldValue bean="${cargaSnapshotInstance}" field="creadoPor"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${cargaSnapshotInstance?.creado}">
				<li class="fieldcontain">
					<span id="creado-label" class="property-label"><g:message code="cargaSnapshot.creado.label" default="Creado" /></span>
					
						<span class="property-value" aria-labelledby="creado-label"><g:formatDate date="${cargaSnapshotInstance?.creado}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${cargaSnapshotInstance?.base}">
				<li class="fieldcontain">
					<span id="base-label" class="property-label"><g:message code="cargaSnapshot.base.label" default="Base" /></span>
					
						<span class="property-value" aria-labelledby="base-label"><g:fieldValue bean="${cargaSnapshotInstance}" field="base"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${cargaSnapshotInstance?.resultadoValidacion}">
				<li class="fieldcontain">
					<span id="resultadoValidacion-label" class="property-label"><g:message code="cargaSnapshot.resultadoValidacion.label" default="Resultado Validacion" /></span>
					
						<span class="property-value" aria-labelledby="resultadoValidacion-label"><g:fieldValue bean="${cargaSnapshotInstance}" field="resultadoValidacion"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${cargaSnapshotInstance?.datos}">
				<li class="fieldcontain">
					<span id="datos-label" class="property-label"><g:message code="cargaSnapshot.datos.label" default="Datos" /></span>
					
						<span class="property-value" aria-labelledby="datos-label"><g:fieldValue bean="${cargaSnapshotInstance}" field="datos"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${cargaSnapshotInstance?.carga}">
				<li class="fieldcontain">
					<span id="carga-label" class="property-label"><g:message code="cargaSnapshot.carga.label" default="Carga" /></span>
					
						<span class="property-value" aria-labelledby="carga-label"><g:link controller="cargaFormulario" action="show" id="${cargaSnapshotInstance?.carga?.id}">${cargaSnapshotInstance?.carga?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:cargaSnapshotInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${cargaSnapshotInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
