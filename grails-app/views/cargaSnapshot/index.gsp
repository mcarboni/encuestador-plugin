
<%@ page import="encuestador.CargaSnapshot" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'cargaSnapshot.label', default: 'CargaSnapshot')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-cargaSnapshot" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-cargaSnapshot" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<g:sortableColumn property="creadoPor" title="${message(code: 'cargaSnapshot.creadoPor.label', default: 'Creado Por')}" />
					
						<g:sortableColumn property="creado" title="${message(code: 'cargaSnapshot.creado.label', default: 'Creado')}" />
					
						<g:sortableColumn property="base" title="${message(code: 'cargaSnapshot.base.label', default: 'Base')}" />
					
						<g:sortableColumn property="resultadoValidacion" title="${message(code: 'cargaSnapshot.resultadoValidacion.label', default: 'Resultado Validacion')}" />
					
						<g:sortableColumn property="datos" title="${message(code: 'cargaSnapshot.datos.label', default: 'Datos')}" />
					
						<th><g:message code="cargaSnapshot.carga.label" default="Carga" /></th>
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${cargaSnapshotInstanceList}" status="i" var="cargaSnapshotInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${cargaSnapshotInstance.id}">${fieldValue(bean: cargaSnapshotInstance, field: "creadoPor")}</g:link></td>
					
						<td><g:formatDate date="${cargaSnapshotInstance.creado}" /></td>
					
						<td>${fieldValue(bean: cargaSnapshotInstance, field: "base")}</td>
					
						<td>${fieldValue(bean: cargaSnapshotInstance, field: "resultadoValidacion")}</td>
					
						<td>${fieldValue(bean: cargaSnapshotInstance, field: "datos")}</td>
					
						<td>${fieldValue(bean: cargaSnapshotInstance, field: "carga")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${cargaSnapshotInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
