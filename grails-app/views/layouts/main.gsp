<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"><!--<![endif]-->
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title><g:layoutTitle default="Grails"/></title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="shortcut icon" href="${assetPath(src: 'favicon.ico')}" type="image/x-icon">
	<link rel="apple-touch-icon" href="${assetPath(src: 'apple-touch-icon.png')}">
	<link rel="apple-touch-icon" sizes="114x114" href="${assetPath(src: 'apple-touch-icon-retina.png')}">
	<asset:stylesheet src="application.css"/>
	<asset:javascript src="application.js"/>
	<g:layoutHead/>
</head>
<body>

<nav class="navbar navbar-inverse">
	<div class="container-fluid">
		<div class="container">
			<div class="navbar-header">
				<a class="navbar-brand" ng-href="#/">Encuestador</a>
			</div>
			<ul class="nav navbar-nav" >
				<li class="${controllerName == '' ? 'active' : ''}">
					<g:link uri="/">INICIO</g:link>
				</li>
			</ul>
			<sec:ifLoggedIn>
				<ul class="nav navbar-nav navbar-right">
					<li class="dropdown"><a href="#" class="dropdown-toggle"
											data-toggle="dropdown" role="button" aria-expanded="false">ADMINISTRAR <span class="caret"></span>
					</a>
						<ul class="dropdown-menu" role="menu">
							<g:each var="c" in="${grailsApplication.controllerClasses.findAll{it.packageName.contains('encuestador')}.sort { it.fullName } }">
								<li ><g:link controller="${c.logicalPropertyName}">${c.fullName}</g:link></li>
							</g:each>
						</ul>
					</li>
					<li class="dropdown"><a href="#" class="dropdown-toggle"
											data-toggle="dropdown" role="button" aria-expanded="false">${sec.username()} <span class="caret"></span>
					</a>
						<ul class="dropdown-menu" role="menu">
							<li>
								<g:remoteLink class="logout" controller="logout" method="post" asynchronous="false" onSuccess="location.reload()">
									Cerrar Sesi&oacute;n
								</g:remoteLink>
							</li>
							<%--						<li><a href="${createLink(controller: 'medio')}">Medios</a></li>--%>
							<%--						<li><a href="${createLink(controller: 'programa')}">Programas</a></li>--%>
							<%--						<li class="divider"></li>--%>
							<%--						<li><a href="${createLink(controller: 'area')}">&Aacute;reas</a></li>--%>
						</ul>
					</li>
				</ul>
			</sec:ifLoggedIn>
		</div>
	</div>
</nav>
<div class="container">
	<g:layoutBody/>
</div>

</body>
</html>
