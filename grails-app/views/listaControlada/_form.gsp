<%@ page import="encuestador.ListaControlada" %>



<div class="fieldcontain ${hasErrors(bean: listaControladaInstance, field: 'codigo', 'error')} required">
	<label for="codigo">
		<g:message code="listaControlada.codigo.label" default="Codigo" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="codigo" required="" value="${listaControladaInstance?.codigo}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: listaControladaInstance, field: 'nombre', 'error')} required">
	<label for="nombre">
		<g:message code="listaControlada.nombre.label" default="Nombre" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="nombre" required="" value="${listaControladaInstance?.nombre}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: listaControladaInstance, field: 'valores', 'error')} ">
	<label for="valores">
		<g:message code="listaControlada.valores.label" default="Valores" />
		
	</label>
	
<ul class="one-to-many">
<g:each in="${listaControladaInstance?.valores?}" var="v">
    <li><g:link controller="valorListaControlada" action="show" id="${v.id}">${v?.encodeAsHTML()}</g:link></li>
</g:each>
<li class="add">
<g:link controller="valorListaControlada" action="create" params="['listaControlada.id': listaControladaInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'valorListaControlada.label', default: 'ValorListaControlada')])}</g:link>
</li>
</ul>


</div>

