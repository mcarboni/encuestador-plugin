
<%@ page import="encuestador.ParticipaEn" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'participaEn.label', default: 'ParticipaEn')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-participaEn" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-participaEn" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list participaEn">
			
				<g:if test="${participaEnInstance?.edicion}">
				<li class="fieldcontain">
					<span id="edicion-label" class="property-label"><g:message code="participaEn.edicion.label" default="Edicion" /></span>
					
						<span class="property-value" aria-labelledby="edicion-label"><g:link controller="edicion" action="show" id="${participaEnInstance?.edicion?.id}">${participaEnInstance?.edicion?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${participaEnInstance?.empresa}">
				<li class="fieldcontain">
					<span id="empresa-label" class="property-label"><g:message code="participaEn.empresa.label" default="Empresa" /></span>
					
						<span class="property-value" aria-labelledby="empresa-label"><g:link controller="empresa" action="show" id="${participaEnInstance?.empresa?.id}">${participaEnInstance?.empresa?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${participaEnInstance?.estadoBase1}">
				<li class="fieldcontain">
					<span id="estadoBase1-label" class="property-label"><g:message code="participaEn.estadoBase1.label" default="Estado Base1" /></span>
					
						<span class="property-value" aria-labelledby="estadoBase1-label"><g:fieldValue bean="${participaEnInstance}" field="estadoBase1"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${participaEnInstance?.fechaApertura}">
				<li class="fieldcontain">
					<span id="fechaApertura-label" class="property-label"><g:message code="participaEn.fechaApertura.label" default="Fecha Apertura" /></span>
					
						<span class="property-value" aria-labelledby="fechaApertura-label"><g:formatDate date="${participaEnInstance?.fechaApertura}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${participaEnInstance?.fechaEnvio}">
				<li class="fieldcontain">
					<span id="fechaEnvio-label" class="property-label"><g:message code="participaEn.fechaEnvio.label" default="Fecha Envio" /></span>
					
						<span class="property-value" aria-labelledby="fechaEnvio-label"><g:formatDate date="${participaEnInstance?.fechaEnvio}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${participaEnInstance?.relevadaExternamente}">
				<li class="fieldcontain">
					<span id="relevadaExternamente-label" class="property-label"><g:message code="participaEn.relevadaExternamente.label" default="Relevada Externamente" /></span>
					
						<span class="property-value" aria-labelledby="relevadaExternamente-label"><g:formatBoolean boolean="${participaEnInstance?.relevadaExternamente}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${participaEnInstance?.resultadoValidacion}">
				<li class="fieldcontain">
					<span id="resultadoValidacion-label" class="property-label"><g:message code="participaEn.resultadoValidacion.label" default="Resultado Validacion" /></span>
					
						<span class="property-value" aria-labelledby="resultadoValidacion-label"><g:fieldValue bean="${participaEnInstance}" field="resultadoValidacion"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${participaEnInstance?.contexto}">
				<li class="fieldcontain">
					<span id="contexto-label" class="property-label"><g:message code="participaEn.contexto.label" default="Contexto" /></span>
					
						<span class="property-value" aria-labelledby="contexto-label"><g:fieldValue bean="${participaEnInstance}" field="contexto"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${participaEnInstance?.estadoBase2}">
				<li class="fieldcontain">
					<span id="estadoBase2-label" class="property-label"><g:message code="participaEn.estadoBase2.label" default="Estado Base2" /></span>
					
						<span class="property-value" aria-labelledby="estadoBase2-label"><g:fieldValue bean="${participaEnInstance}" field="estadoBase2"/></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:participaEnInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${participaEnInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
