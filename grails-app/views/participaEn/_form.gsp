<%@ page import="encuestador.ParticipaEn"%>



<div
	class="fieldcontain ${hasErrors(bean: participaEnInstance, field: 'resultadoValidacion', 'error')} ">
	<label for="resultadoValidacion"> <g:message
			code="participaEn.resultadoValidacion.label"
			default="Resultado Validacion" />

	</label>
	<g:textField name="resultadoValidacion"
		value="${participaEnInstance?.resultadoValidacion}" />

</div>

<div
	class="fieldcontain ${hasErrors(bean: participaEnInstance, field: 'fechaApertura', 'error')} ">
	<label for="fechaApertura"> <g:message
			code="participaEn.fechaApertura.label" default="Fecha Apertura" />

	</label>
	<g:datePicker name="fechaApertura" precision="day"
		value="${participaEnInstance?.fechaApertura}" default="none"
		noSelection="['': '']" />

</div>

<div
	class="fieldcontain ${hasErrors(bean: participaEnInstance, field: 'fechaEnvio', 'error')} ">
	<label for="fechaEnvio"> <g:message
			code="participaEn.fechaEnvio.label" default="Fecha Envio" />

	</label>
	<g:datePicker name="fechaEnvio" precision="day"
		value="${participaEnInstance?.fechaEnvio}" default="none"
		noSelection="['': '']" />

</div>

<div
	class="fieldcontain ${hasErrors(bean: participaEnInstance, field: 'relevadaExternamente', 'error')} ">
	<label for="relevadaExternamente"> <g:message
			code="participaEn.relevadaExternamente.label"
			default="Relevada Externamente" />

	</label>
	<g:checkBox name="relevadaExternamente"
		value="${participaEnInstance?.relevadaExternamente}" />

</div>

<div
	class="fieldcontain ${hasErrors(bean: participaEnInstance, field: 'empresa', 'error')} required">
	<label for="empresa"> <g:message
			code="participaEn.empresa.label" default="Empresa" /> <span
		class="required-indicator">*</span>
	</label>
	<g:select id="empresa" name="empresa.id" from="${encuestador.Empresa.list()}"
		optionKey="id" required="" value="${participaEnInstance?.empresa?.id}"
		class="many-to-one" />

</div>

<div
	class="fieldcontain ${hasErrors(bean: participaEnInstance, field: 'edicion', 'error')} required">
	<label for="edicion"> <g:message
			code="participaEn.edicion.label" default="Edicion" /> <span
		class="required-indicator">*</span>
	</label>
	<g:select id="edicion" name="edicion.id"
              from="${encuestador.Edicion.list()}" optionKey="id" required=""
              value="${participaEnInstance?.edicion?.id}" class="many-to-one" />

</div>

<div
	class="fieldcontain ${hasErrors(bean: participaEnInstance, field: 'estadoBase1', 'error')} required">
	<label for="estadoBase1"> <g:message
			code="participaEn.estadoBase1.label" default="Estado Base1" /> <span
		class="required-indicator">*</span>
	</label>
	<g:select name="estadoBase1" from="${encuestador.EstadoEncuesta?.values()}"
              keys="${encuestador.EstadoEncuesta.values()*.name()}" required=""
              value="${participaEnInstance?.estadoBase1?.name()}" />

</div>

<div
	class="fieldcontain ${hasErrors(bean: participaEnInstance, field: 'estadoBase2', 'error')} required">
	<label for="estadoBase2"> <g:message
			code="participaEn.estadoBase2.label" default="Estado Base2" /> <span
		class="required-indicator">*</span>
	</label>
	<g:select name="estadoBase2" from="${encuestador.EstadoEncuesta?.values()}"
              keys="${encuestador.EstadoEncuesta.values()*.name()}" required=""
              value="${participaEnInstance?.estadoBase2?.name()}" />

</div>

<div
	class="fieldcontain ${hasErrors(bean: participaEnInstance, field: 'contexto', 'error')} required">
	<label for="contexto"> <g:message
			code="formulario.contexto.label" default="Contexto" /> <span
		class="required-indicator">*</span>
	</label>
	<cmeditor:textArea name="contexto" binding="vim" required=""
		value="${participaEnInstance?.contexto}"></cmeditor:textArea>
</div>

