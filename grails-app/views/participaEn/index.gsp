
<%@ page import="encuestador.ParticipaEn" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'participaEn.label', default: 'ParticipaEn')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-participaEn" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-participaEn" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:form method="GET">
				<fieldset class="buttons">
					<label for="empresaId"> <g:message default="ID Empresa" />
					</label>
					<g:field name="empresaId" type="number" required="" value="${params.empresaId}"/>
					<g:actionSubmit class="save" action="search"
						value="${message(code: 'default.button.search.label', default: 'Search')}" />
				</fieldset>
			</g:form>
			<table>
			<thead>
					<tr>
					
						<th><g:message code="participaEn.edicion.label" default="Edicion" /></th>
					
						<th><g:message code="participaEn.empresa.label" default="Empresa" /></th>
					
						<g:sortableColumn property="estadoBase1" title="${message(code: 'participaEn.estadoBase1.label', default: 'Estado Base1')}" />
					
						<g:sortableColumn property="fechaApertura" title="${message(code: 'participaEn.fechaApertura.label', default: 'Fecha Apertura')}" />
					
						<g:sortableColumn property="fechaEnvio" title="${message(code: 'participaEn.fechaEnvio.label', default: 'Fecha Envio')}" />
					
						<g:sortableColumn property="relevadaExternamente" title="${message(code: 'participaEn.relevadaExternamente.label', default: 'Relevada Externamente')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${participaEnInstanceList}" status="i" var="participaEnInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${participaEnInstance.id}">${fieldValue(bean: participaEnInstance, field: "edicion")}</g:link></td>
					
						<td>${fieldValue(bean: participaEnInstance, field: "empresa")}</td>
					
						<td>${fieldValue(bean: participaEnInstance, field: "estadoBase1")}</td>
					
						<td><g:formatDate date="${participaEnInstance.fechaApertura}" /></td>
					
						<td><g:formatDate date="${participaEnInstance.fechaEnvio}" /></td>
					
						<td><g:formatBoolean boolean="${participaEnInstance.relevadaExternamente}" /></td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${participaEnInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
