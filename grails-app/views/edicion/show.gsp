
<%@ page import="encuestador.Edicion"%>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<g:set var="entityName"
	value="${message(code: 'edicion.label', default: 'Edicion')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
</head>
<body>
	<a href="#show-edicion" class="skip" tabindex="-1"><g:message
			code="default.link.skip.label" default="Skip to content&hellip;" /></a>
	<div class="nav" role="navigation">
		<ul>
			<li><a class="home" href="${createLink(uri: '/')}"><g:message
						code="default.home.label" /></a></li>
			<li><g:link class="list" action="index">
					<g:message code="default.list.label" args="[entityName]" />
				</g:link></li>
			<li><g:link class="create" action="create">
					<g:message code="default.new.label" args="[entityName]" />
				</g:link></li>
		</ul>
	</div>
	<div id="show-edicion" class="content scaffold-show" role="main">
		<h1>
			<g:message code="default.show.label" args="[entityName]" />
		</h1>
		<g:if test="${flash.message}">
			<div class="message" role="status">
				${flash.message}
			</div>
		</g:if>
		<ol class="property-list edicion">

			<g:if test="${edicionInstance?.nombre}">
				<li class="fieldcontain"><span id="nombre-label"
					class="property-label"><g:message
							code="edicion.nombre.label" default="Nombre" /></span> <span
					class="property-value" aria-labelledby="nombre-label"><g:fieldValue
							bean="${edicionInstance}" field="nombre" /></span></li>
			</g:if>

			<g:if test="${edicionInstance?.anio}">
				<li class="fieldcontain"><span id="anio-label"
					class="property-label"><g:message code="edicion.anio.label"
							default="Año" /></span> <span class="property-value"
					aria-labelledby="anio-label"><g:fieldValue
							bean="${edicionInstance}" field="anio" /></span></li>
			</g:if>

			<g:if test="${edicionInstance?.inicio}">
				<li class="fieldcontain"><span id="inicio-label"
					class="property-label"><g:message
							code="edicion.inicio.label" default="Inicio" /></span> <span
					class="property-value" aria-labelledby="inicio-label"><g:formatDate
							date="${edicionInstance?.inicio}" /></span></li>
			</g:if>

			<g:if test="${edicionInstance?.fin}">
				<li class="fieldcontain"><span id="fin-label"
					class="property-label"><g:message code="edicion.fin.label"
							default="Fin" /></span> <span class="property-value"
					aria-labelledby="fin-label"><g:formatDate
							date="${edicionInstance?.fin}" /></span></li>
			</g:if>

			<g:if test="${edicionInstance?.scriptValidacion}">
				<li class="fieldcontain"><span id="scriptValidacion-label"
					class="property-label"><g:message
							code="edicion.scriptValidacion.label" default="Script Validacion" /></span>

					<span class="property-value"
					aria-labelledby="scriptValidacion-label"><g:fieldValue
							bean="${edicionInstance}" field="scriptValidacion" /></span></li>
			</g:if>

			<g:if test="${edicionInstance?.templatePresentacion}">
				<li class="fieldcontain"><span id="templatePresentacion-label"
					class="property-label"><g:message
							code="edicion.templatePresentacion.label"
							default="Template Presentacion" /></span> <span class="property-value"
					aria-labelledby="templatePresentacion-label"><g:fieldValue
							bean="${edicionInstance}" field="templatePresentacion" /></span></li>
			</g:if>

			<g:if test="${edicionInstance?.codigo}">
				<li class="fieldcontain"><span id="codigo-label"
					class="property-label"><g:message
							code="edicion.codigo.label" default="Codigo" /></span> <span
					class="property-value" aria-labelledby="codigo-label"><g:fieldValue
							bean="${edicionInstance}" field="codigo" /></span></li>
			</g:if>

			<g:if test="${edicionInstance?.formularios}">
				<li class="fieldcontain"><span id="formularios-label"
					class="property-label"><g:message
							code="edicion.formularios.label" default="Formularios" /></span> <g:each
						in="${edicionInstance.formularios}" var="f">
						<span class="property-value" aria-labelledby="formularios-label"><g:link
								controller="formulario" action="show" id="${f.id}">
								${f?.encodeAsHTML()}
							</g:link></span>
					</g:each></li>
			</g:if>

			<g:if test="${edicionInstance?.habilitado}">
				<li class="fieldcontain"><span id="habilitado-label"
					class="property-label"><g:message
							code="edicion.habilitado.label" default="Habilitado" /></span> <span
					class="property-value" aria-labelledby="habilitado-label"><g:formatBoolean
							boolean="${edicionInstance?.habilitado}" /></span></li>
			</g:if>

		</ol>
		<g:form url="[resource:edicionInstance, action:'delete']"
			method="DELETE">
			<fieldset class="buttons">
				<g:link class="edit" action="edit" resource="${edicionInstance}">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:link>
				<g:actionSubmit class="delete" action="delete"
					value="${message(code: 'default.button.delete.label', default: 'Delete')}"
					onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
			</fieldset>
		</g:form>
	</div>
</body>
</html>
