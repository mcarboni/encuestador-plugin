<%@ page import="encuestador.Edicion" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'edicion.label', default: 'Edicion')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-edicion" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-edicion" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<g:sortableColumn property="nombre" title="${message(code: 'edicion.nombre.label', default: 'Nombre')}" />
					
						<g:sortableColumn property="inicio" title="${message(code: 'edicion.inicio.label', default: 'Inicio')}" />
					
						<g:sortableColumn property="fin" title="${message(code: 'edicion.fin.label', default: 'Fin')}" />
					
						<g:sortableColumn property="scriptValidacion" title="${message(code: 'edicion.scriptValidacion.label', default: 'Script Validacion')}" />
					
						<g:sortableColumn property="templatePresentacion" title="${message(code: 'edicion.templatePresentacion.label', default: 'Template Presentacion')}" />
					
						<g:sortableColumn property="codigo" title="${message(code: 'edicion.codigo.label', default: 'Codigo')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${edicionInstanceList}" status="i" var="edicionInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${edicionInstance.id}">${fieldValue(bean: edicionInstance, field: "nombre")}</g:link></td>
					
						<td><g:formatDate date="${edicionInstance.inicio}" /></td>
					
						<td><g:formatDate date="${edicionInstance.fin}" /></td>
					
						<td>${fieldValue(bean: edicionInstance, field: "scriptValidacion")}</td>
					
						<td>${fieldValue(bean: edicionInstance, field: "templatePresentacion")}</td>
					
						<td>${fieldValue(bean: edicionInstance, field: "codigo")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${edicionInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
