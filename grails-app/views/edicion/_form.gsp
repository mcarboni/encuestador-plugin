<%@ page import="encuestador.Edicion"%>

<div
	class="fieldcontain ${hasErrors(bean: edicionInstance, field: 'encuesta', 'error')} required">
	<label for="encuesta"> <g:message code="edicion.encuesta.label"
			default="Encuesta" /> <span class="required-indicator">*</span>
	</label>
	<g:select id="encuesta" name="encuesta.id"
		from="${encuestador.Encuesta.list()}" optionKey="id" required=""
		value="${edicionInstance?.encuesta?.id}" class="many-to-one" />

</div>

<div
	class="fieldcontain ${hasErrors(bean: edicionInstance, field: 'emailAdministrativo', 'error')} required">
	<label for="emailAdministrativo"> <g:message
			code="edicion.emailAdministrativo.label"
			default="Email Administrativo" /> <span class="required-indicator">*</span>
	</label>
	<g:field type="email" name="emailAdministrativo" required=""
		value="${edicionInstance?.emailAdministrativo}" />

</div>

<div
	class="fieldcontain ${hasErrors(bean: edicionInstance, field: 'emailAdministrativoExterno', 'error')} required">
	<label for="emailAdministrativoExterno"> <g:message
			code="edicion.emailAdministrativoExterno.label"
			default="Email Administrativo Externo" /> <span
		class="required-indicator">*</span>
	</label>
	<g:field type="email" name="emailAdministrativoExterno" required=""
		value="${edicionInstance?.emailAdministrativoExterno}" />

</div>

<div
	class="fieldcontain ${hasErrors(bean: edicionInstance, field: 'emailCopia', 'error')} required">
	<label for="emailCopia"> <g:message
			code="edicion.emailCopia.label" default="Email Copia" /> <span
		class="required-indicator">*</span>
	</label>
	<g:field type="email" name="emailCopia" required=""
		value="${edicionInstance?.emailCopia}" />

</div>

<div
	class="fieldcontain ${hasErrors(bean: edicionInstance, field: 'nombre', 'error')} required">
	<label for="nombre"> <g:message code="edicion.nombre.label"
			default="Nombre" /> <span class="required-indicator">*</span>
	</label>
	<g:textField name="nombre" required=""
		value="${edicionInstance?.nombre}" />

</div>

<div
	class="fieldcontain ${hasErrors(bean: edicionInstance, field: 'anio', 'error')} required">
	<label for="anio"> <g:message code="edicion.anio.label"
			default="Año" /> <span class="required-indicator">*</span>
	</label>
	<g:field type="number" name="anio" required=""
		value="${edicionInstance?.anio}" />

</div>

<div
	class="fieldcontain ${hasErrors(bean: edicionInstance, field: 'inicio', 'error')} required">
	<label for="inicio"> <g:message code="edicion.inicio.label"
			default="Inicio" /> <span class="required-indicator">*</span>
	</label>
	<g:datePicker name="inicio" precision="day"
		value="${edicionInstance?.inicio}" />

</div>

<div
	class="fieldcontain ${hasErrors(bean: edicionInstance, field: 'fin', 'error')} required">
	<label for="fin"> <g:message code="edicion.fin.label"
			default="Fin" /> <span class="required-indicator">*</span>
	</label>
	<g:datePicker name="fin" precision="day"
		value="${edicionInstance?.fin}" />

</div>

<div
	class="fieldcontain ${hasErrors(bean: edicionInstance, field: 'scriptValidacion', 'error')} ">
	<label for="scriptValidacion"> <g:message
			code="edicion.scriptValidacion.label" default="Script Validacion" />

	</label>
	<cmeditor:textArea name="scriptValidacion" binding="vim" required=""
		mode="text/javascript" value="${edicionInstance?.scriptValidacion}"></cmeditor:textArea>
</div>

<div
	class="fieldcontain ${hasErrors(bean: edicionInstance, field: 'scriptSincronizacion', 'error')} ">
	<label for="scriptSincronizacion"> <g:message
			code="edicion.scriptValidacion.label" default="Script Sincronizacion" />

	</label>
	<cmeditor:textArea name="scriptSincronizacion" binding="vim"
		required="" mode="text/javascript"
		value="${edicionInstance?.scriptSincronizacion}"></cmeditor:textArea>
</div>

<div
	class="fieldcontain ${hasErrors(bean: edicionInstance, field: 'funcionesValidacion', 'error')} ">
	<label for="funcionesValidacion"> <g:message
			code="edicion.funcionesValidacion.label"
			default="Funciones Validación" />

	</label>
	<cmeditor:textArea name="funcionesValidacion" binding="vim" required=""
		mode="text/javascript" value="${edicionInstance?.funcionesValidacion}"></cmeditor:textArea>
</div>

<div
	class="fieldcontain ${hasErrors(bean: edicionInstance, field: 'templatePresentacion', 'error')} ">
	<label for="templatePresentacion"> <g:message
			code="edicion.templatePresentacion.label"
			default="Template Presentacion" />

	</label>
	<cmeditor:textArea name="templatePresentacion" binding="vim"
		required="" value="${edicionInstance?.templatePresentacion}"></cmeditor:textArea>

</div>

<div
	class="fieldcontain ${hasErrors(bean: edicionInstance, field: 'codigo', 'error')} required">
	<label for="codigo"> <g:message code="edicion.codigo.label"
			default="Codigo" /> <span class="required-indicator">*</span>
	</label>
	<g:textField name="codigo" required=""
		value="${edicionInstance?.codigo}" />

</div>

<div
	class="fieldcontain ${hasErrors(bean: edicionInstance, field: 'formularios', 'error')} ">
	<label for="formularios"> <g:message
			code="edicion.formularios.label" default="Formularios" />

	</label>

	<ul class="one-to-many">
		<g:each in="${edicionInstance?.formularios?}" var="f">
			<li><g:link controller="formulario" action="show" id="${f.id}">
					${f?.encodeAsHTML()}
				</g:link></li>
		</g:each>
		<li class="add"><g:link controller="formulario" action="create"
				params="['edicion.id': edicionInstance?.id]">
				${message(code: 'default.add.label', args: [message(code: 'formulario.label', default: 'Formulario')])}
			</g:link></li>
	</ul>


</div>

<div
	class="fieldcontain ${hasErrors(bean: edicionInstance, field: 'habilitado', 'error')} ">
	<label for="habilitado"> <g:message
			code="edicion.habilitado.label" default="Habilitado" />

	</label>
	<g:checkBox name="habilitado" value="${edicionInstance?.habilitado}" />

</div>

