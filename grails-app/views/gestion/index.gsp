<!DOCTYPE html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'edicion.label', default: 'Edicion')}" />
    <title><g:message code="default.create.label" args="[entityName]" /></title>
</head>
<body>

<div class="container" role="main">
    <h1>Gestion de encuestas</h1>
    <div class="container">

    <h2>Encuestas activas</h2>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
        <ul>
            <g:each in="${encuestador.Encuesta.findAll()}" var="e">
                <li><g:link action="encuesta" id="${e.id}">${e?.nombre}</g:link></li>
            </g:each>
        </ul>
</div>

</body>
