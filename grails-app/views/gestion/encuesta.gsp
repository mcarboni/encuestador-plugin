<!DOCTYPE html>
    <head>
        <meta name="layout" content="main">
        <g:set var="entityName" value="${message(code: 'edicion.label', default: 'Edicion')}" />
        <title><g:message code="default.create.label" args="[entityName]" /></title>
    </head>
<body>

    <div id="create-edicion" class="content scaffold-create" role="main">
		<h1>${encuestaInstance.nombre}</h1>

        <g:if test="${flash.message}">
            <div class="message" role="status">${flash.message}</div>
        </g:if>
        <ol class="property-list encuesta">

            <g:if test="${encuestaInstance?.codigo}">
                <li class="fieldcontain">
                    <span id="codigo-label" class="property-label"><g:message code="encuesta.codigo.label" default="Codigo" /></span>

                    <span class="property-value" aria-labelledby="codigo-label"><g:fieldValue bean="${encuestaInstance}" field="codigo"/></span>

                </li>
            </g:if>

            <g:if test="${encuestaInstance?.ediciones}">
                <li class="fieldcontain">
                    <span id="ediciones-label" class="property-label"><g:message code="encuesta.ediciones.label" default="Ediciones" /></span>

                    <g:each in="${encuestaInstance.ediciones.sort { it.anio }}" var="e">
                        <span class="property-value" aria-labelledby="ediciones-label"><g:link controller="edicion" action="show" id="${e.id}">${e?.encodeAsHTML()}</g:link></span>
                    </g:each>

                </li>
            </g:if>

            <g:if test="${encuestaInstance?.habilitado}">
                <li class="fieldcontain">
                    <span id="habilitado-label" class="property-label"><g:message code="encuesta.habilitado.label" default="Habilitado" /></span>

                    <span class="property-value" aria-labelledby="habilitado-label"><g:formatBoolean boolean="${encuestaInstance?.habilitado}" /></span>

                </li>
            </g:if>

            <g:if test="${encuestaInstance?.nombre}">
                <li class="fieldcontain">
                    <span id="nombre-label" class="property-label"><g:message code="encuesta.nombre.label" default="Nombre" /></span>

                    <span class="property-value" aria-labelledby="nombre-label"><g:fieldValue bean="${encuestaInstance}" field="nombre"/></span>

                </li>
            </g:if>

        </ol>

        <div class="panel panel-info">
            <div class="panel-heading">Crear nueva edici&oacute;n</div>
            <div class="panel-body">
                <g:form url="[action:'nuevaEdicion']" >
                    <g:set var="today" value="${new Date()}"/>
                    <g:hiddenField name="encuesta_id" value="${encuestaInstance.id}"></g:hiddenField>
                    <fieldset>
                        <div class="form-group">
                            <label for="anio">Año</label>
                            <g:field type="number" name="anio" required="" value="${ anio ?: Math.max(today[Calendar.YEAR], encuestaInstance?.ediciones.last().anio + 1)}" />
                        </div>
                        <div class="form-group">
                            <g:checkBox type="boolean" name="copiarFormularios" value="${copiarFormularios}" />
                            <label for="copiarFormularios">Copiar los formularios del último año</label>
                        </div>
                        <div class="form-group">
                            <g:checkBox type="boolean" name="copiarParticipantes" value="${copiarParticipantes}" />
                            <label for="copiarParticipantes">Invitar los participantes del último año</label>
                        </div>
                    </fieldset>
                    <fieldset>
                        <g:submitButton name="nuevaEdicion" class="btn btn-success" value="${message(code: 'default.button.create.label', default: 'Nueva edición')}" />
                    </fieldset>
                </g:form>
            </div>
        </div>

    </div>

</body>
