<%@ page import="encuestador.Encuesta" %>



<div class="fieldcontain ${hasErrors(bean: encuestaInstance, field: 'codigo', 'error')} required">
	<label for="codigo">
		<g:message code="encuesta.codigo.label" default="Codigo" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="codigo" required="" value="${encuestaInstance?.codigo}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: encuestaInstance, field: 'ediciones', 'error')} ">
	<label for="ediciones">
		<g:message code="encuesta.ediciones.label" default="Ediciones" />
		
	</label>
	
<ul class="one-to-many">
<g:each in="${encuestaInstance?.ediciones?}" var="e">
    <li><g:link controller="edicion" action="show" id="${e.id}">${e?.encodeAsHTML()}</g:link></li>
</g:each>
<li class="add">
<g:link controller="edicion" action="create" params="['encuesta.id': encuestaInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'edicion.label', default: 'Edicion')])}</g:link>
</li>
</ul>


</div>

<div class="fieldcontain ${hasErrors(bean: encuestaInstance, field: 'habilitado', 'error')} ">
	<label for="habilitado">
		<g:message code="encuesta.habilitado.label" default="Habilitado" />
		
	</label>
	<g:checkBox name="habilitado" value="${encuestaInstance?.habilitado}" />

</div>

<div class="fieldcontain ${hasErrors(bean: encuestaInstance, field: 'nombre', 'error')} required">
	<label for="nombre">
		<g:message code="encuesta.nombre.label" default="Nombre" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="nombre" required="" value="${encuestaInstance?.nombre}"/>

</div>

