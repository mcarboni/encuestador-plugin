
<%@ page import="encuestador.EventoParticipacion" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'eventoParticipacion.label', default: 'EventoParticipacion')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-eventoParticipacion" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-eventoParticipacion" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<g:sortableColumn property="fecha" title="${message(code: 'eventoParticipacion.fecha.label', default: 'Fecha')}" />
					
						<th><g:message code="eventoParticipacion.usuario.label" default="Usuario" /></th>
					
						<g:sortableColumn property="base" title="${message(code: 'eventoParticipacion.base.label', default: 'Base')}" />
					
						<g:sortableColumn property="estadoOrigen" title="${message(code: 'eventoParticipacion.estadoOrigen.label', default: 'Estado Origen')}" />
					
						<g:sortableColumn property="estadoDestino" title="${message(code: 'eventoParticipacion.estadoDestino.label', default: 'Estado Destino')}" />
					
						<g:sortableColumn property="observacion" title="${message(code: 'eventoParticipacion.observacion.label', default: 'Observacion')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${eventoParticipacionInstanceList}" status="i" var="eventoParticipacionInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${eventoParticipacionInstance.id}">${fieldValue(bean: eventoParticipacionInstance, field: "fecha")}</g:link></td>
					
						<td>${fieldValue(bean: eventoParticipacionInstance, field: "usuario")}</td>
					
						<td>${fieldValue(bean: eventoParticipacionInstance, field: "base")}</td>
					
						<td>${fieldValue(bean: eventoParticipacionInstance, field: "estadoOrigen")}</td>
					
						<td>${fieldValue(bean: eventoParticipacionInstance, field: "estadoDestino")}</td>
					
						<td>${fieldValue(bean: eventoParticipacionInstance, field: "observacion")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${eventoParticipacionInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
