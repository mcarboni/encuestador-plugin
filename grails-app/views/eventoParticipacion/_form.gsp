<%@ page import="encuestador.EventoParticipacion" %>



<div class="fieldcontain ${hasErrors(bean: eventoParticipacionInstance, field: 'fecha', 'error')} required">
	<label for="fecha">
		<g:message code="eventoParticipacion.fecha.label" default="Fecha" />
		<span class="required-indicator">*</span>
	</label>
	<g:datePicker name="fecha" precision="day"  value="${eventoParticipacionInstance?.fecha}"  />

</div>

<div class="fieldcontain ${hasErrors(bean: eventoParticipacionInstance, field: 'usuario', 'error')} required">
	<label for="usuario">
		<g:message code="eventoParticipacion.usuario.label" default="Usuario" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="usuario" name="usuario.id" from="${encuestador.security.Person.list()}" optionKey="id" required="" value="${eventoParticipacionInstance?.usuario?.id}" class="many-to-one"/>

</div>

<div class="fieldcontain ${hasErrors(bean: eventoParticipacionInstance, field: 'base', 'error')} required">
	<label for="base">
		<g:message code="eventoParticipacion.base.label" default="Base" />
		<span class="required-indicator">*</span>
	</label>
	<g:select name="base" from="${encuestador.Base?.values()}" keys="${encuestador.Base.values()*.name()}" required="" value="${eventoParticipacionInstance?.base?.name()}" />

</div>

<div class="fieldcontain ${hasErrors(bean: eventoParticipacionInstance, field: 'estadoOrigen', 'error')} ">
	<label for="estadoOrigen">
		<g:message code="eventoParticipacion.estadoOrigen.label" default="Estado Origen" />
		
	</label>
	<g:select name="estadoOrigen" from="${encuestador.EstadoEncuesta?.values()}" keys="${encuestador.EstadoEncuesta.values()*.name()}" value="${eventoParticipacionInstance?.estadoOrigen?.name()}" noSelection="['': '']"/>

</div>

<div class="fieldcontain ${hasErrors(bean: eventoParticipacionInstance, field: 'estadoDestino', 'error')} required">
	<label for="estadoDestino">
		<g:message code="eventoParticipacion.estadoDestino.label" default="Estado Destino" />
		<span class="required-indicator">*</span>
	</label>
	<g:select name="estadoDestino" from="${encuestador.EstadoEncuesta?.values()}" keys="${encuestador.EstadoEncuesta.values()*.name()}" required="" value="${eventoParticipacionInstance?.estadoDestino?.name()}" />

</div>

<div class="fieldcontain ${hasErrors(bean: eventoParticipacionInstance, field: 'observacion', 'error')} required">
	<label for="observacion">
		<g:message code="eventoParticipacion.observacion.label" default="Observacion" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="observacion" required="" value="${eventoParticipacionInstance?.observacion}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: eventoParticipacionInstance, field: 'participacion', 'error')} required">
	<label for="participacion">
		<g:message code="eventoParticipacion.participacion.label" default="Participacion" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="participacion" name="participacion.id" from="${encuestador.ParticipaEn.list()}" optionKey="id" required="" value="${eventoParticipacionInstance?.participacion?.id}" class="many-to-one"/>

</div>

