
<%@ page import="encuestador.EventoParticipacion" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'eventoParticipacion.label', default: 'EventoParticipacion')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-eventoParticipacion" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-eventoParticipacion" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list eventoParticipacion">
			
				<g:if test="${eventoParticipacionInstance?.fecha}">
				<li class="fieldcontain">
					<span id="fecha-label" class="property-label"><g:message code="eventoParticipacion.fecha.label" default="Fecha" /></span>
					
						<span class="property-value" aria-labelledby="fecha-label"><g:formatDate date="${eventoParticipacionInstance?.fecha}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${eventoParticipacionInstance?.usuario}">
				<li class="fieldcontain">
					<span id="usuario-label" class="property-label"><g:message code="eventoParticipacion.usuario.label" default="Usuario" /></span>
					
						<span class="property-value" aria-labelledby="usuario-label"><g:link controller="person" action="show" id="${eventoParticipacionInstance?.usuario?.id}">${eventoParticipacionInstance?.usuario?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${eventoParticipacionInstance?.base}">
				<li class="fieldcontain">
					<span id="base-label" class="property-label"><g:message code="eventoParticipacion.base.label" default="Base" /></span>
					
						<span class="property-value" aria-labelledby="base-label"><g:fieldValue bean="${eventoParticipacionInstance}" field="base"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${eventoParticipacionInstance?.estadoOrigen}">
				<li class="fieldcontain">
					<span id="estadoOrigen-label" class="property-label"><g:message code="eventoParticipacion.estadoOrigen.label" default="Estado Origen" /></span>
					
						<span class="property-value" aria-labelledby="estadoOrigen-label"><g:fieldValue bean="${eventoParticipacionInstance}" field="estadoOrigen"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${eventoParticipacionInstance?.estadoDestino}">
				<li class="fieldcontain">
					<span id="estadoDestino-label" class="property-label"><g:message code="eventoParticipacion.estadoDestino.label" default="Estado Destino" /></span>
					
						<span class="property-value" aria-labelledby="estadoDestino-label"><g:fieldValue bean="${eventoParticipacionInstance}" field="estadoDestino"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${eventoParticipacionInstance?.observacion}">
				<li class="fieldcontain">
					<span id="observacion-label" class="property-label"><g:message code="eventoParticipacion.observacion.label" default="Observacion" /></span>
					
						<span class="property-value" aria-labelledby="observacion-label"><g:fieldValue bean="${eventoParticipacionInstance}" field="observacion"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${eventoParticipacionInstance?.participacion}">
				<li class="fieldcontain">
					<span id="participacion-label" class="property-label"><g:message code="eventoParticipacion.participacion.label" default="Participacion" /></span>
					
						<span class="property-value" aria-labelledby="participacion-label"><g:link controller="participaEn" action="show" id="${eventoParticipacionInstance?.participacion?.id}">${eventoParticipacionInstance?.participacion?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:eventoParticipacionInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${eventoParticipacionInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
