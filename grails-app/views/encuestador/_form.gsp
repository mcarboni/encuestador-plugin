<%@ page import="encuestador.Encuestador" %>



<div class="fieldcontain ${hasErrors(bean: encuestadorInstance, field: 'email', 'error')} required">
	<label for="email">
		<g:message code="encuestador.email.label" default="Email" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="email" required="" value="${encuestadorInstance?.email}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: encuestadorInstance, field: 'nombre', 'error')} required">
	<label for="nombre">
		<g:message code="encuestador.nombre.label" default="Nombre" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="nombre" required="" value="${encuestadorInstance?.nombre}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: encuestadorInstance, field: 'user', 'error')} required">
	<label for="user">
		<g:message code="encuestador.user.label" default="User" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="user" name="user.id" from="${encuestador.security.Person.list()}" optionKey="id" required="" value="${encuestadorInstance?.user?.id}" class="many-to-one"/>

</div>

