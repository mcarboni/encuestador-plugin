
<%@ page import="encuestador.Encuestador" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'encuestador.label', default: 'Encuestador')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-encuestador" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-encuestador" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<g:sortableColumn property="email" title="${message(code: 'encuestador.email.label', default: 'Email')}" />
					
						<g:sortableColumn property="nombre" title="${message(code: 'encuestador.nombre.label', default: 'Nombre')}" />
					
						<th><g:message code="encuestador.user.label" default="User" /></th>
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${encuestadorInstanceList}" status="i" var="encuestadorInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${encuestadorInstance.id}">${fieldValue(bean: encuestadorInstance, field: "email")}</g:link></td>
					
						<td>${fieldValue(bean: encuestadorInstance, field: "nombre")}</td>
					
						<td>${fieldValue(bean: encuestadorInstance, field: "user")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${encuestadorInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
