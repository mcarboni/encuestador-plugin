<%@ page import="encuestador.Empresa" %>



<div class="fieldcontain ${hasErrors(bean: empresaInstance, field: 'nombre', 'error')} required">
	<label for="nombre">
		<g:message code="empresa.nombre.label" default="Nombre" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="nombre" required="" value="${empresaInstance?.nombre}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: empresaInstance, field: 'email', 'error')} required">
	<label for="email">
		<g:message code="empresa.email.label" default="Email" />
		<span class="required-indicator">*</span>
	</label>
	<g:field type="email" name="email" required="" value="${empresaInstance?.email}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: empresaInstance, field: 'cuit', 'error')} required">
	<label for="cuit">
		<g:message code="empresa.cuit.label" default="Cuit" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="cuit" required="" value="${empresaInstance?.cuit}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: empresaInstance, field: 'empresaId', 'error')} required">
	<label for="empresaId">
		<g:message code="empresa.empresaId.label" default="Empresa Id" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="empresaId" type="number" value="${empresaInstance.empresaId}" required=""/>

</div>

<div class="fieldcontain ${hasErrors(bean: empresaInstance, field: 'puntosDeContacto', 'error')} ">
	<label for="puntosDeContacto">
		<g:message code="empresa.puntosDeContacto.label" default="Puntos De Contacto" />
		
	</label>
	<g:select name="puntosDeContacto" from="${encuestador.PuntoDeContacto.list()}" multiple="multiple" optionKey="id" size="5" value="${empresaInstance?.puntosDeContacto*.id}" class="many-to-many"/>

</div>

<div class="fieldcontain ${hasErrors(bean: empresaInstance, field: 'user', 'error')} required">
	<label for="user">
		<g:message code="empresa.user.label" default="User" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="user" name="user.id" from="${encuestador.security.Person.list()}" optionKey="id" required="" value="${empresaInstance?.user?.id}" class="many-to-one"/>

</div>

