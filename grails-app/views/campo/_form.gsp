<%@ page import="encuestador.Campo" %>



<div class="fieldcontain ${hasErrors(bean: campoInstance, field: 'esObligatorio', 'error')} ">
	<label for="esObligatorio">
		<g:message code="campo.esObligatorio.label" default="Es Obligatorio" />
		
	</label>
	<g:checkBox name="esObligatorio" value="${campoInstance?.esObligatorio}" />

</div>

<div class="fieldcontain ${hasErrors(bean: campoInstance, field: 'etiqueta', 'error')} required">
	<label for="etiqueta">
		<g:message code="campo.etiqueta.label" default="Etiqueta" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="etiqueta" required="" value="${campoInstance?.etiqueta}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: campoInstance, field: 'formulario', 'error')} required">
	<label for="formulario">
		<g:message code="campo.formulario.label" default="Formulario" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="formulario" name="formulario.id" from="${encuestador.Formulario.list()}" optionKey="id" required="" value="${campoInstance?.formulario?.id}" class="many-to-one"/>

</div>

<div class="fieldcontain ${hasErrors(bean: campoInstance, field: 'longitud', 'error')} required">
	<label for="longitud">
		<g:message code="campo.longitud.label" default="Longitud" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="longitud" type="number" value="${campoInstance.longitud}" required=""/>

</div>

<div class="fieldcontain ${hasErrors(bean: campoInstance, field: 'nombre', 'error')} required">
	<label for="nombre">
		<g:message code="campo.nombre.label" default="Nombre" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="nombre" required="" value="${campoInstance?.nombre}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: campoInstance, field: 'orden', 'error')} required">
	<label for="orden">
		<g:message code="campo.orden.label" default="Orden" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="orden" type="number" value="${campoInstance.orden}" required=""/>

</div>

<div class="fieldcontain ${hasErrors(bean: campoInstance, field: 'tipo', 'error')} required">
	<label for="tipo">
		<g:message code="campo.tipo.label" default="Tipo" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="tipo" required="" value="${campoInstance?.tipo}"/>

</div>

