
<%@ page import="encuestador.Campo" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'campo.label', default: 'Campo')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-campo" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-campo" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list campo">
			
				<g:if test="${campoInstance?.esObligatorio}">
				<li class="fieldcontain">
					<span id="esObligatorio-label" class="property-label"><g:message code="campo.esObligatorio.label" default="Es Obligatorio" /></span>
					
						<span class="property-value" aria-labelledby="esObligatorio-label"><g:formatBoolean boolean="${campoInstance?.esObligatorio}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${campoInstance?.etiqueta}">
				<li class="fieldcontain">
					<span id="etiqueta-label" class="property-label"><g:message code="campo.etiqueta.label" default="Etiqueta" /></span>
					
						<span class="property-value" aria-labelledby="etiqueta-label"><g:fieldValue bean="${campoInstance}" field="etiqueta"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${campoInstance?.formulario}">
				<li class="fieldcontain">
					<span id="formulario-label" class="property-label"><g:message code="campo.formulario.label" default="Formulario" /></span>
					
						<span class="property-value" aria-labelledby="formulario-label"><g:link controller="formulario" action="show" id="${campoInstance?.formulario?.id}">${campoInstance?.formulario?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${campoInstance?.longitud}">
				<li class="fieldcontain">
					<span id="longitud-label" class="property-label"><g:message code="campo.longitud.label" default="Longitud" /></span>
					
						<span class="property-value" aria-labelledby="longitud-label"><g:fieldValue bean="${campoInstance}" field="longitud"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${campoInstance?.nombre}">
				<li class="fieldcontain">
					<span id="nombre-label" class="property-label"><g:message code="campo.nombre.label" default="Nombre" /></span>
					
						<span class="property-value" aria-labelledby="nombre-label"><g:fieldValue bean="${campoInstance}" field="nombre"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${campoInstance?.orden}">
				<li class="fieldcontain">
					<span id="orden-label" class="property-label"><g:message code="campo.orden.label" default="Orden" /></span>
					
						<span class="property-value" aria-labelledby="orden-label"><g:fieldValue bean="${campoInstance}" field="orden"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${campoInstance?.tipo}">
				<li class="fieldcontain">
					<span id="tipo-label" class="property-label"><g:message code="campo.tipo.label" default="Tipo" /></span>
					
						<span class="property-value" aria-labelledby="tipo-label"><g:fieldValue bean="${campoInstance}" field="tipo"/></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:campoInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${campoInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
