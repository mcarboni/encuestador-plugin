
<%@ page import="encuestador.Campo" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'campo.label', default: 'Campo')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-campo" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-campo" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<g:sortableColumn property="esObligatorio" title="${message(code: 'campo.esObligatorio.label', default: 'Es Obligatorio')}" />
					
						<g:sortableColumn property="etiqueta" title="${message(code: 'campo.etiqueta.label', default: 'Etiqueta')}" />
					
						<th><g:message code="campo.formulario.label" default="Formulario" /></th>
					
						<g:sortableColumn property="longitud" title="${message(code: 'campo.longitud.label', default: 'Longitud')}" />
					
						<g:sortableColumn property="nombre" title="${message(code: 'campo.nombre.label', default: 'Nombre')}" />
					
						<g:sortableColumn property="orden" title="${message(code: 'campo.orden.label', default: 'Orden')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${campoInstanceList}" status="i" var="campoInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${campoInstance.id}">${fieldValue(bean: campoInstance, field: "esObligatorio")}</g:link></td>
					
						<td>${fieldValue(bean: campoInstance, field: "etiqueta")}</td>
					
						<td>${fieldValue(bean: campoInstance, field: "formulario")}</td>
					
						<td>${fieldValue(bean: campoInstance, field: "longitud")}</td>
					
						<td>${fieldValue(bean: campoInstance, field: "nombre")}</td>
					
						<td>${fieldValue(bean: campoInstance, field: "orden")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${campoInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
