<%@ page import="encuestador.EventoContacto" %>



<div class="fieldcontain ${hasErrors(bean: eventoContactoInstance, field: 'comentarios', 'error')} required">
	<label for="comentarios">
		<g:message code="eventoContacto.comentarios.label" default="Comentarios" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="comentarios" required="" value="${eventoContactoInstance?.comentarios}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: eventoContactoInstance, field: 'contacto', 'error')} required">
	<label for="contacto">
		<g:message code="eventoContacto.contacto.label" default="Contacto" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="contacto" name="contacto.id" from="${encuestador.PuntoDeContacto.list()}" optionKey="id" required="" value="${eventoContactoInstance?.contacto?.id}" class="many-to-one"/>

</div>

<div class="fieldcontain ${hasErrors(bean: eventoContactoInstance, field: 'fecha', 'error')} required">
	<label for="fecha">
		<g:message code="eventoContacto.fecha.label" default="Fecha" />
		<span class="required-indicator">*</span>
	</label>
	<g:datePicker name="fecha" precision="day"  value="${eventoContactoInstance?.fecha}"  />

</div>

<div class="fieldcontain ${hasErrors(bean: eventoContactoInstance, field: 'titulo', 'error')} required">
	<label for="titulo">
		<g:message code="eventoContacto.titulo.label" default="Titulo" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="titulo" required="" value="${eventoContactoInstance?.titulo}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: eventoContactoInstance, field: 'usuario', 'error')} required">
	<label for="usuario">
		<g:message code="eventoContacto.usuario.label" default="Usuario" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="usuario" name="usuario.id" from="${encuestador.security.Person.list()}" optionKey="id" required="" value="${eventoContactoInstance?.usuario?.id}" class="many-to-one"/>

</div>

