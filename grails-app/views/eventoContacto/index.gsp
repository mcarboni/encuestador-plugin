
<%@ page import="encuestador.EventoContacto" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'eventoContacto.label', default: 'EventoContacto')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-eventoContacto" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-eventoContacto" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<g:sortableColumn property="comentarios" title="${message(code: 'eventoContacto.comentarios.label', default: 'Comentarios')}" />
					
						<th><g:message code="eventoContacto.contacto.label" default="Contacto" /></th>
					
						<g:sortableColumn property="fecha" title="${message(code: 'eventoContacto.fecha.label', default: 'Fecha')}" />
					
						<g:sortableColumn property="titulo" title="${message(code: 'eventoContacto.titulo.label', default: 'Titulo')}" />
					
						<th><g:message code="eventoContacto.usuario.label" default="Usuario" /></th>
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${eventoContactoInstanceList}" status="i" var="eventoContactoInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${eventoContactoInstance.id}">${fieldValue(bean: eventoContactoInstance, field: "comentarios")}</g:link></td>
					
						<td>${fieldValue(bean: eventoContactoInstance, field: "contacto")}</td>
					
						<td><g:formatDate date="${eventoContactoInstance.fecha}" /></td>
					
						<td>${fieldValue(bean: eventoContactoInstance, field: "titulo")}</td>
					
						<td>${fieldValue(bean: eventoContactoInstance, field: "usuario")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${eventoContactoInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
