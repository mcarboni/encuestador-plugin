
<%@ page import="encuestador.AsignacionParticipante" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'asignacionParticipante.label', default: 'AsignacionParticipante')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-asignacionParticipante" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-asignacionParticipante" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<th><g:message code="asignacionParticipante.encuestador.label" default="Encuestador" /></th>
					
						<th><g:message code="asignacionParticipante.participante.label" default="Participante" /></th>
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${asignacionParticipanteInstanceList}" status="i" var="asignacionParticipanteInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${asignacionParticipanteInstance.id}">${fieldValue(bean: asignacionParticipanteInstance, field: "encuestador")}</g:link></td>
					
						<td>${fieldValue(bean: asignacionParticipanteInstance, field: "participante")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${asignacionParticipanteInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
