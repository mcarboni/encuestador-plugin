<%@ page import="encuestador.AsignacionParticipante" %>



<div class="fieldcontain ${hasErrors(bean: asignacionParticipanteInstance, field: 'encuestador', 'error')} required">
	<label for="encuestador">
		<g:message code="asignacionParticipante.encuestador.label" default="Encuestador" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="encuestador" name="encuestador.id" from="${encuestador.Encuestador.list()}" optionKey="id" required="" value="${asignacionParticipanteInstance?.encuestador?.id}" class="many-to-one"/>

</div>

<div class="fieldcontain ${hasErrors(bean: asignacionParticipanteInstance, field: 'participante', 'error')} required">
	<label for="participante">
		<g:message code="asignacionParticipante.participante.label" default="Participante" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="participante" name="participante.id" from="${encuestador.ParticipaEn.list()}" optionKey="id" required="" value="${asignacionParticipanteInstance?.participante?.id}" class="many-to-one"/>

</div>

