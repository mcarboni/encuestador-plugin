
<%@ page import="encuestador.ContactoTemplate" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'contactoTemplate.label', default: 'ContactoTemplate')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-contactoTemplate" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-contactoTemplate" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list contactoTemplate">
			
				<g:if test="${contactoTemplateInstance?.codigo}">
				<li class="fieldcontain">
					<span id="codigo-label" class="property-label"><g:message code="contactoTemplate.codigo.label" default="Codigo" /></span>
					
						<span class="property-value" aria-labelledby="codigo-label"><g:fieldValue bean="${contactoTemplateInstance}" field="codigo"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${contactoTemplateInstance?.asunto}">
				<li class="fieldcontain">
					<span id="asunto-label" class="property-label"><g:message code="contactoTemplate.asunto.label" default="Asunto" /></span>
					
						<span class="property-value" aria-labelledby="asunto-label"><g:fieldValue bean="${contactoTemplateInstance}" field="asunto"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${contactoTemplateInstance?.template}">
				<li class="fieldcontain">
					<span id="template-label" class="property-label"><g:message code="contactoTemplate.template.label" default="Template" /></span>
					
						<span class="property-value" aria-labelledby="template-label"><g:fieldValue bean="${contactoTemplateInstance}" field="template"/></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:contactoTemplateInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${contactoTemplateInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
