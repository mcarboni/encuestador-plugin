<%@ page import="encuestador.ContactoTemplate" %>



<div class="fieldcontain ${hasErrors(bean: contactoTemplateInstance, field: 'codigo', 'error')} required">
	<label for="codigo">
		<g:message code="contactoTemplate.codigo.label" default="Codigo" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="codigo" required="" value="${contactoTemplateInstance?.codigo}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: contactoTemplateInstance, field: 'asunto', 'error')} required">
	<label for="asunto">
		<g:message code="contactoTemplate.asunto.label" default="Asunto" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="asunto" required="" value="${contactoTemplateInstance?.asunto}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: contactoTemplateInstance, field: 'template', 'error')} required">
	<label for="template">
		<g:message code="contactoTemplate.template.label" default="Template" />
		<span class="required-indicator">*</span>
	</label>
	<cmeditor:textArea name="template" binding="vim" required="" value="${contactoTemplateInstance?.template}"></cmeditor:textArea>

</div>

