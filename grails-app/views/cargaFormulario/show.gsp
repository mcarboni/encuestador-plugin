
<%@ page import="encuestador.CargaFormulario" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'cargaFormulario.label', default: 'CargaFormulario')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-cargaFormulario" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-cargaFormulario" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list cargaFormulario">
			
				<g:if test="${cargaFormularioInstance?.creadoPor}">
				<li class="fieldcontain">
					<span id="creadoPor-label" class="property-label"><g:message code="cargaFormulario.creadoPor.label" default="Creado Por" /></span>
					
						<span class="property-value" aria-labelledby="creadoPor-label"><g:fieldValue bean="${cargaFormularioInstance}" field="creadoPor"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${cargaFormularioInstance?.creado}">
				<li class="fieldcontain">
					<span id="creado-label" class="property-label"><g:message code="cargaFormulario.creado.label" default="Creado" /></span>
					
						<span class="property-value" aria-labelledby="creado-label"><g:formatDate date="${cargaFormularioInstance?.creado}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${cargaFormularioInstance?.actualizadoPor}">
				<li class="fieldcontain">
					<span id="actualizadoPor-label" class="property-label"><g:message code="cargaFormulario.actualizadoPor.label" default="Actualizado Por" /></span>
					
						<span class="property-value" aria-labelledby="actualizadoPor-label"><g:fieldValue bean="${cargaFormularioInstance}" field="actualizadoPor"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${cargaFormularioInstance?.actualizado}">
				<li class="fieldcontain">
					<span id="actualizado-label" class="property-label"><g:message code="cargaFormulario.actualizado.label" default="Actualizado" /></span>
					
						<span class="property-value" aria-labelledby="actualizado-label"><g:formatDate date="${cargaFormularioInstance?.actualizado}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${cargaFormularioInstance?.formulario}">
				<li class="fieldcontain">
					<span id="formulario-label" class="property-label"><g:message code="cargaFormulario.formulario.label" default="Formulario" /></span>
					
						<span class="property-value" aria-labelledby="formulario-label"><g:link controller="formulario" action="show" id="${cargaFormularioInstance?.formulario?.id}">${cargaFormularioInstance?.formulario?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${cargaFormularioInstance?.participante}">
				<li class="fieldcontain">
					<span id="participante-label" class="property-label"><g:message code="cargaFormulario.participante.label" default="Participante" /></span>
					
						<span class="property-value" aria-labelledby="participante-label"><g:link controller="participaEn" action="show" id="${cargaFormularioInstance?.participante?.id}">${cargaFormularioInstance?.participante?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${cargaFormularioInstance?.base}">
				<li class="fieldcontain">
					<span id="base-label" class="property-label"><g:message code="cargaFormulario.base.label" default="Base" /></span>
					
						<span class="property-value" aria-labelledby="base-label"><g:fieldValue bean="${cargaFormularioInstance}" field="base"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${cargaFormularioInstance?.datos}">
				<li class="fieldcontain">
					<span id="datos-label" class="property-label"><g:message code="cargaFormulario.datos.label" default="Datos" /></span>
					
						<span class="property-value" aria-labelledby="datos-label"><g:fieldValue bean="${cargaFormularioInstance}" field="datos"/></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:cargaFormularioInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${cargaFormularioInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
