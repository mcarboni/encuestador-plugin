<%@ page import="encuestador.CargaFormulario" %>



<div class="fieldcontain ${hasErrors(bean: cargaFormularioInstance, field: 'creadoPor', 'error')} required">
	<label for="creadoPor">
		<g:message code="cargaFormulario.creadoPor.label" default="Creado Por" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="creadoPor" required="" value="${cargaFormularioInstance?.creadoPor}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: cargaFormularioInstance, field: 'creado', 'error')} required">
	<label for="creado">
		<g:message code="cargaFormulario.creado.label" default="Creado" />
		<span class="required-indicator">*</span>
	</label>
	<g:datePicker name="creado" precision="day"  value="${cargaFormularioInstance?.creado}"  />

</div>

<div class="fieldcontain ${hasErrors(bean: cargaFormularioInstance, field: 'actualizadoPor', 'error')} required">
	<label for="actualizadoPor">
		<g:message code="cargaFormulario.actualizadoPor.label" default="Actualizado Por" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="actualizadoPor" required="" value="${cargaFormularioInstance?.actualizadoPor}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: cargaFormularioInstance, field: 'actualizado', 'error')} required">
	<label for="actualizado">
		<g:message code="cargaFormulario.actualizado.label" default="Actualizado" />
		<span class="required-indicator">*</span>
	</label>
	<g:datePicker name="actualizado" precision="day"  value="${cargaFormularioInstance?.actualizado}"  />

</div>

<div class="fieldcontain ${hasErrors(bean: cargaFormularioInstance, field: 'formulario', 'error')} required">
	<label for="formulario">
		<g:message code="cargaFormulario.formulario.label" default="Formulario" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="formulario" name="formulario.id" from="${encuestador.Formulario.list()}" optionKey="id" required="" value="${cargaFormularioInstance?.formulario?.id}" class="many-to-one"/>

</div>

<div class="fieldcontain ${hasErrors(bean: cargaFormularioInstance, field: 'participante', 'error')} required">
	<label for="participante">
		<g:message code="cargaFormulario.participante.label" default="Participante" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="participante" name="participante.id" from="${encuestador.ParticipaEn.list()}" optionKey="id" required="" value="${cargaFormularioInstance?.participante?.id}" class="many-to-one"/>

</div>

<div class="fieldcontain ${hasErrors(bean: cargaFormularioInstance, field: 'base', 'error')} required">
	<label for="base">
		<g:message code="cargaFormulario.base.label" default="Base" />
		<span class="required-indicator">*</span>
	</label>
	<g:select name="base" from="${encuestador.Base?.values()}" keys="${encuestador.Base.values()*.name()}" required="" value="${cargaFormularioInstance?.base?.name()}" />

</div>

<div class="fieldcontain ${hasErrors(bean: cargaFormularioInstance, field: 'datos', 'error')} required">
	<label for="datos">
		<g:message code="cargaFormulario.datos.label" default="Datos" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="datos" required="" value="${cargaFormularioInstance?.datos}"/>

</div>

