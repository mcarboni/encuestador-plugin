
<%@ page import="encuestador.CargaFormulario" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'cargaFormulario.label', default: 'CargaFormulario')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-cargaFormulario" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-cargaFormulario" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<g:sortableColumn property="creadoPor" title="${message(code: 'cargaFormulario.creadoPor.label', default: 'Creado Por')}" />
					
						<g:sortableColumn property="creado" title="${message(code: 'cargaFormulario.creado.label', default: 'Creado')}" />
					
						<g:sortableColumn property="actualizadoPor" title="${message(code: 'cargaFormulario.actualizadoPor.label', default: 'Actualizado Por')}" />
					
						<g:sortableColumn property="actualizado" title="${message(code: 'cargaFormulario.actualizado.label', default: 'Actualizado')}" />
					
						<th><g:message code="cargaFormulario.formulario.label" default="Formulario" /></th>
					
						<th><g:message code="cargaFormulario.participante.label" default="Participante" /></th>
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${cargaFormularioInstanceList}" status="i" var="cargaFormularioInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${cargaFormularioInstance.id}">${fieldValue(bean: cargaFormularioInstance, field: "creadoPor")}</g:link></td>
					
						<td><g:formatDate date="${cargaFormularioInstance.creado}" /></td>
					
						<td>${fieldValue(bean: cargaFormularioInstance, field: "actualizadoPor")}</td>
					
						<td><g:formatDate date="${cargaFormularioInstance.actualizado}" /></td>
					
						<td>${fieldValue(bean: cargaFormularioInstance, field: "formulario")}</td>
					
						<td>${fieldValue(bean: cargaFormularioInstance, field: "participante")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${cargaFormularioInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
