<%@ page import="encuestador.ValorListaControlada" %>



<div class="fieldcontain ${hasErrors(bean: valorListaControladaInstance, field: 'orden', 'error')} required">
	<label for="orden">
		<g:message code="valorListaControlada.orden.label" default="Orden" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="orden" type="number" value="${valorListaControladaInstance.orden}" required=""/>

</div>

<div class="fieldcontain ${hasErrors(bean: valorListaControladaInstance, field: 'codigo', 'error')} required">
	<label for="codigo">
		<g:message code="valorListaControlada.codigo.label" default="Codigo" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="codigo" required="" value="${valorListaControladaInstance?.codigo}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: valorListaControladaInstance, field: 'descripcion', 'error')} required">
	<label for="descripcion">
		<g:message code="valorListaControlada.descripcion.label" default="Descripcion" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="descripcion" required="" value="${valorListaControladaInstance?.descripcion}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: valorListaControladaInstance, field: 'discriminador', 'error')} ">
	<label for="discriminador">
		<g:message code="valorListaControlada.discriminador.label" default="Discriminador" />
		
	</label>
	<g:textField name="discriminador" value="${valorListaControladaInstance?.discriminador}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: valorListaControladaInstance, field: 'lista', 'error')} required">
	<label for="lista">
		<g:message code="valorListaControlada.lista.label" default="Lista" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="lista" name="lista.id" from="${encuestador.ListaControlada.list()}" optionKey="id" required="" value="${valorListaControladaInstance?.lista?.id}" class="many-to-one"/>

</div>

