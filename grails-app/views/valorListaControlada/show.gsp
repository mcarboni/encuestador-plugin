
<%@ page import="encuestador.ValorListaControlada" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'valorListaControlada.label', default: 'ValorListaControlada')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-valorListaControlada" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-valorListaControlada" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list valorListaControlada">
			
				<g:if test="${valorListaControladaInstance?.orden}">
				<li class="fieldcontain">
					<span id="orden-label" class="property-label"><g:message code="valorListaControlada.orden.label" default="Orden" /></span>
					
						<span class="property-value" aria-labelledby="orden-label"><g:fieldValue bean="${valorListaControladaInstance}" field="orden"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${valorListaControladaInstance?.codigo}">
				<li class="fieldcontain">
					<span id="codigo-label" class="property-label"><g:message code="valorListaControlada.codigo.label" default="Codigo" /></span>
					
						<span class="property-value" aria-labelledby="codigo-label"><g:fieldValue bean="${valorListaControladaInstance}" field="codigo"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${valorListaControladaInstance?.descripcion}">
				<li class="fieldcontain">
					<span id="descripcion-label" class="property-label"><g:message code="valorListaControlada.descripcion.label" default="Descripcion" /></span>
					
						<span class="property-value" aria-labelledby="descripcion-label"><g:fieldValue bean="${valorListaControladaInstance}" field="descripcion"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${valorListaControladaInstance?.discriminador}">
				<li class="fieldcontain">
					<span id="discriminador-label" class="property-label"><g:message code="valorListaControlada.discriminador.label" default="Discriminador" /></span>
					
						<span class="property-value" aria-labelledby="discriminador-label"><g:fieldValue bean="${valorListaControladaInstance}" field="discriminador"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${valorListaControladaInstance?.lista}">
				<li class="fieldcontain">
					<span id="lista-label" class="property-label"><g:message code="valorListaControlada.lista.label" default="Lista" /></span>
					
						<span class="property-value" aria-labelledby="lista-label"><g:link controller="listaControlada" action="show" id="${valorListaControladaInstance?.lista?.id}">${valorListaControladaInstance?.lista?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:valorListaControladaInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${valorListaControladaInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
