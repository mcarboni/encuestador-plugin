
<%@ page import="encuestador.ValorListaControlada" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'valorListaControlada.label', default: 'ValorListaControlada')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-valorListaControlada" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-valorListaControlada" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<g:sortableColumn property="orden" title="${message(code: 'valorListaControlada.orden.label', default: 'Orden')}" />
					
						<g:sortableColumn property="codigo" title="${message(code: 'valorListaControlada.codigo.label', default: 'Codigo')}" />
					
						<g:sortableColumn property="descripcion" title="${message(code: 'valorListaControlada.descripcion.label', default: 'Descripcion')}" />
					
						<g:sortableColumn property="discriminador" title="${message(code: 'valorListaControlada.discriminador.label', default: 'Discriminador')}" />
					
						<th><g:message code="valorListaControlada.lista.label" default="Lista" /></th>
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${valorListaControladaInstanceList}" status="i" var="valorListaControladaInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${valorListaControladaInstance.id}">${fieldValue(bean: valorListaControladaInstance, field: "orden")}</g:link></td>
					
						<td>${fieldValue(bean: valorListaControladaInstance, field: "codigo")}</td>
					
						<td>${fieldValue(bean: valorListaControladaInstance, field: "descripcion")}</td>
					
						<td>${fieldValue(bean: valorListaControladaInstance, field: "discriminador")}</td>
					
						<td>${fieldValue(bean: valorListaControladaInstance, field: "lista")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${valorListaControladaInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
