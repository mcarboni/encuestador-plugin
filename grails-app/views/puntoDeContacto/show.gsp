
<%@ page import="encuestador.PuntoDeContacto" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'puntoDeContacto.label', default: 'PuntoDeContacto')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-puntoDeContacto" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-puntoDeContacto" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list puntoDeContacto">
			
				<g:if test="${puntoDeContactoInstance?.contacto}">
				<li class="fieldcontain">
					<span id="contacto-label" class="property-label"><g:message code="puntoDeContacto.contacto.label" default="Contacto" /></span>
					
						<span class="property-value" aria-labelledby="contacto-label"><g:fieldValue bean="${puntoDeContactoInstance}" field="contacto"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${puntoDeContactoInstance?.persona}">
				<li class="fieldcontain">
					<span id="persona-label" class="property-label"><g:message code="puntoDeContacto.persona.label" default="Persona" /></span>
					
						<span class="property-value" aria-labelledby="persona-label"><g:fieldValue bean="${puntoDeContactoInstance}" field="persona"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${puntoDeContactoInstance?.posicion}">
				<li class="fieldcontain">
					<span id="posicion-label" class="property-label"><g:message code="puntoDeContacto.posicion.label" default="Posicion" /></span>
					
						<span class="property-value" aria-labelledby="posicion-label"><g:fieldValue bean="${puntoDeContactoInstance}" field="posicion"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${puntoDeContactoInstance?.tipo}">
				<li class="fieldcontain">
					<span id="tipo-label" class="property-label"><g:message code="puntoDeContacto.tipo.label" default="Tipo" /></span>
					
						<span class="property-value" aria-labelledby="tipo-label"><g:fieldValue bean="${puntoDeContactoInstance}" field="tipo"/></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:puntoDeContactoInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${puntoDeContactoInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
