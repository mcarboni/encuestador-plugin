<%@ page import="encuestador.PuntoDeContacto" %>



<div class="fieldcontain ${hasErrors(bean: puntoDeContactoInstance, field: 'contacto', 'error')} required">
	<label for="contacto">
		<g:message code="puntoDeContacto.contacto.label" default="Contacto" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="contacto" required="" value="${puntoDeContactoInstance?.contacto}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: puntoDeContactoInstance, field: 'persona', 'error')} required">
	<label for="persona">
		<g:message code="puntoDeContacto.persona.label" default="Persona" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="persona" required="" value="${puntoDeContactoInstance?.persona}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: puntoDeContactoInstance, field: 'posicion', 'error')} required">
	<label for="posicion">
		<g:message code="puntoDeContacto.posicion.label" default="Posicion" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="posicion" required="" value="${puntoDeContactoInstance?.posicion}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: puntoDeContactoInstance, field: 'tipo', 'error')} required">
	<label for="tipo">
		<g:message code="puntoDeContacto.tipo.label" default="Tipo" />
		<span class="required-indicator">*</span>
	</label>
	<g:select name="tipo" from="${encuestador.TipoPuntoDeContacto?.values()}" keys="${encuestador.TipoPuntoDeContacto.values()*.name()}" required="" value="${puntoDeContactoInstance?.tipo?.name()}" />

</div>

