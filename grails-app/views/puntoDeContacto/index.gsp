
<%@ page import="encuestador.PuntoDeContacto" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'puntoDeContacto.label', default: 'PuntoDeContacto')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-puntoDeContacto" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-puntoDeContacto" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<g:sortableColumn property="contacto" title="${message(code: 'puntoDeContacto.contacto.label', default: 'Contacto')}" />
					
						<g:sortableColumn property="persona" title="${message(code: 'puntoDeContacto.persona.label', default: 'Persona')}" />
					
						<g:sortableColumn property="posicion" title="${message(code: 'puntoDeContacto.posicion.label', default: 'Posicion')}" />
					
						<g:sortableColumn property="tipo" title="${message(code: 'puntoDeContacto.tipo.label', default: 'Tipo')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${puntoDeContactoInstanceList}" status="i" var="puntoDeContactoInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${puntoDeContactoInstance.id}">${fieldValue(bean: puntoDeContactoInstance, field: "contacto")}</g:link></td>
					
						<td>${fieldValue(bean: puntoDeContactoInstance, field: "persona")}</td>
					
						<td>${fieldValue(bean: puntoDeContactoInstance, field: "posicion")}</td>
					
						<td>${fieldValue(bean: puntoDeContactoInstance, field: "tipo")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${puntoDeContactoInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
