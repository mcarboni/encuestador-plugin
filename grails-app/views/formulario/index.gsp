
<%@ page import="encuestador.Formulario" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'formulario.label', default: 'Formulario')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-formulario" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-formulario" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<th><g:message code="formulario.edicion.label" default="Edicion" /></th>
					
						<g:sortableColumn property="orden" title="${message(code: 'formulario.orden.label', default: 'Orden')}" />
					
						<g:sortableColumn property="tabla" title="${message(code: 'formulario.tabla.label', default: 'Tabla')}" />
					
<%--						<g:sortableColumn property="template" title="${message(code: 'formulario.template.label', default: 'Template')}" />--%>
					
						<g:sortableColumn property="titulo" title="${message(code: 'formulario.titulo.label', default: 'Titulo')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${formularioInstanceList}" status="i" var="formularioInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${formularioInstance.id}">${fieldValue(bean: formularioInstance, field: "edicion")}</g:link></td>
					
						<td>${fieldValue(bean: formularioInstance, field: "orden")}</td>
					
						<td>${fieldValue(bean: formularioInstance, field: "tabla")}</td>
					
<%--						<td>${fieldValue(bean: formularioInstance, field: "template")}</td>--%>
					
						<td>${fieldValue(bean: formularioInstance, field: "titulo")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${formularioInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
