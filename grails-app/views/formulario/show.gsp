
<%@ page import="encuestador.Formulario" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'formulario.label', default: 'Formulario')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-formulario" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-formulario" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list formulario">
			
				<g:if test="${formularioInstance?.edicion}">
				<li class="fieldcontain">
					<span id="edicion-label" class="property-label"><g:message code="formulario.edicion.label" default="edicion" /></span>
					
						<span class="property-value" aria-labelledby="edicion-label"><g:link controller="edicion" action="show" id="${formularioInstance?.edicion?.id}">${formularioInstance?.edicion?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${formularioInstance?.orden}">
				<li class="fieldcontain">
					<span id="orden-label" class="property-label"><g:message code="formulario.orden.label" default="Orden" /></span>
					
						<span class="property-value" aria-labelledby="orden-label"><g:fieldValue bean="${formularioInstance}" field="orden"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${formularioInstance?.tabla}">
				<li class="fieldcontain">
					<span id="tabla-label" class="property-label"><g:message code="formulario.tabla.label" default="Tabla" /></span>
					
						<span class="property-value" aria-labelledby="tabla-label"><g:fieldValue bean="${formularioInstance}" field="tabla"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${formularioInstance?.template}">
				<li class="fieldcontain">
					<span id="template-label" class="property-label"><g:message code="formulario.template.label" default="Template" /></span>
					
						<span class="property-value" aria-labelledby="template-label"><g:fieldValue bean="${formularioInstance}" field="template"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${formularioInstance?.campos}">
				<li class="fieldcontain">
					<span id="template-label" class="property-label"><g:message code="formulario.campos.label" default="Campos" /></span>
					
						<span class="property-value" aria-labelledby="campos-label"><g:fieldValue bean="${formularioInstance}" field="campos"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${formularioInstance?.titulo}">
				<li class="fieldcontain">
					<span id="titulo-label" class="property-label"><g:message code="formulario.titulo.label" default="Titulo" /></span>
					
						<span class="property-value" aria-labelledby="titulo-label"><g:fieldValue bean="${formularioInstance}" field="titulo"/></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:formularioInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${formularioInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:link class="list" action="refrescarTemplates" resource="${formularioInstance}">Refrescar templates</g:link>
					<g:link class="edit" action="obtenerCampos" resource="${formularioInstance}">Obtener campos</g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
