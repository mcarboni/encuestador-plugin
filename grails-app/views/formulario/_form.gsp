<%@ page import="encuestador.Edicion; encuestador.Formulario" %>



<div class="fieldcontain ${hasErrors(bean: formularioInstance, field: 'edicion', 'error')} required">
	<label for="edicion">
		<g:message code="formulario.edicion.label" default="Edicion" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="edicion" name="edicion.id" from="${encuestador.Edicion.list()}" optionKey="id" required="" value="${formularioInstance?.edicion?.id}" class="many-to-one"/>

</div>

<div class="fieldcontain ${hasErrors(bean: formularioInstance, field: 'orden', 'error')} required">
	<label for="orden">
		<g:message code="formulario.orden.label" default="Orden" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="orden" type="number" value="${formularioInstance.orden}" required=""/>

</div>

<div class="fieldcontain ${hasErrors(bean: formularioInstance, field: 'tabla', 'error')} required">
	<label for="tabla">
		<g:message code="formulario.tabla.label" default="Tabla" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="tabla" required="" value="${formularioInstance?.tabla}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: formularioInstance, field: 'template', 'error')} required">
	<label for="template">
		<g:message code="formulario.template.label" default="Template" />
		<span class="required-indicator">*</span>
	</label>
    <cmeditor:textArea name="template" binding="vim" required="" value="${formularioInstance?.template}"></cmeditor:textArea>
</div>

<div class="fieldcontain ${hasErrors(bean: formularioInstance, field: 'templateVista', 'error')} required">
	<label for="template">
		<g:message code="formulario.templateVista.label" default="Template Vista" />
		<span class="required-indicator">*</span>
	</label>
    <cmeditor:textArea name="templateVista" binding="vim" required="" value="${formularioInstance?.templateVista}"></cmeditor:textArea>
</div>

<div class="fieldcontain ${hasErrors(bean: formularioInstance, field: 'scriptValidacion', 'error')} required">
    <label for="template">
        <g:message code="formulario.scriptValidacion.label" default="Script Validacion" />
        <span class="required-indicator">*</span>
    </label>
    <cmeditor:textArea name="scriptValidacion" mode="text/javascript" required="" value="${formularioInstance?.scriptValidacion}"></cmeditor:textArea>
</div>

<div class="fieldcontain ${hasErrors(bean: formularioInstance, field: 'titulo', 'error')} required">
	<label for="titulo">
		<g:message code="formulario.titulo.label" default="Titulo" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="titulo" required="" value="${formularioInstance?.titulo}"/>

</div>

<div
		class="fieldcontain ${hasErrors(bean: formularioInstance, field: 'obligatorio', 'error')} ">
	<label for="obligatorio"> <g:message
			code="formulario.obligatorio.label" default="Es Obligatorio?" />

	</label>
	<g:checkBox name="obligatorio" value="${formularioInstance?.obligatorio}" />

</div>

