class EncuestadorPluginUrlMappings {

	static mappings = {
		"/api/manager/$controller/$action?/$id?(.$format)?" {
			controller = $controller
			namespace = "manager"
		}
		//        "/api/empresa/encuesta"(resources: 'encuesta', namespace: "empresa")
		//        "/api/empresa/formulario"(resources: 'formulario', namespace: "empresa")

		"/api/listas/buscarElementos" {
			controller = "listas"
			action = "buscarElementos"
			namespace = "api"
		}

		"/api/listas/$id" {
			controller = "listas"
			namespace = "api"
		}

		"/api/estados" {
			action = "estados"
			controller = "listas"
			namespace= "api"
		}

		"/api/$controller/$action?/$id?(.$format)?" {
			controller = $controller
			namespace = "api"
		}

		"/$controller/$action?/$id?(.$format)?"{ constraints { // apply constraints here
			} }

		"/"(view:"/index")
		"500"(view:'/error')
	}
}
