grails.plugins.twitterbootstrap.fixtaglib = true
grails.plugins.twitterbootstrap.defaultBundle = 'bundle_bootstrap'

// Added by the Spring Security Core plugin:
grails.plugin.springsecurity.userLookup.userDomainClassName = 'encuestador.security.Person'
grails.plugin.springsecurity.userLookup.authorityJoinClassName = 'encuestador.security.PersonAuthority'
grails.plugin.springsecurity.authority.className = 'encuestador.security.Authority'
grails.plugin.springsecurity.requestMap.className = 'encuestador.security.Requestmap'
grails.plugin.springsecurity.securityConfigType = 'Requestmap'
grails.plugin.springsecurity.filterChain.chainMap = [
        '/api/**': 'JOINED_FILTERS,-exceptionTranslationFilter,-authenticationProcessingFilter,-securityContextPersistenceFilter,-rememberMeAuthenticationFilter',  // Stateless chain
        '/**': 'JOINED_FILTERS,-restTokenValidationFilter,-restExceptionTranslationFilter'                                                                          // Traditional chain
]

grails.plugin.springsecurity.rest.login.useJsonCredentials=true
grails.plugin.springsecurity.rest.login.usernamePropertyName='username'
grails.plugin.springsecurity.rest.login.passwordPropertyName='password'
grails.plugin.springsecurity.rest.login.active=true
grails.plugin.springsecurity.rest.login.failureStatusCode=401
grails.plugin.springsecurity.rest.login.endpointUrl='/api/authenticate'
grails.plugin.springsecurity.rest.logout.endpointUrl='/api/logout'
grails.plugin.springsecurity.rest.token.validation.endpointUrl='/api/validate'
grails.plugin.springsecurity.rest.token.storage.useGrailsCache=true
grails.plugin.springsecurity.rest.token.storage.grailsCacheName='tokens'
grails.plugin.springsecurity.rest.token.storage.useJwt=true
//grails.plugin.springsecurity.rest.token.storage.jwt.useSignedJwt=true
//grails.plugin.springsecurity.rest.token.storage.jwt.secret='qrD6h8K6S9503Q06Y6Rfk21TErImPYqa'
//grails.plugin.springsecurity.rest.token.storage.jwt.expiration=1800

environments {
    development {
        grails {
            mail {
                host = "smtp.mincyt.gov.ar"
                port = 25
            }
        }
    }
    test {
        grails {
            mail {
                host = "goku.mincyt.gov.ar"
                port = 25
            }
        }
    }
    production {
        grails {
            mail {
                host = "goku.mincyt.gov.ar"
                port = 25
            }
        }
    }
}
