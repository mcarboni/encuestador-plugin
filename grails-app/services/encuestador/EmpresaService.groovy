package encuestador

import grails.transaction.Transactional
import encuestador.security.Person
import groovy.json.JsonSlurper
import groovy.json.JsonBuilder

import javax.script.Bindings
import javax.script.ScriptContext
import javax.script.ScriptEngine
import javax.script.ScriptEngineManager
import javax.script.SimpleScriptContext

@Transactional
class EmpresaService {

    ScriptEngineManager manager = new ScriptEngineManager()
    ScriptEngine engine = manager.getEngineByName("JavaScript")

    def buscar(user, String id = null) {
        if (!id) { // si no pasó id trae todas las encuestas del usuario
            def enq = ParticipaEn.createCriteria().list {
                edicion {
                    eq('habilitado', true)
                    order('anio', 'desc')
                }
                empresa {
                    eq('user', user)
                }
            }

            enq
        } else { // si pasó el id trae SOLO la encuesta solicitado para el usuario
            def enq = ParticipaEn.createCriteria().get {
                edicion {
                    eq('codigo', id)
                    eq('habilitado', true)
                }
                empresa {
                    eq('user', user)
                }
            }
            enq
        }
    }

    @Transactional
    def cambiarEstado(Person user, ParticipaEn enq, EstadoEncuesta nuevoEstado) {
        def elEstado = 'error'
        def elMensaje = 'Problemas intentando cambiar al estado $nuevoEstado'
        def estadoActual = enq.estadoBase1
        if ((estadoActual == EstadoEncuesta.PRECARGADA || estadoActual == EstadoEncuesta.INVITADA) && nuevoEstado == EstadoEncuesta.ABIERTA) {
            elEstado = 'success'
            elMensaje = 'La empresa abrio la encuesta.'
            enq.estadoBase1 = nuevoEstado
            enq.fechaApertura = new Date()
            enq.save(flush: true)
            new EventoParticipacion(fecha: enq.fechaApertura, usuario: user, estadoOrigen: estadoActual, estadoDestino: nuevoEstado, base: Base.BASE_UNO, observacion: elMensaje, participacion: enq).save(flush: true)
        } else if ((estadoActual == EstadoEncuesta.PRECARGADA || estadoActual == EstadoEncuesta.ABIERTA || estadoActual == EstadoEncuesta.REABIERTA) && nuevoEstado == EstadoEncuesta.POR_VALIDAR) {
            elEstado = 'success'
            elMensaje = 'La empresa envio la encuesta.'
            enq.estadoBase1 = nuevoEstado
            enq.fechaEnvio = new Date()
            enq.save(flush: true)
            new EventoParticipacion(fecha: enq.fechaEnvio, usuario: user, estadoOrigen: estadoActual, estadoDestino: nuevoEstado, base: Base.BASE_UNO, observacion: elMensaje, participacion: enq).save(flush: true)
        }

        [estado: elEstado, mensaje: elMensaje]
    }

    def buscarOCrearFormulario(user, Long id) {
        def cargafrm = CargaFormulario.createCriteria().get {
            participante {
                empresa {
                    eq('user', user)
                }
            }
            formulario {
                eq('id', id)
            }
        }
        if (!cargafrm) { // la empresa no cargo el formulario aun
            // busco el formulario
            def base = Base.BASE_UNO
            def Formulario frm = Formulario.get(id)
            def empr = Empresa.findByUser(user)
            
            if (frm) {
                // me fijo si el usuario participa en la encuesta correspondiente al formulario
                def prt = ParticipaEn.findByEmpresaAndEdicion(empr,frm.edicion)
				def estadosEditables = [EstadoEncuesta.ABIERTA, /*EstadoEncuesta.INVITADA, */EstadoEncuesta.REABIERTA]
//				def editable = estadosEditables.any { it.equals(prt.estadoBase1) }

                if (prt) {
                    // se lo doy de alta
                    cargafrm = new CargaFormulario(creadoPor: user.username, base: base, formulario: frm, participante: prt)
                }
            }
        }

        sincronizar(cargafrm)

        cargafrm
    }

    def sincronizar(CargaFormulario cf) {
        def prt = cf.participante
        def frm = cf.formulario

        def ds = Dataset.findByCodigo("${prt.empresa.empresaId}_${prt.edicion.encuesta.codigo}")
        if (ds && (!prt.fechaSincronizacion || ds.actualizado.after(prt.fechaSincronizacion))) {
            sincronizarParticipaEn(prt,ds)
        }

        def hayFusion = !cf.id || (!prt.fechaSincronizacion || cf.actualizado.before(prt.fechaSincronizacion))

        if (hayFusion) {
            def js = new JsonSlurper().parseText(prt.contexto)
            def datos = new JsonSlurper().parseText(cf.datos)

            // fusionando!!
            if (js."$frm.tabla") {
                def tabla = js."$frm.tabla"
                tabla.each{ k, v -> datos.putAt(k,v) }
                def b =  new JsonBuilder( datos ).toPrettyString()
                cf.datos = b
            }
        }
        cf
    }

    def buscarEncuestas(def user, String id=null, Boolean habilitado = true) {

        def enqs = ParticipaEn.createCriteria().list {
            empresa {
                eq('user', user)
            }
            edicion {
                eq('habilitado', habilitado)
                encuesta {
                    eq('habilitado', habilitado)
                    if (id) {
                        eq('codigo', id)
                    }
                }
            }
        }

        enqs
    }

    def sincronizarParticipaEn(ParticipaEn p, Dataset d) {
        ScriptContext newContext = new SimpleScriptContext()
        Bindings engineScope = newContext.getBindings(ScriptContext.ENGINE_SCOPE)

        String script = "var errors = [];var warnings = [];"
        def contexto = p.contexto ?: '{}'
        script += "var empresaId = '$p.empresa.empresaId';"
        script += "var datos = $d.datos;"
        script += "var contexto = $contexto;\n"
        script += "var anio = $p.edicion.anio;\n"
        script += p.edicion.scriptSincronizacion ?: ''
        script += "var esValido = errors.length == 0;\n"
        engine.eval(script, engineScope);
        log.info script
        if (engineScope.get('esValido')) {
            p.contexto =  new JsonBuilder(engineScope.get('contexto')).toPrettyString()
            p.fechaSincronizacion = new Date()
            p.save(flush: true)
        }
    }
}

