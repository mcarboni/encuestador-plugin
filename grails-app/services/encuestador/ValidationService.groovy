package encuestador

import grails.transaction.Transactional
import javax.script.*


@Transactional
class ValidationService {

	def listaService
	
    ScriptEngineManager manager = new ScriptEngineManager()
    ScriptEngine engine = manager.getEngineByName("JavaScript")

    def validarCargaFormulario(CargaFormulario cf) {
        ScriptContext newContext = new SimpleScriptContext()
        Bindings engineScope = newContext.getBindings(ScriptContext.ENGINE_SCOPE)

		engineScope.putAt("listaService", listaService);
		
        //String script =  cf.participante.edicion.funcionesValidacion ?: ''
		String script = "var $cf.formulario.tabla = $cf.datos;\n"
        def contexto = cf.participante.contexto ?: '{}'
        script += "var empresa = $contexto;\n"
		script += "var anio = $cf.participante.edicion.anio;\n"
        script += "var warnings = [];\n"
        script += cf.formulario.scriptValidacion ?: ''
        script += "var esValido = warnings.length == 0;\n"		

        engine.eval(script, engineScope);

        return [esValido: engineScope.get('esValido'), mensajes: engineScope.get('warnings'), contexto: engineScope.get('empresa')]
    }

    def validarEncuesta(ParticipaEn p) {
        ScriptContext newContext = new SimpleScriptContext()
        Bindings engineScope = newContext.getBindings(ScriptContext.ENGINE_SCOPE)

		engineScope.putAt("listaService", listaService);
		String script = "var errors = [];var warnings = [];"
		def contexto = p.contexto ?: '{}'
        script += "var empresa = $contexto;\n"
		script += "var anio = $p.edicion.anio;\n"		

        p.edicion.formularios.each { f ->
            def cf = CargaFormulario.findByFormularioAndParticipante(f,p)
            if (cf) {
                script += "var $cf.formulario.tabla = $cf.datos;\n"
                script += cf.formulario.scriptValidacion ?: ''
            } else if (f.obligatorio) {
                script += "errors.push('\"$f.orden. $f.titulo\" no se ha cargado.');"
            } else {
                script += "var $f.tabla = {}; " //
            }
        }
		script += "var esValido = errors.length == 0;\n"
		
		engine.eval(script, engineScope);
		log.info script
		
		if (engineScope.get('esValido')) {
	        script = p.edicion.scriptValidacion ?:'';
	
	        script += "esValido = errors.length == 0;\n"
       		engine.eval(script, engineScope);
		}
        log.info script

        return [esValido: engineScope.get('esValido'), mensajes: engineScope.get('errors'), contexto: engineScope.get('empresa')]
    }

}
