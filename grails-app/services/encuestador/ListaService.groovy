package encuestador

import grails.transaction.Transactional

@Transactional
class ListaService {

    def traerLista(codigo) {
		ValorListaControlada.createCriteria().list {
			lista {
				eq('codigo', codigo)
			}
			order("orden")
		}
    }

	def traerElementoDeLista(codigo, codigoLista) {
		ValorListaControlada.createCriteria().get {
			eq('codigo', codigo)
			lista {
				eq('codigo', codigoLista)
			}
		}
	}

	def traerListaComoTupla(codigo) {
		traerLista(codigo).collect { it.codigo }
	}

	def buscarEnLista(String codigoLista, String q) {
		ValorListaControlada.createCriteria().list {
			ilike('descripcion', "%q%")
			lista {
				eq('codigo', codigoLista)
			}
			order("orden")
		}
	}
}
