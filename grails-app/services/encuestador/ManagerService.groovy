package encuestador

import org.apache.commons.lang3.RandomStringUtils
import org.springframework.security.core.userdetails.UserDetails

import encuestador.security.Authority
import encuestador.security.Person
import encuestador.security.PersonAuthority
import grails.gsp.PageRenderer
import grails.plugin.springsecurity.rest.authentication.RestAuthenticationEventPublisher
import grails.plugin.springsecurity.rest.token.AccessToken
import grails.transaction.Transactional

@Transactional
class ManagerService {

	def templateEngine = new groovy.text.SimpleTemplateEngine()
	def mailService
	def PageRenderer groovyPageRenderer

	/**
	 * 
	 * @param user 		
	 * @param codigo
	 * @param params
	 * @return Lista de participantes que puede ver el usuario pasado por parámetro
	 * 
	 */
	def empresasParaUsuarioYEncuesta(user, codigo, params ) {
		def enq
		if (user.authorities.any {
			'ROLE_FOP'.equals(it.authority)
		}) {
			def encuestador1 = Encuestador.findByUser(user)

			enq = AsignacionParticipante.createCriteria().list(params) {
				projections { property('participante') }
				eq('encuestador', encuestador1)
				participante {
					edicion {
						if (params.edicion) {
							eq('codigo', params.edicion)
						}
					}
					empresa {
						if (params.empresaId) {
							eq('empresaId', params.long('empresaId'))
						}
						if (params.empresa) {
							ilike ('nombre', '%'+params.empresa+'%')
						}
						if (params.cuit) {
							eq('cuit', params.cuit)
						}
						order('nombre')
					}
					if (params.estado) {
						eq('estadoBase1', EstadoEncuesta.valueOf(params.estado))
					}
				}
			}
		} else {
			enq = ParticipaEn.createCriteria().list(params) {
				if (params.traerEmpresas) {
					projections { property('empresa') }
				}
				edicion {
					if (params.edicion) {
						eq('codigo', params.edicion)
					} else {
						eq('codigo', codigo)
					}
				}
				empresa {
					if (params.empresaId) {
						eq('empresaId', params.empresaId)
					}
					if (params.empresa) {
						ilike ('nombre', '%'+params.empresa+'%')
					}
					if (params.cuit) {
						eq('cuit', params.cuit)
					}
					order('nombre')
				}
				if (params.estado) {
					eq('estadoBase1', EstadoEncuesta.valueOf(params.estado))
				}
				if (user.authorities.any { 'ROLE_FOP_SUPERVISOR'.equals(it.authority)  }) {
					eq('relevadaExternamente', true)
				}
			}
		}

		enq
	}

	def eventosPorEncuesta(codigo, params ) {
		def ev
		ev = EventoParticipacion.createCriteria().list(params) {
			participacion {
				edicion {
					if (params.edicion) {
						eq('codigo', params.edicion)
					}
				}

				empresa{
					if (params.empresaId) {
						eq('empresaId', params.long('empresaId'))
					}
					if (params.empresa) {
						ilike ('nombre', '%'+params.empresa+'%')
					}
					if (params.cuit) {
						eq('cuit', params.cuit)
					}
					order("empresaId")
				}
			}
			order("fecha")
		}

		ev
	}

	/**
	 *
	 * @param solicitud
	 * @return
	 */
	@Transactional
	def contactar(user, solicitud) {
		def status = 'success'
		def message = 'Se envió el correo.'

		def participaEn = ParticipaEn.get(solicitud.participanteId)
		if (participaEn) {
		}
		def puntoDeContacto
		if (solicitud.puntoDeContacto == 'otro') {
			message += 'Hay que agregar nuevo punto de contacto.'
			puntoDeContacto = new PuntoDeContacto(contacto: solicitud.email, posicion: solicitud.posicion, persona: solicitud.nombre, tipo: TipoPuntoDeContacto.EMAIL)
			puntoDeContacto.save(flush: true)
			participaEn.empresa.addToPuntosDeContacto(puntoDeContacto)
			participaEn.empresa.save(flush: true)
		} else {
			puntoDeContacto = PuntoDeContacto.get(solicitud.puntoDeContacto)
		}

		def c = ContactoTemplate.findByCodigo(solicitud.tipoContacto)
		def cuerpo, asunto, elEvento, elComentario
		if (!c) {
			asunto = solicitud.asunto
			cuerpo = solicitud.cuerpo
			elEvento = "$user.username envio un correo electronico al contacto $puntoDeContacto.persona ($puntoDeContacto.contacto)."
			elComentario = "Asunto: $asunto\r\nCuerpo:$cuerpo\r\n"
		} else {
			def bindings = [nombre: puntoDeContacto.persona,encuesta: participaEn?.edicion]
			if (solicitud.tipoContacto == 'reenvio') {
				bindings['empresa'] = participaEn.empresa.nombre
				bindings['username'] = participaEn.empresa.user.username
				bindings['password'] = RandomStringUtils.random(9, true, true)
				bindings['fecha'] = new Date().format("dd 'de' MMMM 'de' yyyy")

				if (participaEn.relevadaExternamente) {
					bindings['campo'] = 'la Dirección Nacional de Información Científica perteneciente a la Subsecretaría de Estudios y Prospectiva junto a la Fundación del Observatorio Pyme (FOP)'
					bindings['plazo'] = '14 de agosto hasta el 28 de agosto'
					bindings['telefono'] = '(011) 4018-6525/6522, de lunes a jueves de 8 a 18hs'
					bindings['email'] = participaEn.edicion.emailAdministrativoExterno
				} else {
					bindings['campo'] = 'la Dirección Nacional de Información Científica perteneciente a la Subsecretaría de Estudios y Prospectiva'
					bindings['plazo'] = '14 de septiembre hasta el 29 de septiembre'
					bindings['telefono'] = '(011) 4899-5000 internos: 3060, 3070 y 3038'
					bindings['email'] = participaEn.edicion.emailAdministrativo
				}

				participaEn.empresa.user.password = bindings['password']
				participaEn.empresa.user.save(flush: true)
				elEvento = "$user.username solicito el reenvio de usuario y contraseña para '$participaEn.empresa.nombre' y se envió un correo electronico al contacto $puntoDeContacto.persona ($puntoDeContacto.contacto)."
				elComentario = "Se reenvio el usuario y la contraseña."
			}

			cuerpo = templateEngine.createTemplate(c.template).make(bindings).toString()
			asunto = c.asunto
		}

		mailService.sendMail {
			multipart true
			to puntoDeContacto.contacto
			bcc participaEn.edicion.emailCopia
			from participaEn.relevadaExternamente ?  participaEn.edicion.emailAdministrativoExterno : participaEn.edicion.emailAdministrativo
			subject asunto
			html cuerpo
		}

		def ev = new EventoContacto(fecha: new Date(),
		usuario: user,
		contacto: puntoDeContacto,
		titulo: elEvento,
		comentarios: elComentario)
		ev.save(flush: true)
		def aide = ev.id
		resultado(status, message)
	}

	def resultado(status, message) {
		[estado: status, mensaje: message]
	}

	@Transactional
	def asignarEncuestadorA(Person user, datos) {
		def mensaje
		if (!datos.participanteId) {
			mensaje = 'Debe seleccionar una empresa.'
		} else if (!datos.encuestadorId) {
			mensaje = 'Debe seleccionar un evaluador.'
		} else {
			def elParticipante = ParticipaEn.get(datos.participanteId)
			def elEncuestador = Encuestador.get(datos.encuestadorId)
			def actual = AsignacionParticipante.findByParticipante(elParticipante)
			if (actual) {
				actual.delete()
			}
			new AsignacionParticipante(encuestador: elEncuestador, participante: elParticipante).save(flush:  true)
			new EventoParticipacion(
					participacion: elParticipante,
					base: Base.BASE_UNO,
					estadoOrigen: elParticipante.estadoBase1,
					estadoDestino: elParticipante.estadoBase1,
					fecha: new Date(),
					usuario: user,
					observacion: "$user.username asigno a $elEncuestador.user.username la empresa '$elParticipante.empresa.nombre', desasignando al encuestador anterior."
					).save( flush:  true)
			return resultado('success', 'Empresa asociada al Evaluador')
		}
		resultado('error', mensaje)
	}

	def messageSource

	def nuevaEmpresa(datos) {
		def mensaje
		if (!datos.empresaUsername) {
			mensaje = 'empresaUsername: Debe ingresar el nombre de usuario para la empresa'
			//} else if (!datos.razonSocial) {
			//mensaje = 'razonSocial: Debe ingresar la razón social'
		} else if (!datos.cuit) {
			mensaje = 'cuit: Debe ingresar el CUIT de la empresa'
		} else if (!datos.email) {
			mensaje = 'email: Debe ingresar la dirección de correo electronico'
		} else {
			mensaje = 'Problemas intentando registrar la empresa'
			def re = Authority.findByAuthority("ROLE_EMPRESA")
			def pass = (datos.empresaUsername)
			def person = new Person(username: datos.empresaUsername, password: pass)
			def emp
			Person.withTransaction { status ->
				try {
					if (!person.save()) {
						log.error "Failed to Save Person"
						mensaje = person.errors.allErrors.collect { messageSource.getMessage(it, null)}.join(";")
						throw new RuntimeException( mensaje )
					}

					def newUser = Person.findByUsername(datos.empresaUsername)
					new PersonAuthority(authority:re, person: newUser).save()

					emp = new Empresa(cuit: datos.cuit, nombre: datos.razonSocial == '' ? datos.cuit : datos.razonSocial, user: newUser, email: datos.email)
					if (!emp.save()) {
						log.error "Failed to Save Empresa"
						mensaje = emp.errors.allErrors.collect { messageSource.getMessage(it, null)}.join(";")
						throw new RuntimeException( mensaje )
					}
					emp = Empresa.findByCuit(datos.cuit)
					Edicion enq = Edicion.last(sort:"anio")
					new ParticipaEn(empresa: emp, edicion: enq).save(flush:true)
				} catch (RuntimeException ex) {
					status.setRollbackOnly()
					return resultado('error', ex.getMessage())
				}
			}
			if (emp) {
				return 	[estado: 'success', mensaje: 'Empresa '+ datos.razonSocial + ' registrada.', 'cuit': emp.cuit, 'empresaId': emp.empresaId]
			}
		}
		return resultado('error', mensaje)
	}

	def cambiarEstado(Person user, ParticipaEn pe, EstadoEncuesta nuevoEstado, params) {
		ParticipaEn enq = ParticipaEn.findById(pe.id)
		def elEstado = 'error'
		def elMensaje = "Problemas intentando cambiar al estado $nuevoEstado"
		def estadoActual = enq.estadoBase1
		if (estadoActual == EstadoEncuesta.POR_VALIDAR && nuevoEstado == EstadoEncuesta.REABIERTA) {
			elEstado = 'success'
			elMensaje = "$user.username reabrio la encuesta $enq.edicion.codigo para el la empresa '$enq.empresa.nombre'."
			enq.estadoBase1 = nuevoEstado
			// traer todos
			Edicion ed = Edicion.findById(enq.edicion.id);
			ed.formularios.each {
				CargaFormulario cf = CargaFormulario.findByFormularioAndParticipante(it, enq)
				if (cf) {
					cf.crearSnapshot(user.username).save()
				}
			}
			enq.save(flush: true)
			new EventoParticipacion(fecha: enq.fechaApertura, usuario: user, estadoOrigen: estadoActual, estadoDestino: nuevoEstado, base: Base.BASE_UNO, observacion: elMensaje, participacion: enq).save(flush: true)
		} else if (nuevoEstado == EstadoEncuesta.REABIERTA && (estadoActual == EstadoEncuesta.VALIDADA || estadoActual == EstadoEncuesta.DECLINADA || estadoActual == EstadoEncuesta.ABIERTA)) {
			elEstado = 'success'
			elMensaje = "$user.username reabrio EXCEPCIONALMENTE la encuesta $enq.edicion.codigo para el la empresa '$enq.empresa.nombre' que se encontraba en $estadoActual."
			enq.estadoBase1 = nuevoEstado
			enq.save(flush: true)
			new EventoParticipacion(fecha: enq.fechaApertura, usuario: user, estadoOrigen: estadoActual, estadoDestino: nuevoEstado, base: Base.BASE_UNO, observacion: elMensaje, participacion: enq).save(flush: true)
		} else if (estadoActual == EstadoEncuesta.POR_VALIDAR && nuevoEstado == EstadoEncuesta.VALIDADA) {
			elEstado = 'success'
			elMensaje = "$user.username valido la encuesta $enq.edicion.codigo para el la empresa '$enq.empresa.nombre'."
			enq.estadoBase1 = nuevoEstado
			enq.save(flush: true)
			new EventoParticipacion(fecha: enq.fechaEnvio, usuario: user, estadoOrigen: estadoActual, estadoDestino: nuevoEstado, base: Base.BASE_UNO, observacion: elMensaje, participacion: enq).save(flush: true)
		} else if (nuevoEstado == EstadoEncuesta.OBSERVADA) {
			elEstado = 'success'
			elMensaje = "$user.username observo la encuesta $enq.edicion.codigo para el la empresa '$enq.empresa.nombre'."
			elMensaje += params.observaciones ? "\n\r$params.observaciones" : ""
			enq.estadoBase1 = nuevoEstado
			enq.save(flush: true)
			new EventoParticipacion(fecha: enq.fechaEnvio, usuario: user, estadoOrigen: estadoActual, estadoDestino: nuevoEstado, base: Base.BASE_UNO, observacion: elMensaje, participacion: enq).save(flush: true)
		} else if (nuevoEstado == EstadoEncuesta.DECLINADA) {
			elEstado = 'success'
			elMensaje = "$user.username declino la encuesta $enq.edicion.codigo para el la empresa '$enq.empresa.nombre'."
			elMensaje += params.observaciones ? "\n\r$params.observaciones" : ""
			enq.estadoBase1 = nuevoEstado
			enq.save(flush: true)
			new EventoParticipacion(fecha: enq.fechaEnvio, usuario: user, estadoOrigen: estadoActual, estadoDestino: nuevoEstado, base: Base.BASE_UNO, observacion: elMensaje, participacion: enq).save(flush: true)
		}

		[estado: elEstado, mensaje: elMensaje]
	}

	def tokenGenerator
	def tokenStorageService
	def RestAuthenticationEventPublisher authenticationEventPublisher
	def userDetailsService

	def autenticar(username) {
		//def username = username

		UserDetails us = userDetailsService.loadUserByUsername(username)

		AccessToken accessToken = tokenGenerator.generateAccessToken(us)
		log.debug "Generated token: ${accessToken}"

		tokenStorageService.storeToken(accessToken.accessToken, us)

		authenticationEventPublisher.publishTokenCreation(accessToken)

		return accessToken
	}

	def empresas(user, params ) {
		def enq
		if (!params.edicion) {
			enq = Empresa.createCriteria().list(params) {
				if (params.empresaId) {
					eq('empresaId', params.empresaId)
				}
				if (params.empresa) {
					ilike ('nombre', '%'+params.empresa+'%')
				}
				if (params.cuit) {
					eq('cuit', params.cuit)
				}
				order('nombre')
			}
		} else {
			params.traerEmpresas = true
			enq = empresasParaUsuarioYEncuesta(user,params.edicion, params)
		}

		enq
	}

	def edicionesPorEmpresa(user, params ) {
		def enq
		enq = ParticipaEn.createCriteria().list(params) {
			edicion {
				if (params.codigo) {
					eq('codigo', codigo)
				}
				order('anio', 'desc')
			}
			empresa {
				eq('empresaId', params.empresaId)
			}
		}
		enq
	}

}
