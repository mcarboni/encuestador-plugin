package encuestador

import encuestador.security.Person
import grails.plugin.springsecurity.SpringSecurityUtils
import grails.plugin.springsecurity.userdetails.GrailsUser
import grails.plugin.springsecurity.userdetails.GrailsUserDetailsService
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UsernameNotFoundException
import grails.transaction.Transactional

@Transactional
class CaseInsensitiveDetailsService implements GrailsUserDetailsService {

    static final List NO_ROLES = [new SimpleGrantedAuthority(SpringSecurityUtils.NO_ROLE)]

    UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Person.withTransaction { status ->
            Person user = Person.findByUsernameIlike(username)
            if (!user) throw new UsernameNotFoundException('User not found', username)

            def authorities = user.authorities.collect { new SimpleGrantedAuthority(it.authority) }

            new GrailsUser(user.username, user.password, user.enabled, !user.accountExpired, !user.passwordExpired,
                    !user.accountLocked, authorities ?: NO_ROLES, user.id)
        }
    }

    UserDetails loadUserByUsername(String username, boolean loadRoles) throws UsernameNotFoundException {
        loadUserByUsername username
    }
}

