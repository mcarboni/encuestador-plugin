package encuestador

import grails.converters.JSON
import grails.transaction.Transactional
import grails.web.JSONBuilder
import groovy.json.JsonBuilder
import groovy.json.JsonSlurper

import javax.script.Bindings
import javax.script.ScriptContext
import javax.script.ScriptEngine
import javax.script.ScriptEngineManager
import javax.script.SimpleScriptContext

@Transactional
class EncuestaService {

    def listaService

    ScriptEngineManager manager = new ScriptEngineManager()
    ScriptEngine engine = manager.getEngineByName("JavaScript")

    public Map<String,Object> traerDatosEdicion(String codigoEdicion, String empresaId) {
        def ret = [:]
        Edicion edi = Edicion.findByCodigo(codigoEdicion)
        Empresa emp = Empresa.findByEmpresaId(empresaId)
        ParticipaEn p = ParticipaEn.findByEmpresaAndEdicion(emp, edi)
        ret['participa'] = false
        if (p) {
            ret['participa'] = true
            p.edicion.formularios.each { f ->
                def cf = CargaFormulario.findByFormularioAndParticipante(f,p)
                ret[f.tabla] = cf? new JsonSlurper().parseText(cf.datos) : [:]
            }
        }
        ret
    }

    /**
     *
     *
     * @param ParticipaEn
     * @return
     */
    @Transactional
    def sincronizar(ParticipaEn p) {
        def codigoDataset = "${p.empresa.empresaId}_${p.edicion.encuesta.codigo}"
        def anio = p.edicion.anio.toString()
        def aura = new Date()
        def d = Dataset.findByCodigo(codigoDataset) ?: new Dataset(codigo: codigoDataset, creado: aura, actualizado: aura)

        // busca los datos de todos los formularios
        def datosEdicion = traerDatosEdicion(p.edicion.codigo, p.empresa.empresaId)
        def datos = new JsonSlurper().parseText(d.datos) ?: [:]

        // actualiza o inserta los datos correspondientes a la edicion
        datos[anio] = datosEdicion

        d.actualizado = aura
        d.datos = new JsonBuilder(datos).toPrettyString() // vuelve a guardar como JSON

        d.save()
        d
        /*
        encuesta.ediciones.each {
            ParticipaEn p = ParticipaEn.findByEmpresaAndEdicion(empresa, it)
            if ( p && p.edicion.codigo != codigo) {
                ScriptContext newContext = new SimpleScriptContext()
                Bindings engineScope = newContext.getBindings(ScriptContext.ENGINE_SCOPE)

                engineScope.putAt("listaService", listaService);
                engineScope.putAt("encuestaService", this);
                String script = "var errors = [];var warnings = [];"
                def contexto = p.contexto ?: '{}'
                script += "var empresaId = '$empresa.empresaId';"
                script += "var contexto = $contexto;\n"
                script += "var anio = $p.edicion.anio;\n"
                script += p.edicion.scriptSincronizacion ?: ''
                script += "var esValido = errors.length == 0;\n"
                println (script)
                engine.eval(script, engineScope);
                log.info script
                if (engineScope.get('esValido')) {
                    print engineScope.get('contexto')
                    p.contexto =  new JsonBuilder(engineScope.get('contexto')).toPrettyString()
                    // print p.contexto
                    p.save(flush: true)
                }
            }
        }
        */
    }
}
