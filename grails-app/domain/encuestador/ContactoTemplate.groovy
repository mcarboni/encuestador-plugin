package encuestador

class ContactoTemplate {

    String codigo
    String asunto
    String template

    static constraints = {
        codigo()
        asunto()
        template()
    }

    static mapping = {
        template type: 'text'
    }

}
