package encuestador

class CargaSnapshot {

    static belongsTo = [carga: CargaFormulario]

    String datos = "{}"
    String resultadoValidacion = "[]"
    Date creado = new Date()
    String creadoPor
    Base base

    static mapping = {
        datos type: 'text'
        resultadoValidacion type: 'text'
    }

    static constraints = {
        creadoPor()
        creado()
        base()
        resultadoValidacion()
        datos()
    }

}
