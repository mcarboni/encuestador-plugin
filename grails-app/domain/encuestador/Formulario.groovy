package encuestador

import groovy.transform.ToString

@ToString(includes=['titulo','encuesta'], includeNames=true, includePackage=false)
class Formulario {

    String titulo
    String tabla
    String template = ""
    String templateVista = ""
    String scriptValidacion = ""
	String campos
    Boolean obligatorio = true
    Integer orden

    static belongsTo = [edicion: Edicion]
    static constraints = {
        template nullable: true
		scriptValidacion nullable: true
		templateVista nullable: true
		campos nullable: true
        obligatorio nullable: true
    }

    static mapping = {
        scriptValidacion type: 'text'
        template type: 'text'
		templateVista type: 'text'
		campos type: 'text'
    }

	def extraerCampos() {
		def camposRegex = /datos\.\w+/
		
		(this.template =~ /$camposRegex/).collect { '"'+it.tokenize('.')[1]+'"' } as Set
	}

    def beforeInsert() {
        this.campos = extraerCampos()
    }

    def beforeUpdate() {
        this.campos = extraerCampos()
    }
}
