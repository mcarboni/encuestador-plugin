package encuestador

import encuestador.security.Person

class EventoParticipacion {

	Date fecha
	Person usuario
	EstadoEncuesta estadoOrigen
	EstadoEncuesta estadoDestino
	Base base
	String observacion

	static belongsTo = [participacion: ParticipaEn]

	static mapping = { observacion type: 'text' }

	static constraints = {
		fecha()
		usuario()
		base()
		estadoOrigen nullable: true
		estadoDestino()
		observacion()
	}
}
