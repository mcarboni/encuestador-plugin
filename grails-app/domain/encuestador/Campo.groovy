package encuestador

import groovy.transform.ToString

@ToString(includes='nombre', includeNames=true, includePackage=false)
class Campo {

    String nombre
    String etiqueta
    String tipo
    Integer longitud
    Boolean esObligatorio
    Integer orden

    static belongsTo = [formulario: Formulario]

    static constraints = {
    }
}
