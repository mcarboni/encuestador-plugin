package encuestador

import encuestador.security.Person
import groovy.transform.ToString

import static java.util.UUID.*

@ToString(includes='nombre', includeNames=true, includePackage=false)
class Empresa {

    String nombre

    String cuit

    String email

    String empresaId = randomUUID().toString()

    String anotaciones = '{}'

    static belongsTo = [user: Person]

    static hasMany = [puntosDeContacto: PuntoDeContacto]

    static constraints = {
        //nombre unique: true
        //cuit unique: true
        email email: true, blank: false //, unique: true
        anotaciones nullable: true
    }
}
