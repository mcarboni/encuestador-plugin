package encuestador

class ValorListaControlada {

    String codigo

    String descripcion

    String discriminador

    Integer orden

    static belongsTo = [lista: ListaControlada]

    static constraints = {
        orden()
        codigo()
        descripcion()
        discriminador nullable: true
    }

    static mapping = {
    }
}
