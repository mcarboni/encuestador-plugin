package encuestador

import groovy.transform.ToString

@ToString
class Encuesta {

    String codigo

    String nombre

    Boolean habilitado

    static hasMany = [ ediciones: Edicion ]

    static constraints = {
    }

}
