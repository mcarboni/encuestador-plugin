package encuestador

class PuntoDeContacto {

    String contacto
    String persona
    String posicion
    TipoPuntoDeContacto tipo

    static constraints = {
    }

}
