 package encuestador

import groovy.transform.ToString

@ToString(includes=['empresa','encuesta', 'estadoBase1', 'estadoBase2'], includeNames=true, includePackage=false)
class ParticipaEn {

	static estadosEditables = [EstadoEncuesta.ABIERTA, EstadoEncuesta.REABIERTA]
	
    EstadoEncuesta estadoBase1 = EstadoEncuesta.INVITADA
    EstadoEncuesta estadoBase2 = EstadoEncuesta.ABIERTA
	String resultadoValidacion = '[]'
	String contexto = '{}'
	Date fechaApertura
	Date fechaEnvio
	Date fechaSincronizacion
	Boolean relevadaExternamente = false
	Empresa empresa
	Edicion edicion

	static belongsTo = [empresa: Empresa, edicion: Edicion]

    static constraints = {
		id()
		edicion()
		empresa()
		estadoBase1()
		fechaApertura nullable: true
		fechaEnvio nullable: true
		relevadaExternamente nullable: true
		resultadoValidacion nullable: true
		contexto nullable: true
		fechaSincronizacion nullable: true
	}
	
	static mapping = {
		contexto type: 'text'
		resultadoValidacion type: 'text'
	}
	
	def boolean esEditable(Base base) {
		def estadoBase = this.estadoBase1
		if (Base.BASE_DOS.equals(base)) {
			estadoBase = this.estadoBase2
		}
		def loEs = estadosEditables.any { it.equals(estadoBase) }
		
		loEs
	}
}
