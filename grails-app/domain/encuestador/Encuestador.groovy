package encuestador

import encuestador.security.Person

class Encuestador {

    String nombre

    String email

    static belongsTo = [user: Person]

    static constraints = {
    }

    def boolean puedeAsignaUsuarios() {
        user?.authorities?.find { it.authority == 'ROLE_DNIC' || it.authority == 'ROLE_FOP_ADMIN'}
    }

}
