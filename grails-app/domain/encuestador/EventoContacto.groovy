package encuestador

import encuestador.security.Person

class EventoContacto {

    Date fecha
    Person usuario // el usuario que realiza el evento de contacto
    PuntoDeContacto contacto
    String titulo // invitacion, envio de correo, llamado telefonico
    String comentarios

    static constraints = {
    }

    static mapping = {
        comentarios type: 'text'
    }

}
