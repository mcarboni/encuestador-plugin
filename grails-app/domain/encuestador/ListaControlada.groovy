package encuestador

class ListaControlada {

    String codigo

    String nombre

    static hasMany = [ valores: ValorListaControlada ]

    static constraints = {
        codigo()
        nombre()
    }
}
