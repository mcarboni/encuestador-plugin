package encuestador

class Dataset {

    String codigo
    String titulo = ''
    String datos = '{}'
    Date creado = new Date()
    Date actualizado = new Date()

    static mapping = {
        datos type: 'text'
    }

    static constraints = {

    }
}
