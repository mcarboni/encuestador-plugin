package encuestador

import groovy.transform.ToString

@ToString(includes=['usuario', 'formulario', 'participante', 'actualizado'], includeNames=true, includePackage=false)
class CargaFormulario {

	String creadoPor
	String actualizadoPor
	Date creado = new Date()
	Date actualizado = new Date()
	String datos = "{}"
	String resultadoValidacion = "[]"
	EstadoCarga estado = EstadoCarga.P
	Base base

	static belongsTo = [formulario: Formulario, participante: ParticipaEn]

	static mapping = { 
		datos type: 'text'
		resultadoValidacion type: 'text'
	}

	static constraints = {
		creadoPor()
		creado()
		actualizadoPor()
		actualizado()
		actualizado()
		formulario( unique: 'participante')
		participante()
	}

	def actualizar(usuario, datos) {
		if ( participante.estadoBase1 in [EstadoEncuesta.ABIERTA, EstadoEncuesta.REABIERTA]&& formulario.edicion.habilitado) {
			this.actualizadoPor = usuario
			this.actualizado = new Date()
			this.datos = datos
		}
	}

	def crearSnapshot(usuario) {
		new CargaSnapshot(
				creadoPor: usuario,
				creado: new Date(),
				datos: this.datos,
				base: this.base,
				resultadoValidacion: this.resultadoValidacion,
				carga: this
		)
	}
}
