package encuestador

import groovy.transform.ToString

@ToString(includes='nombre', includeNames=true, includePackage=false)
class Edicion {

    String codigo

    String nombre

    Date inicio

    Date fin

    Integer anio

    Boolean habilitado = true

    String scriptValidacion = ""

    String scriptSincronizacion = ""
	
	String funcionesValidacion = ""

    String templatePresentacion

    String url

    String abreviatura
	
	String emailAdministrativo
	
	String emailAdministrativoExterno
	
	String emailCopia

    static belongsTo = [ encuesta : Encuesta ]

    static hasMany = [ formularios: Formulario ]

    static constraints = {
        nombre()
        anio()
        inicio()
        fin()
        url nullable: true
        abreviatura nullable: true
        scriptValidacion nullable: true
        scriptSincronizacion nullable: true
		templatePresentacion nullable: true
		emailAdministrativo nullable: true
		emailAdministrativoExterno nullable: true
		emailCopia nullable: true
    }

    static mapping = {
        scriptValidacion type: 'text'
        scriptSincronizacion type: 'text'
		funcionesValidacion type: 'text'
        formularios sort: 'orden'
		templatePresentacion type: 'text'
    }
}
